package com.iconcept.tpbguide.tpbevents.Models;

import java.util.Date;

import org.json.JSONArray;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("ScheduleDate")
public class Schedule extends ParseObject{
	public String getTitle(){
		return getString("title");
	}
	public Date getDate(){
		return getDate("date");
	}
	public JSONArray getScheduleSlots(){
		return getJSONArray("scheduleSlots");
	}
	
}
