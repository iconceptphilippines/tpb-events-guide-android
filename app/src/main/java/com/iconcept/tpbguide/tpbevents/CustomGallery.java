package com.iconcept.tpbguide.tpbevents;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.iconcept.tpbguide.tpbevents.R;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

public class CustomGallery extends ParseQueryAdapter<ParseObject>{
	public static String id;
	public static Event_Single eve; 
	public CustomGallery(Context context) {
		super(context, new ParseQueryAdapter.QueryFactory<ParseObject>() {	
			public ParseQuery create() {
//				eve = new Event_Single();
//				id = eve.getIntent().getExtras().getString("OBJECT_ID");
				ParseQuery query = new ParseQuery("Post");
				query.whereEqualTo("objectId", "A6eAKHKQVh");
				ParseQuery inner = new ParseQuery("Photo");
				inner.whereMatchesQuery("post", query);
				return inner;
			}
		});
	}
	
	@Override
	public View getItemView(ParseObject object, View v, ViewGroup parent) {
		if (v == null) {
			v = View.inflate(getContext(), R.layout.custom_gallery_layout, null);
		}
		super.getItemView(object, v, parent);
		ParseImageView eventsImage = (ParseImageView) v.findViewById(R.id.img);
		ParseFile imageFile = object.getParseFile("image");
		if (imageFile != null) {
			eventsImage.setParseFile(imageFile);
			eventsImage.loadInBackground();
		}
		return v;
		
		
	}
}
