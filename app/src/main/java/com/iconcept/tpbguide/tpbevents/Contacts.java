package com.iconcept.tpbguide.tpbevents;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.iconcept.tpbguide.tpbevents.R;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ParseRelation;

public class Contacts extends Activity {

	ListView contactList;
	ProgressBar progress;
	String userId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(false);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		setContentView(R.layout.activity_contacts);
		loadContacts();
	}

	@Override
	protected void onResume(){
		super.onResume();
		loadContacts();
	}

	public void loadContacts()
	{

		progress = (ProgressBar) findViewById(R.id.contactProgress);
		contactList = (ListView) findViewById(R.id.contactList);
		
		progress.setVisibility(View.VISIBLE);
		contactList.setVisibility(View.GONE);

		final ArrayList<ParseObject> contactListData = new ArrayList<ParseObject>();
		final AttendeesAdapter contactListDataAdapter = new AttendeesAdapter(this, contactListData); 
		
		contactList.setAdapter(contactListDataAdapter);
		
		ParseUser currentUser = ParseUser.getCurrentUser();
		ParseQuery queryContacts = currentUser.getRelation("contacts").getQuery();
		queryContacts.findInBackground(new FindCallback<ParseUser>() 
		{
			@Override
			public void done(final List<ParseUser> contacts, ParseException e)
			{
				if( e == null )
				{
					if( contacts.size() > 0 )
					{
						progress.setVisibility(View.GONE);
						contactList.setVisibility(View.VISIBLE);
						contactListDataAdapter.addAll(contacts);
						contactList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
								String userObjId = String.valueOf(contacts.get(position).getObjectId());
								Intent intent = new Intent(getApplicationContext(), AttendeesProfile.class);
								intent.putExtra("UserId", userObjId);
								invalidateOptionsMenu();
								startActivity(intent);
							}
						});
					}else{
						Toast.makeText(getApplicationContext(), "No contacts saved!.", Toast.LENGTH_LONG).show();
					}
				}else{
					Toast.makeText(getApplicationContext(), "Error in retrieving contacts: " + e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
