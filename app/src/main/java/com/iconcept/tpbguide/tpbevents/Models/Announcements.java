package com.iconcept.tpbguide.tpbevents.Models;

import java.util.Date;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Announcement")
public class Announcements extends ParseObject {
	public String getTitle(){
		return getString("title");
	}
	public Date getDate(){
		return getCreatedAt();
	}
	public String getContent(){
		return getString("content");
	}
}	
