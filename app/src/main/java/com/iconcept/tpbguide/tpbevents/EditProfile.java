package com.iconcept.tpbguide.tpbevents;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.iconcept.tpbguide.tpbevents.R;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

public class EditProfile extends Activity {
	EditText username;
	EditText userEmail;
	EditText userFname;
	EditText userLname;
	EditText userMname;
	EditText userComp;
	EditText userPos;
	EditText userNo;
	ParseUser current;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(isNetworkAvailable()) {
			displayProfile();
			getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
			getActionBar().setDisplayHomeAsUpEnabled(true);
        }else {
        	
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Connection failed");
            builder.setMessage("Enable mobile network or Wi-Fi to continue using this application.");

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                	startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                }
            });
            builder.show();
        }
		
	}
	public void displayProfile(){
		current = ParseUser.getCurrentUser();
		if(current != null){
			setContentView(R.layout.activity_edit_profile);
			username = (EditText) findViewById(R.id.edit_userName);
			userEmail = (EditText) findViewById(R.id.edit_email);
			userFname = (EditText) findViewById(R.id.edit_userFname);
			userLname = (EditText) findViewById(R.id.edit_userLname);
			userMname = (EditText) findViewById(R.id.edit_userMname);
			userComp = (EditText) findViewById(R.id.edit_user_comp);
			userPos = (EditText) findViewById(R.id.edit_userPos);
			userNo = (EditText) findViewById(R.id.edit_userPh);
			
			username.setText(current.getUsername());
			userEmail.setText(current.getEmail());
			userFname.setText(current.getString("firstName"));
			userLname.setText(current.getString("lastName"));
			userMname.setText(current.getString("middleName"));
			userComp.setText(current.getString("company"));
			userPos.setText(current.getString("position"));
			userNo.setText(current.getString("mobileNo"));
			
		} else {
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
		}
	}
	
	public void saveProfile(View view) {
		final String email = userEmail.getText().toString();
		final String fname = userFname.getText().toString();
		final String lname = userLname.getText().toString();
		final String mname = userMname.getText().toString();
		final String comp = userComp.getText().toString();
		final String pos = userPos.getText().toString();
		final String phone = userNo.getText().toString();
		
		Integer error = 0;
    	if(!email.equals("")){
    		current.put("email", email);
    	} else {
    		error++;
    	}
    	if(!fname.equals("")){
    		current.put("firstName", fname);
    	} else {
    		error++;
    	}
    	if(!lname.equals("")){
    		current.put("lastName", lname);
    	} else {
    		error++;
    	}
    	if(!comp.equals("")){
    		current.put("company", comp);
    	} else {
    		error++;
    	}
    	if(!pos.equals("")){
    		current.put("position", pos);
    	} else {
    		error++;
    	}
    	current.put("middleName", mname);
    	current.put("mobileNo", phone);
    	if(error == 0){
    		current.saveInBackground();
    		Intent userProfile = new Intent(getApplicationContext(), UserProfile.class);
    		invalidateOptionsMenu();
    		finish();
    		startActivity(userProfile);
    	} else {
    		Toast.makeText(getApplicationContext(), "Please Complete all the Fields", Toast.LENGTH_SHORT).show();
    	}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.edit_profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
	    case android.R.id.home:
	    	Intent userProfile = new Intent(this, UserProfile.class);
	    	invalidateOptionsMenu();
	    	finish();
	    	startActivity(userProfile);
	        return true;
	    case R.id.action_resetPassword:
	    	actionResetPassword();
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	private void actionResetPassword() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Reset Password");
		builder.setMessage("Are you sure you want to reset your password?");
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int id) {
        	   ParseUser user = ParseUser.getCurrentUser();
       		String userEmail = user.getEmail();
       		ParseUser.requestPasswordResetInBackground(userEmail, new RequestPasswordResetCallback() {

       			@Override
       			public void done(ParseException e) {
       				// TODO Auto-generated method stub
       				if (e==null) {
       					Toast.makeText(getApplicationContext(),"Please see your email for reset password instruction.",Toast.LENGTH_LONG).show();
       				}else {
       					Toast.makeText(getApplicationContext(), "Reset Password failed.",Toast.LENGTH_LONG).show();
       				}
       			}
       			
       		});
           }
       })
       .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int id) {
        	   dialog.dismiss();  	   
           }
       }).show() ;
	}
	private boolean isNetworkAvailable() {
    	ConnectivityManager connectivityManager 
        = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		  return activeNetworkInfo != null;
	}
}
