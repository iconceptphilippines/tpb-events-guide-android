package com.iconcept.tpbguide.tpbevents;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.iconcept.tpbguide.tpbevents.R;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class Registration extends Activity {
	
	EditText regUserName;
	EditText regUserFname;
	EditText regUserMname;
	EditText regUserLname;
	EditText regUserComp;
	EditText regUserEmail;
	EditText regUserPos;
	EditText regUserNo;
	EditText regUserPw;
	EditText regUserCPw;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		setContentView(R.layout.activity_registration);
		
		regUserFname = (EditText) findViewById(R.id.reg_userFname);
		regUserLname = (EditText) findViewById(R.id.reg_userLname);
		regUserComp = (EditText) findViewById(R.id.reg_userComp);
		regUserNo = (EditText) findViewById(R.id.reg_userNo);
		regUserPos = (EditText) findViewById(R.id.reg_userPos);
		regUserName = (EditText) findViewById(R.id.reg_userName);
		regUserEmail = (EditText) findViewById(R.id.reg_userEmail);
		regUserPw = (EditText) findViewById(R.id.reg_userPw);
		Button register = (Button) findViewById(R.id.btn_signup);
		
		
		
		register.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final String userFname = regUserFname.getText().toString().trim();
				final String userLname = regUserLname.getText().toString().trim();
				final String userComp = regUserComp.getText().toString().trim();
				final String userPos = regUserPos.getText().toString().trim();
				final String userNo = regUserNo.getText().toString().trim();
				final String userName = regUserName.getEditableText().toString();
				final String userEmail = regUserEmail.getText().toString().trim();
				final String passWord = regUserPw.getText().toString().trim();
				
				ParseUser signUser = new ParseUser();
				signUser.put("firstName", userFname);
				signUser.put("middleName", "");
				signUser.put("lastName", userLname);
				signUser.put("company", userComp);
				signUser.put("position", userPos);
				signUser.put("mobileNo", userNo);
				signUser.setUsername(userName);
				signUser.setEmail(userEmail);
				signUser.setPassword(passWord);
				try {
					signUser.signUpInBackground(new SignUpCallback() {
						@Override
						public void done(com.parse.ParseException e) {
							if(e==null){
								AlertDialog.Builder alert = new AlertDialog.Builder(Registration.this);
								alert.setTitle("Registration Successful.");
								alert.setMessage("Succesfully Register. Please Login to Continue");
								alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										if(ParseUser.getCurrentUser() != null){
											ParseUser.getCurrentUser();
											ParseUser.logOut();
										}
										Intent intent = new Intent(Registration.this, LoginActivity.class);
										startActivity(intent);
										Registration.this.finish();
									}
								});
								alert.show();
							} else {
								AlertDialog.Builder failed = new AlertDialog.Builder(Registration.this);
								failed.setTitle("Error");
								failed.setMessage(e.getMessage());
								failed.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
									}
								});
								failed.show();
							}
						}
					});
				} catch(Exception f){
					AlertDialog.Builder error = new AlertDialog.Builder(Registration.this);
					error.setTitle("Error");
					error.setMessage(f.getMessage());
					error.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
					error.show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
	    case android.R.id.home:
	    	Intent loginActivity = new Intent(this, LoginActivity.class);
	    	invalidateOptionsMenu();
	    	finish();
	    	startActivity(loginActivity);
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
}
