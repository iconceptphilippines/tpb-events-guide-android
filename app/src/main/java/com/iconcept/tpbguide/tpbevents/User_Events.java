package com.iconcept.tpbguide.tpbevents;

import java.util.ArrayList;
import java.util.List;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.Event;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ParseQueryAdapter.OnQueryLoadListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class User_Events extends Activity {
	ListView eventsLists;
	public final static String EXTRA_MESSAGE = "com.tpbguide.tpbevents";
	public CustomListEventsUser listEventsAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user__events);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		getActionBar().setTitle("My Events");
		
		 invalidateOptionsMenu();
	    	if(isNetworkAvailable()) {
	        	connectionList();
	        }else {
	        	 noConnectionList();
	        }
	}
	
	public void noConnectionList() {
		ProgressBar eventsLoading = (ProgressBar) findViewById(R.id.loadingEvent_user);
		eventsLoading.setVisibility(View.GONE);
    	Button btn_reload_list = (Button) findViewById(R.id.btn_retry_load_user);
    	btn_reload_list.setVisibility(View.VISIBLE);
    	
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Connection failed");
        builder.setMessage("Enable mobile network or Wi-Fi to continue using this application.");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            	startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        });
        builder.show();
	}
	public void connectionList() {
    	 // Initialize the subclass of ParseQueryAdapter
    	eventsLists = (ListView) findViewById(R.id.listEventsUser);
    	
		listEventsAdapter = new CustomListEventsUser(this);
		
		listEventsAdapter.addOnQueryLoadListener((new OnQueryLoadListener<ParseObject>() {

			@Override
			public void onLoaded(List<ParseObject> objects, Exception e) {
				// TODO Auto-generated method stub
				ProgressBar eventsLoading = (ProgressBar) findViewById(R.id.loadingEvent_user);
				eventsLoading.setVisibility(View.GONE);
			}

			@Override
			public void onLoading() {
				// TODO Auto-generated method stub
				ProgressBar eventsLoading = (ProgressBar) findViewById(R.id.loadingEvent_user);
				eventsLoading.setVisibility(View.VISIBLE);
			}
			})
		);
		
		eventsLists.setAdapter(listEventsAdapter);
		eventsLists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	        	// selected item
	            String selected = ((TextView) view.findViewById(R.id.eventsTitle)).getText().toString();
	            String eventsObjectId = ((TextView) view.findViewById(R.id.eventId)).getText().toString();
	            
	            Intent intent = new Intent(getApplicationContext(), Event_Single.class);
	            String eventClick = eventsObjectId;
	            intent.putExtra("EVENT_ID", eventClick);
	            startActivity(intent);
        	}
		});
		
	}
	public void btn_load_list_user(View view) {
    	if(isNetworkAvailable()) {
    		connectionList();
    		Button btn_reload_list = (Button) findViewById(R.id.btn_retry_load_user);
        	btn_reload_list.setVisibility(View.GONE);
    	}
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user__events, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        this.finish();
	        return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	private boolean isNetworkAvailable() {
		// TODO Auto-generated method stub
    	ConnectivityManager connectivityManager 
        = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		  return activeNetworkInfo != null;
	}
}
