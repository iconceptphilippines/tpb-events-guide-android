package com.iconcept.tpbguide.tpbevents;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.Schedule;

public class ScheduleListAdapter extends ArrayAdapter<Schedule> {
	Schedule sched = new Schedule();
	
	public ScheduleListAdapter(Context context, String schedId, List<Schedule> schedule){
		super(context, 0, schedule);
		
		sched = new Schedule();
	}
	
	@Override
	public View getView(int position, View v, ViewGroup parent){
		
		if(v==null){
			v = LayoutInflater.from(getContext()).inflate(R.layout.sched_items, parent, false);
			final ViewHolder holder = new ViewHolder();
			
			holder.schedTitle = (TextView) v.findViewById(R.id.schedTitle);
			
			v.setTag(holder);
		}
		
		Schedule sched = (Schedule) getItem(position);
		ViewHolder holder = (ViewHolder) v.getTag();
		
//		if(!sched.getTitle().equals("")){
		SimpleDateFormat date = new SimpleDateFormat("LLLL dd, yyyy - cccc");
		date.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));

		String eventDateSched = date.format(sched.getDate());
		holder.schedTitle.setText(eventDateSched);
//		} else {
//			holder.schedTitle.setVisibility(View.GONE);
//		}
		
		return v;
	}
	
	final class ViewHolder{
		TextView schedTitle;
	}

}
