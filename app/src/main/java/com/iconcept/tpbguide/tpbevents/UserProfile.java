package com.iconcept.tpbguide.tpbevents;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.List;
import com.iconcept.tpbguide.tpbevents.R;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.parse.SaveCallback;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UserProfile extends Activity {
	
	private static final int SELECT_PICTURE = 1;

	private String selectedImagePath;
	private RoundedImageView imageView;
	byte[] image;
	Bitmap bitmap;
	ParseFile file;
	String userId;
	
	TextView userFullName;
	TextView userName;
	TextView userFname;
	TextView userEmail;
	TextView userPhone;
	TextView userLname;
	TextView userMname;
	TextView userPos;
	TextView userComp;
	ParseUser current_user;
	View fname;
	View email;
	View mname;
	View lname;
	View pos;
	View num;
	View comp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(isNetworkAvailable()) {
			setContentView(R.layout.activity_user_profile);
			getActionBar().setDisplayHomeAsUpEnabled(true);
			getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
			final ProgressBar progressProfile = (ProgressBar) findViewById(R.id.progressProfile);
			final LinearLayout detailsLinear = (LinearLayout) findViewById(R.id.line_profile);
			imageView = (RoundedImageView) findViewById(R.id.image_user);
			current_user = ParseUser.getCurrentUser();
			userId = current_user.getObjectId();
			ParseQuery<ParseObject> query = ParseQuery.getQuery("_User");
//			query.whereEqualTo("objectId", userId);
			 query.getInBackground(userId, new GetCallback<ParseObject>() {
				 @Override
	             public void done(ParseObject userProfile, ParseException e) {
	            	 // TODO Auto-generated method stub
	               if (e == null) {
	            	   try {
	            		   progressProfile.setVisibility(View.GONE);
	            		   detailsLinear.setVisibility(View.VISIBLE);
	            		 //set avatar of the current user
		            	   ParseFile imageFile = userProfile.getParseFile("avatar");
		            	   if (imageFile != null) {
		            		   imageView = (RoundedImageView) findViewById(R.id.image_user);
			            	   imageView.setParseFile(imageFile);
			            	   imageView.loadInBackground(new GetDataCallback() {
									@Override
									public void done(byte[] bm, ParseException e) {
										// TODO Auto-generated method stub
										if(e==null){
											
										} else {
											Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
										}
									}
			            	   });
		            	   } else {
		            		   imageView.setImageResource(R.drawable.ic_action_user);
		            		   imageView.setBackgroundResource(R.drawable.roundcorner);
		            	   }
		            	   
		            	   userFullName = (TextView) findViewById(R.id.userFullName);
		            	   userName = (TextView) findViewById(R.id.prof_userName);
		            	   userEmail = (TextView) findViewById(R.id.prof_userEmail);
		            	   userPhone = (TextView) findViewById(R.id.prof_userNo);
		            	   userFname = (TextView) findViewById(R.id.prof_userFname);
		            	   userMname = (TextView) findViewById(R.id.prof_userMname);
		            	   userLname = (TextView) findViewById(R.id.prof_userLname);
		            	   userPos = (TextView) findViewById(R.id.prof_userPos);
		            	   userComp = (TextView) findViewById(R.id.prof_userComp);
		            	   
		            	   fname = (View) findViewById(R.id.fname);
		            	   email = (View) findViewById(R.id.email);
		            	   mname = (View) findViewById(R.id.mname);
		            	   lname = (View) findViewById(R.id.lname);
		            	   pos = (View) findViewById(R.id.pos);
		            	   num = (View) findViewById(R.id.num);
		            	   comp = (View) findViewById(R.id.comp);
		            	   
		            	   String profUserName = userProfile.getString("username").toString();
		            	   String profUserEmail = userProfile.getString("email").toString();
		            	   String profUserNo = userProfile.getString("mobileNo").toString();
		            	   String profUserFname = userProfile.getString("firstName").toString();
		            	   String profUserMname = userProfile.getString("middleName").toString();
		            	   String profUserLname = userProfile.getString("lastName").toString();
		            	   String profUserComp = userProfile.getString("company").toString();
		            	   String profUserPos = userProfile.getString("position").toString();
		            	   String profFullName = profUserFname + " " +profUserLname;
		            	   
		            	   if(!(profUserFname.equals("") && profUserLname.equals(""))){
		            		   userFullName.setText(profFullName);
		            	   }
		            	   if(!profUserName.equals("")) {
		            		   userName.setText(profUserName);
		            	   } else {
		            		   userName.setVisibility(View.GONE);
		            	   }
		            	   if(!profUserEmail.equals("")) {
		            		   userEmail.setText(profUserEmail);
		            	   } else {
		            		   userEmail.setVisibility(View.GONE);
		            		   email.setVisibility(View.GONE);
		            	   }
		            	   if(!profUserFname.equals("")){
		            		   userFname.setText(profUserFname);
		            	   } else {
		            		   userFname.setVisibility(View.GONE);
		            		   fname.setVisibility(View.GONE);
		            	   }
		            	   if(!profUserMname.equals("")){
		            		   userMname.setText(profUserMname);
		            	   } else {
		            		   TextView mn = (TextView) findViewById(R.id.userMname);
		            		   mn.setVisibility(View.GONE);
		            		   userMname.setVisibility(View.GONE);
		            		   mname.setVisibility(View.GONE);
		            	   }
		            	   if(!profUserLname.equals("")){
		            		   userLname.setText(profUserLname);
		            	   } else {
		            		   userLname.setVisibility(View.GONE);
		            		   lname.setVisibility(View.GONE);
		            	   }
		            	   if(!profUserComp.equals("")){
		            		   userComp.setText(profUserComp);
		            	   } else {
		            		   userComp.setVisibility(View.GONE);
		            		   comp.setVisibility(View.GONE);
		            	   }
		            	   if(!profUserNo.equals("")) {
		            		   userPhone.setText(profUserNo);
		            	   } else {
		            		   TextView ph = (TextView) findViewById(R.id.userNo);
		            		   ph.setVisibility(View.GONE);
		            		   userPhone.setVisibility(View.GONE);
		            		   num.setVisibility(View.GONE);
		            	   }
		            	   if(!profUserPos.equals("")){
		            		   userPos.setText(profUserPos);
		            	   } else {
		            		   userPos.setVisibility(View.GONE);
		            		   pos.setVisibility(View.GONE);
		            	   }
//		            	   progressProfile.setVisibility(View.GONE);
	            	   } catch (Exception d){
	            		   d.printStackTrace();
	            	   }
	               }
	             }
	           });
        }else {
        	
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Connection failed");
            builder.setMessage("Enable mobile network or Wi-Fi to continue using this application.");

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                	startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                }
            });
            builder.show();
        }
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_profile, menu);
		return true;
	}
	
	public void changeAvatar(View view){
		 try {
			 Intent intent = new Intent();
			 intent.setType("image/*");
			 intent.setAction(Intent.ACTION_GET_CONTENT);
			 startActivityForResult(Intent.createChooser(intent, "Select Picture"),1);
             image = readInFile(selectedImagePath);
         }
         catch(Exception e) { 
             e.printStackTrace();
         }
	}
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (resultCode == RESULT_OK) {
	        if (requestCode == SELECT_PICTURE) {
	            Uri selectedImageUri = data.getData();
	            if (Build.VERSION.SDK_INT < 19) {
	                selectedImagePath = getPath(selectedImageUri);
	                File f = new File(selectedImagePath);
	                String imageName = f.getName();
	                bitmap = BitmapFactory.decodeFile(selectedImagePath);
	                RoundImage roundedImage = new RoundImage(bitmap);
	                imageView = (RoundedImageView) findViewById(R.id.image_user);
	                imageView.setImageDrawable(roundedImage);
	                ByteArrayOutputStream stream = new ByteArrayOutputStream();
	           	 	bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
	                image = stream.toByteArray();
	                file = new ParseFile("profile-" + imageName+".png", image);
	                file.saveInBackground();
	                
	                ParseQuery<ParseObject> query = ParseQuery.getQuery("_User");
	             // Retrieve the object by id
	                query.getInBackground(userId, new GetCallback<ParseObject>() {
	                	public void done(ParseObject userImage, ParseException e) {
	                		if (e == null) {
//		                	 userImage.put("Image", "picturePath2");
		                	// Create a column named "ImageFile" and insert the image
			                	userImage.put("avatar", file);
			                	userImage.saveInBackground();
			                	imageView.setParseFile(file);
			                	imageView.loadInBackground();
	                		} else {
	                	 
	                		}
	                	}
	                });
	                Toast.makeText(this, imageName + " uploaded successfully.",Toast.LENGTH_SHORT).show();
	            } else {
	            	
	                ParcelFileDescriptor parcelFileDescriptor;
	                try {
	                    parcelFileDescriptor = getContentResolver().openFileDescriptor(selectedImageUri, "r");
	                    FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
	                    Bitmap bmp = BitmapFactory.decodeFileDescriptor(fileDescriptor);
	                    parcelFileDescriptor.close();
	                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
	                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
	                    image = stream.toByteArray();
	                    file = new ParseFile("profile-" + userId+".png", image);
		                file.saveInBackground(new SaveCallback() {
							@Override
							public void done(ParseException e1) {
								// TODO Auto-generated method stub
								if(e1==null){
									imageView.setParseFile(file);
				                	imageView.loadInBackground();
								} else { 
									Toast.makeText(getApplicationContext(), e1.getMessage(), Toast.LENGTH_SHORT).show();
								}
							}
						});
	                    current_user.put("avatar", file);
	                    current_user.saveInBackground(new SaveCallback() {
							@Override
							public void done(ParseException e) {
								// TODO Auto-generated method stub
								if(e==null){
									Toast.makeText(getApplicationContext(), "Profile Picture Updated.", Toast.LENGTH_SHORT).show();
								} else {
									Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
								}
							}
						});
	                    
	                } catch (FileNotFoundException e) {
	                    e.printStackTrace();
	                } catch (IOException e) {
	                    // TODO Auto-generated catch block
	                    e.printStackTrace();
	                }
	            }
	        }
	    }
	}

	/**
	 * helper to retrieve the path of an image URI
	 */
	public String getPath(Uri uri) {
	        if( uri == null ) {
	            return null;
	        }
	        String[] projection = { MediaStore.Images.Media.DATA };
	        Cursor cursor = managedQuery(uri, projection, null, null, null);
	        if( cursor != null ){
	            int column_index = cursor
	            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	            cursor.moveToFirst();
	            return cursor.getString(column_index);
	        }
	        return uri.getPath();
	}

	private byte[] readInFile(String selectedImagePath) throws IOException {
	    // TODO Auto-generated method stub
	    byte[] data = null;
	    File file = new File(selectedImagePath);
	    InputStream input_stream = new BufferedInputStream(new FileInputStream(file));
	    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	    data = new byte[16384]; // 16K
	    int bytes_read;
	    while ((bytes_read = input_stream.read(data, 0, data.length)) != -1) {
	        buffer.write(data, 0, bytes_read);
	    }
	    input_stream.close();
	    return buffer.toByteArray();

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        this.finish();
	        return true;
	    case R.id.action_editProfile:
	    	actionEditProfile();
	    	return true;
	    case R.id.action_profile_qr:
	    	profileQR();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	private void profileQR(){
		Bundle extra = new Bundle();
		Intent profileQR = new Intent(this, MyProfileQRCode.class);
		ParseUser user = ParseUser.getCurrentUser();
		extra.putString("OBJECT_ID", user.getObjectId());
		extra.putString("ACTION_NAME", "addContact");
		extra.putString("ACTION_TITLE", "My Profile QR Code");
		
		profileQR.putExtras(extra);
		invalidateOptionsMenu();
		startActivity(profileQR);
	}
	
	private void actionEditProfile() {
//		Toast.makeText(getApplicationContext(),"Change Email",Toast.LENGTH_LONG).show();
		Intent editProfile = new Intent(this, EditProfile.class);
    	invalidateOptionsMenu();
    	finish();
    	startActivity(editProfile);
		
	}
	private boolean isNetworkAvailable() {
		// TODO Auto-generated method stub
    	ConnectivityManager connectivityManager 
        = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		  return activeNetworkInfo != null;
	}
}
