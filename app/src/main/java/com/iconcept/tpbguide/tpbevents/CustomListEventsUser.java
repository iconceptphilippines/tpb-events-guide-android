package com.iconcept.tpbguide.tpbevents;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.Event;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

public class CustomListEventsUser extends ParseQueryAdapter<ParseObject>{
	public CustomListEventsUser(Context context) {
		super(context, new ParseQueryAdapter.QueryFactory<ParseObject>() {
			
			public ParseQuery create() {
				ParseQuery query = new ParseQuery("EventAttendee");
				query.include("event");
				query.whereEqualTo("user", ParseUser.getCurrentUser());
				return query;
			}
		});
	}
	
		@Override
		public View getItemView(ParseObject object, View v, ViewGroup parent) {
			if (v == null) {
				v = View.inflate(getContext(), R.layout.custom_list_events, null);
			}
			super.getItemView(object, v, parent);
			Event event = (Event) object.getParseObject("event");
			
			ParseImageView eventsImage = (ParseImageView) v.findViewById(R.id.eventsImage);
			ParseFile imageFile = event.getParseFile("image");
			if (imageFile != null) {
				eventsImage.setParseFile(imageFile);
				eventsImage.loadInBackground();
			}

			TextView titleEvents = (TextView) v.findViewById(R.id.eventsTitle);
			titleEvents.setText(event.getString("title"));
			
			TextView eventsObjectId = (TextView) v.findViewById(R.id.eventId);
			eventsObjectId.setText(event.getObjectId());
			
			TextView eventId = (TextView) v.findViewById(R.id.eventsObjectId);
			
			
			String status = event.getString("status");
			if(status.equalsIgnoreCase("editor") || status == "editor"){
				eventId.setText("- Editor");
			} else {
				eventId.setVisibility(View.GONE);
			}
			

			// Add a reminder of how long this item has been outstanding
//			TextView excerptEvents = (TextView) v.findViewById(R.id.eventsExcerpt);
//			timestampView.setText(object.getDate("Date").toString());
//			excerptEvents.setText(object.getString("details"));
			
			return v;
		}
}