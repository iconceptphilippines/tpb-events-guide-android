package com.iconcept.tpbguide.tpbevents;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import com.iconcept.tpbguide.tpbevents.R;

public class SplashPage extends Activity {

    private static int SPLASH_TIME_OUT = 2000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_splash_page);
		 new Handler().postDelayed(new Runnable() {

	            @Override
	            public void run() {
	                Intent i = new Intent(SplashPage.this, Events_List.class);
	                startActivity(i);
	                finish();
	            }
	        }, SPLASH_TIME_OUT);
	}
}
