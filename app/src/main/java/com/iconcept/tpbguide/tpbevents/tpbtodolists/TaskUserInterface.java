package com.iconcept.tpbguide.tpbevents.tpbtodolists;

import com.parse.ParseUser;
import com.iconcept.tpbguide.tpbevents.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TaskUserInterface extends Activity{

	Button buttonLogout,buttonViewTasks;
	TextView currUser;
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_user_interface);
        getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        
        buttonLogout = (Button) findViewById(R.id.btnLogout);
        buttonViewTasks = (Button) findViewById(R.id.btnViewTasks);
        
        currUser = (TextView) findViewById(R.id.lblCurrentUser);
        
        btnViewTaskClick();
        btnlogoutClick();
        currentUser();
        
	}
	
	public void btnlogoutClick() {
	   	   
		 buttonLogout.setOnClickListener(new OnClickListener() {
	     @Override
	     	 public void onClick(View arg0) {
	                	
	    	 	/*ParseUser.logOut();
	    	 	Intent openNewActivity = new Intent(getApplicationContext(), ToDoListsUI.class);
	    	 	startActivity(openNewActivity);
	    	 	finish();		*/
	         }
	     });
	 }
	
	 public void btnViewTaskClick() {
	   	   
		 buttonViewTasks.setOnClickListener(new OnClickListener() {
	     @Override
	     	 public void onClick(View arg0) {
		         try{
		        	 Intent openNewActivity = new Intent(getApplicationContext(), TaskLayout.class);
		        	 startActivity(openNewActivity);
		        	 finish();
	
		         } catch(Exception e) {
		        	 
		         }
	         }
	     });
	 }
	
	 private void currentUser(){
    	 ParseUser currentUser = ParseUser.getCurrentUser();
    	 if (currentUser != null) {
    		 currUser.setText(currentUser.getUsername().toString());
    	 }
    }
	 
}
