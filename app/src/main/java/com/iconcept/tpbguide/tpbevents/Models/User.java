package com.iconcept.tpbguide.tpbevents.Models;

import com.parse.ParseFile;
import com.parse.ParseUser;

public class User extends ParseUser{
	public String getFname(){
		return getString("firstName");
	}
	public String getLname(){
		return getString("lastName");
	}
	public ParseFile getAvatar(){
		return getParseFile("avatar");
	}
}
