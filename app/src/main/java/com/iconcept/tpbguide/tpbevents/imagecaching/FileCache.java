package com.iconcept.tpbguide.tpbevents.imagecaching;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

public class FileCache {

	private File cacheDir;

	public FileCache(Context context) {
		// Find the dir to save cached images
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED))
			cacheDir = new File(
					android.os.Environment.getExternalStorageDirectory(),
					"ParseGridView");
		else
			cacheDir = context.getCacheDir();
		if (!cacheDir.exists())
			cacheDir.mkdirs();
	}

	public File getFile(String url) {
		String filename = String.valueOf(url.hashCode());
		// String filename = URLEncoder.encode(url);
		File f = new File(cacheDir, filename);
		return f;

	}

	public void clear() {
		File[] files = cacheDir.listFiles();
		if (files == null)
			return;
		for (File f : files)
			f.delete();
	}
	public void BitmapConversion(Bitmap bmp){
		File myDir = new File(Environment.getExternalStorageDirectory() + "/tpb_cache"); 
		if(!myDir.exists()){
			myDir.mkdirs();	
		}
		Random rand = new Random();
		int n = rand.nextInt(1000);
		fname = "imageName"+n+".jpg";
		File file = new File (myDir, fname);
		if (file.exists ()) file.delete (); 
		try {
		       FileOutputStream out = new FileOutputStream(file);
		       bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
		       out.flush();
		       out.close();
		       bmp.recycle();
		} catch (Exception e) {
		       e.printStackTrace();
		}
	}
	private static String fname;

	public static String getFname() {
		return fname;
	}

}
