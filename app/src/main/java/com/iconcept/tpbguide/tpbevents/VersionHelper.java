package com.iconcept.tpbguide.tpbevents;

import android.app.Activity;

class VersionHelper
{
    static void refreshActionBarMenu(Activity activity)
    {
        activity.invalidateOptionsMenu();
    }
}