package com.iconcept.tpbguide.tpbevents;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.Announcements;

public class AnnouncementAdapter extends ArrayAdapter<Announcements> {
	private Announcements announcement;
	private String size;
	
	public AnnouncementAdapter(Context context, String eventId, List<Announcements> announcementList) {
		super(context, 0, announcementList);
		announcement = new Announcements();
	}
	
	@Override
	public View getView(final int position, View v, final ViewGroup parent){
		if(v==null){
			v = LayoutInflater.from(getContext()).inflate(R.layout.announce_items, parent, false);
			final ViewHolder holder = new ViewHolder();
			holder.announceTitle = (TextView) v.findViewById(R.id.announceTitle);
			v.setTag(holder);
		}
		 final Announcements announcement = (Announcements) getItem(position);
		 final ViewHolder holder = (ViewHolder) v.getTag();
		 holder.announceTitle.setText(announcement.getTitle());
		return v;
	}
	 class ViewHolder{
		TextView announceTitle;
	}
}
