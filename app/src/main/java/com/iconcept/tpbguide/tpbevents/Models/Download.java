package com.iconcept.tpbguide.tpbevents.Models;

import java.util.Date;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

@ParseClassName("File")
public class Download extends ParseObject{
	public ParseFile getFile(){
		return getParseFile("file");
	}
	public String getFileName(){
		return getString("fileName");
	}
	public Double getFileSize(){
		return getDouble("fileSize");
	}
	public String getFileSource(){
		return getString("fileSource");
	}
	public String getFileType(){
		return getString("fileType");
	}
	public String getFileUrl(){
		return getString("fileURL");
	}
	public Date getDateCreated(){
		return getCreatedAt();
	}
}
