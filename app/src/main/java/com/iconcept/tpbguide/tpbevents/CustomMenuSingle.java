package com.iconcept.tpbguide.tpbevents;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.iconcept.tpbguide.tpbevents.R;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

public class CustomMenuSingle extends ParseQueryAdapter<ParseObject>{
	public CustomMenuSingle(Context context) {
		super(context, new ParseQueryAdapter.QueryFactory<ParseObject>() {
			
			public ParseQuery create() {
				ParseQuery query = new ParseQuery("Menu");
				ParseQuery<ParseObject> current = ParseQuery.getQuery("Event");
//				query.whereEqualTo("objectId", current.find());
				return query;
			}
		});
	}
		@Override
		public View getItemView(ParseObject object, View v, ViewGroup parent) {
			if (v == null) {
				v = View.inflate(getContext(), R.layout.custom_menu_list, null);
			}
			LinearLayout list = new LinearLayout(getContext());			
			
			for(int count=0; count<12; count++){
				TextView menu = (TextView) v.findViewById(R.id.menulist);
				menu.setText(object.getString("title"));
				list.addView(menu);
			}
			return v;
		}
}