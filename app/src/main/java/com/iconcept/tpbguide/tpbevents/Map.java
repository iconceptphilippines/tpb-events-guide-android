package com.iconcept.tpbguide.tpbevents;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import android.*;
import android.Manifest;
import android.R.drawable;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.Announcements;
import com.iconcept.tpbguide.tpbevents.Models.Download;
import com.iconcept.tpbguide.tpbevents.Models.Schedule;
import com.iconcept.tpbguide.tpbevents.imagecaching.GridViewActivity;
import com.iconcept.tpbguide.tpbevents.imagecaching.GridViewAdapter;
import com.iconcept.tpbguide.tpbevents.imagecaching.ViewPagerActivity;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

public class Map extends Activity implements OnMapReadyCallback {

	private final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 310161;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			int v = getPackageManager().getPackageInfo("com.google.android.gms", 0 ).versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}

		try{
			setContentView(R.layout.activity_map);
			getActionBar().setDisplayHomeAsUpEnabled(true);
			getActionBar().setDisplayShowTitleEnabled(false);
			getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

			MapFragment fm = (MapFragment) getFragmentManager().findFragmentById(R.id.locationMap);
			fm.getMapAsync(this);
		}catch(NullPointerException ex){
			Log.d("MAP_ERROR", ex.getMessage());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onMapReady(GoogleMap map) {
		String latitude = getIntent().getStringExtra("LATITUDE");
		String longitude = getIntent().getStringExtra("LONGITUDE");

		Double lati = Double.parseDouble(latitude);
		Double longi = Double.parseDouble(longitude);

//		ParseGeoPoint point = new ParseGeoPoint(lati,longi);
//		Double lat = point.getLatitude();
//		Double lon = point.getLongitude();
		LatLng latLng = new LatLng(lati, longi);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				if (checkSelfPermission(Manifest.permission.CALL_PRIVILEGED) != PackageManager.PERMISSION_GRANTED) {
//											if (ActivityCompat.shouldShowRequestPermissionRationale(getParent(),
//													Manifest.permission.READ_CONTACTS)) {
					ActivityCompat.requestPermissions(Map.this,
							new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},
							MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
					return;
				}
				return;
			}
		}

		map.setMyLocationEnabled(true);
		map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,16));
		// Zoom in the Google Map
//		String add = object.getString("addressLine1") + " " + object.getString("city") + ", " + object.getString("stateOrProvince") + " " + object.getString("zipCode");
		map.addMarker(new MarkerOptions().position(latLng));
	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {

					// permission was granted, yay! Do the
					// contacts-related task you need to do.
					MapFragment fm = (MapFragment) getFragmentManager().findFragmentById(R.id.locationMap);
					fm.getMapAsync(this);
				} else {

					// permission denied, boo! Disable the
					// functionality that depends on this permission.
				}
				return;
			}

			// other 'case' lines to check for other
			// permissions this app might request
		}
	}
}
