package com.iconcept.tpbguide.tpbevents;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.iconcept.tpbguide.tpbevents.R;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

public class CustomInfoList extends ParseQueryAdapter<ParseObject> {
	public CustomInfoList(Context context) {
		super(context, new ParseQueryAdapter.QueryFactory<ParseObject>() {			
			public ParseQuery create() {
				ParseQuery query = new ParseQuery("Post");
				query.whereEqualTo("objectId", "C6wdR6y7Og");
				ParseQuery<?> inner = new ParseQuery("Post");
				inner.whereMatchesQuery("parent", query);
				return inner;
			}
		});
	}
		@Override
		public View getItemView(ParseObject object, View v, ViewGroup parent) {
			if (v == null) {
				v = View.inflate(getContext(), R.layout.custom_list_items_sub, null);
			}
			super.getItemView(object, v, parent);
			ParseImageView infoimg = (ParseImageView) v.findViewById(R.id.itemImg);
			ParseFile imageFile = object.getParseFile("image");
			if (imageFile != null) {
				infoimg.setParseFile(imageFile);
				infoimg.loadInBackground();
			}
			TextView infotitle = (TextView) v.findViewById(R.id.txtinfo);
			infotitle.setText(object.getString("title"));
			return v;
			
			
		}
}
