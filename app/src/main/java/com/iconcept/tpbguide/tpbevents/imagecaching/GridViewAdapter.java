package com.iconcept.tpbguide.tpbevents.imagecaching;

import java.util.ArrayList;
import java.util.List;

import com.iconcept.tpbguide.tpbevents.R;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;


public class GridViewAdapter extends BaseAdapter {

	Context context;
	LayoutInflater inflater;
	ImageLoader imageLoader;
	private List<ImageLists> imagearraylist = null;
	private ArrayList<ImageLists> arraylist;

	public GridViewAdapter(Context context, List<ImageLists> imagearraylist) {
		this.context = context;
		this.imagearraylist = imagearraylist;
		inflater = LayoutInflater.from(context);
		this.arraylist = new ArrayList<ImageLists>();
		this.arraylist.addAll(imagearraylist);
		imageLoader = new ImageLoader(context);
	}

	public class ViewHolder {
		ImageView phone;
	}

	@Override
	public int getCount() {
		return imagearraylist.size();
	}

	@Override
	public Object getItem(int position) {
		return imagearraylist.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup parent) {
		final ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.gridview_item, null);
			holder.phone = (ImageView) view.findViewById(R.id.grid_image);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		imageLoader.DisplayImage(imagearraylist.get(position).getImagelist(),
				holder.phone);
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(context, ViewPagerActivity.class);	
				ViewPagerActivity.setId(id);
				ViewPagerActivity.setPosition(position);
				context.startActivity(intent);
			}
		});
		return view;
	}
	public static String id;
	public static void setId(String id){
		GridViewAdapter.id = id;
	}
}
