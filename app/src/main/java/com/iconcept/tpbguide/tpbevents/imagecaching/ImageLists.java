package com.iconcept.tpbguide.tpbevents.imagecaching;

public class ImageLists {
	
	private String imagelist;

	public String getImagelist() {
		return imagelist;
	}

	public void setImagelist(String imagelist) {
		this.imagelist = imagelist;
	}

}
