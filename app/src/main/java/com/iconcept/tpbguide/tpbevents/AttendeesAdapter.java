package com.iconcept.tpbguide.tpbevents;

import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import com.iconcept.tpbguide.tpbevents.R;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseObject;

public class AttendeesAdapter extends ArrayAdapter<ParseObject>{
	
	public AttendeesAdapter (Context context, List<ParseObject> users){
		super(context, 0, users);
	}
	
	@Override
	public View getView(int position, View v, ViewGroup parent){
		RoundedImageView userImage;
		TextView fullName;
		v = LayoutInflater.from(getContext()).inflate(R.layout.attendees_items, parent,false);
		fullName = (TextView) v.findViewById(R.id.attendeeName);
		userImage = (RoundedImageView) v.findViewById(R.id.attendeeImage);
		v.setTag(new ViewHolder(userImage, fullName));
		ParseObject user = (ParseObject) getItem(position);
		final ViewHolder holder = (ViewHolder) v.getTag();
		String fname = user.getString("firstName");
		String lname = user.getString("lastName");
		String completeName = fname + " " + lname;
		holder.fullName.setText(completeName);
		if(user.getParseFile("avatar") == null || user.getParseFile("avatar").equals("")){
			holder.userImage.setImageResource(R.drawable.ic_action_person);
			holder.userImage.setBackgroundResource(R.drawable.roundcorner);
			holder.userImage.setScaleType(ScaleType.FIT_XY);
		} else {
			holder.userImage.setParseFile(user.getParseFile("avatar"));
			holder.userImage.setScaleType(ScaleType.CENTER_CROP);
			holder.userImage.loadInBackground(new GetDataCallback() {
				@Override
				public void done(byte[] arg0, ParseException e) {
					// TODO Auto-generated method stub
					if(e != null){
						Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
						holder.userImage.setVisibility(View.GONE);
					}
 				}
			});
		}
		return v;
	}
	final class ViewHolder{
		TextView fullName;
		RoundedImageView userImage;
		public ViewHolder(RoundedImageView userImage, TextView fullName) {
	        this.userImage = userImage;
	        this.fullName = fullName;
	    }
	}
}
