package com.iconcept.tpbguide.tpbevents;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.v4.app.ActivityCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.iconcept.tpbguide.tpbevents.R;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ParseObject;
import com.parse.ParseRelation;
import com.parse.SaveCallback;
import com.parse.FindCallback;
import java.util.List;

public class AttendeesProfile extends Activity {

	private RelativeLayout attendeesProfileRel;
	private ProgressBar attendeesProfileProg;
	private final int ADD_CONTACT = 0;
	private final int DELETE_CONTACT = 1;
	private int contactFlag;
	private int DELETE_SPECIFIC;
	private Button addContact, message, emailme, call;
	private String dialogTitle;
	private String userID;
	private TextView userId, name,uName,nplace,com,cplace,pos,pplace,email,eplace,phone,phplace;
	private ArrayList<JSONObject> list = new ArrayList<JSONObject>();
	private ParseUser currentUser;
	private ParseObject userObject;
	private JSONArray jsonArray;
	private final int MY_PERMISSIONS_REQUEST_CALL_PRIVILEGE = 31016;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_attendees_profile);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		getActionBar().setDisplayShowTitleEnabled(false);

		Intent intent = getIntent();
		userID = intent.getStringExtra("UserId");
		userId = (TextView) findViewById(R.id.userId);
		name = (TextView) findViewById(R.id.FirstName);
		uName = (TextView) findViewById(R.id.userName);
		nplace = (TextView) findViewById(R.id.nPlace);
		com = (TextView) findViewById(R.id.comp);
		cplace = (TextView) findViewById(R.id.cplacholder);
		pos = (TextView) findViewById(R.id.position);
		pplace = (TextView) findViewById(R.id.pplaceholder);
		email = (TextView) findViewById(R.id.email);
		eplace = (TextView) findViewById(R.id.eplaceholder);
		phone = (TextView) findViewById(R.id.phone);
		phplace = (TextView) findViewById(R.id.phplaceholder);
		addContact = (Button) findViewById(R.id.addContact);
		message = (Button) findViewById(R.id.message);
		emailme = (Button) findViewById(R.id.em);
		call = (Button) findViewById(R.id.call);
		attendeesProfileProg = (ProgressBar) findViewById(R.id.attendProfileProg);
		attendeesProfileRel = (RelativeLayout) findViewById(R.id.relAttendProfileDetails);

		currentUser = ParseUser.getCurrentUser();

		if (currentUser != null && currentUser.getObjectId().equals(userID)) {

			attendeesProfileProg.setVisibility(View.GONE);
			attendeesProfileRel.setVisibility(View.VISIBLE);
			String own = currentUser.getString("username");
			String fname = currentUser.getString("firstName");
			String lname = currentUser.getString("lastName");
			String comp = currentUser.getString("company");
			String position = currentUser.getString("position");
			String emailadd = currentUser.getString("email");
			String phones = currentUser.getString("mobileNo");

			ParseFile avatar = currentUser.getParseFile("avatar");
			RoundedImageView img = (RoundedImageView) findViewById(R.id.avatar);
			if (avatar != null) {
				img.setParseFile(avatar);
				img.loadInBackground();
			} else {
				img.setImageResource(R.drawable.default_user);
			}
			name.setText("You");
			uName.setText(own);
			addContact.setVisibility(View.GONE);
			userId.setText(fname + " " + lname);
			nplace.setText("NAME");
			com.setText(comp);
			cplace.setText("COMPANY");
			pos.setText(position);
			pplace.setText("POSITION");
			email.setText(emailadd);
			eplace.setText("EMAIL");
			phone.setText(phones);
			phplace.setText("PHONE");
		} else {
			ParseQuery<ParseUser> other = ParseQuery.getQuery("_User");
			other.getInBackground(userID, new GetCallback<ParseUser>() {
				@Override
				public void done(final ParseUser user, ParseException e) {
					if (e == null) {
						attendeesProfileProg.setVisibility(View.GONE);
						attendeesProfileRel.setVisibility(View.VISIBLE);
						userObject = user;
						String fname = user.getString("firstName");
						String lname = user.getString("lastName");
						String uname = user.getString("username");
						String co = user.getString("company");
						String po = user.getString("position");
						final String em = user.getString("email");
						final String ph = user.getString("mobileNo");
						ParseFile avatar = user.getParseFile("avatar");
						final RoundedImageView img = (RoundedImageView) findViewById(R.id.avatar);
						if (avatar != null) {
							img.setParseFile(avatar);
							img.loadInBackground(new GetDataCallback() {
								@Override
								public void done(byte[] byt, ParseException e) {
									if(e != null){
										Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
									}
								}
							});
						} else {
							img.setImageResource(R.drawable.ic_action_person);
							img.setBackgroundResource(R.drawable.roundcorner);
						}
						name.setText(fname + " " + lname);
						uName.setText(uname);
						final String attendeeId = user.getObjectId();
						jsonArray = currentUser.getJSONArray("contacts");
						addContact.setText("LOADING...");
						contactFlag = 0;

						ParseRelation relation = currentUser.getRelation("contacts");
						ParseQuery checkQuery = relation.getQuery();
						checkQuery.findInBackground(new FindCallback<ParseUser>() {
							@Override
							public void done(List<ParseUser> contactUsers, ParseException e)
							{
								if( e == null )
								{
									for( ParseUser contactUser : contactUsers )
									{
										if( String.valueOf(contactUser.getObjectId()).equals(userID) )
										{
											addContact.setText("DELETE CONTACT");
											contactFlag = 1;
											break;
										}else{
											addContact.setText("ADD CONTACT");
										}
									}
								}else{
									Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
								}
							}
						});

						addContact.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if( addContact.getText().equals("LOADING...") )
								{
									return;
								}
								if (contactFlag == ADD_CONTACT) {
									dialogTitle = "Are you sure you want to add this person?";
								} else if (contactFlag == DELETE_CONTACT) {
									dialogTitle = "Are you sure you want to delete this from you contacts?";
								}
								alertDialog();
							}
						});

						userId.setText(fname + " " + lname);
						nplace.setText("NAME");
						com.setText(co);
						cplace.setText("COMPANY");
						pos.setText(po);
						pplace.setText("POSITION");
						email.setText(em);
						eplace.setText("EMAIL");
						if (ph != null) {
							phone.setText(ph);
							phplace.setText("PHONE");
							call.setVisibility(View.VISIBLE);
							call.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									Intent call = new Intent(Intent.ACTION_CALL);
									call.setData(Uri.parse("tel:" + ph));

									if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
										if (checkSelfPermission(Manifest.permission.CALL_PRIVILEGED) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//											if (ActivityCompat.shouldShowRequestPermissionRationale(getParent(),
//													Manifest.permission.READ_CONTACTS)) {
											ActivityCompat.requestPermissions(AttendeesProfile.this,
													new String[]{Manifest.permission.CALL_PRIVILEGED,Manifest.permission.CALL_PHONE},
													MY_PERMISSIONS_REQUEST_CALL_PRIVILEGE);
											return;
										}
									}

									startActivity(call);
								}
							});
						}
						message.setVisibility(View.VISIBLE);
						message.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								Intent i = new Intent(getApplicationContext(),
										Messaging.class);
								i.putExtra("userid", userID);
								startActivity(i);
							}
						});
						emailme.setVisibility(View.VISIBLE);
						emailme.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								Intent emailto = new Intent(Intent.ACTION_SENDTO);
								emailto.setData(Uri.parse("mailto:" + em));
								startActivity(emailto);
							}
						});
					} else {
						addContact.setText("ADD CONTACT");
					}
				}
			});
		}

	}
	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_CALL_PRIVILEGE: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {

					// permission was granted, yay! Do the
					// contacts-related task you need to do.
					call.performClick();
				}
				return;
			}

			// other 'case' lines to check for other
			// permissions this app might request
		}
	}
	private void alertDialog() {
		Dialog dialog;
		AlertDialog.Builder alBuilder = new AlertDialog.Builder(this);
		alBuilder.setTitle(dialogTitle);
		alBuilder.setNegativeButton("YES",new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (contactFlag == ADD_CONTACT) {
					ParseRelation relation = currentUser.getRelation("contacts");
					relation.add(userObject);
					currentUser.saveInBackground(new SaveCallback() {
						@Override
						public void done(ParseException arg0) {
							String saveMessage = "UNSUCCESSFUL...";
							if (arg0 == null) {
								saveMessage = "SUCCESSFULLY ADDED...";
								addContact.setText("DELETE CONTACT");
								contactFlag = 1;
								Toast.makeText(getApplicationContext(),saveMessage,Toast.LENGTH_SHORT).show();
							}else{
								Toast.makeText(getApplicationContext(),saveMessage,Toast.LENGTH_SHORT).show();
							}
						}
					});
				}
				if (contactFlag == DELETE_CONTACT) {
					ParseRelation relation = currentUser.getRelation("contacts");
					relation.remove(userObject);
					currentUser.saveInBackground(new SaveCallback() {
						@Override
						public void done(ParseException arg0) {
							String deleteMessage = "CONTACT NOT DELETED...";
							if (arg0 == null) {
								deleteMessage  = "SUCESSFULLY DELETED...";
								addContact.setText("ADD CONTACT");
								contactFlag = 0;
								Toast.makeText(getApplicationContext(), deleteMessage,	Toast.LENGTH_SHORT).show();
							}else{
								Toast.makeText(getApplicationContext(), deleteMessage,	Toast.LENGTH_SHORT).show();
							}
						}
					});
				}
			}
		});
		alBuilder.setPositiveButton("NO",new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialog = alBuilder.create();
		dialog.show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
