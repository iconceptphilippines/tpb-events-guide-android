package com.iconcept.tpbguide.tpbevents;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.DownloadAdapter.ViewHolder;
import com.iconcept.tpbguide.tpbevents.Models.Download;
import com.iconcept.tpbguide.tpbevents.Models.User;

public class UserAdapter extends ArrayAdapter<User>{

	User user;
	public UserAdapter(Context context, String menuId, List<User> userList) {
		super(context, 0, userList);
		// TODO Auto-generated constructor stub

		user = new User();
	}
	@Override
	public View getView(int position, View v, ViewGroup parent){
		if(v==null){
			v = LayoutInflater.from(getContext()).inflate(R.layout.attendees_list, parent, false);
            final ViewHolder holder = new ViewHolder();
            holder.attendeesName = (TextView) v.findViewById(R.id.attendeeName);
            holder.avatar = (RoundedImageView) v.findViewById(R.id.attendeeImage);
            v.setTag(holder);
		}
		User user = (User) getItem(position);
		ViewHolder holder = (ViewHolder) v.getTag();
		
		String fname = user.getFname();
		String lname = user.getLname();
		
		holder.attendeesName.setText(fname + " " + lname);
		
		if(!user.getAvatar().equals("")){
			holder.avatar.setParseFile(user.getAvatar());
			holder.avatar.loadInBackground();
		} else {
			
		}
		return v;
		
	}
	
	final class ViewHolder {
		TextView attendeesName;
		RoundedImageView avatar;
	}
}
