package com.iconcept.tpbguide.tpbevents;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.ScheduleSlot;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseImageView;
import com.parse.ParseQuery;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ScheduleDetails extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getActionBar().setDisplayShowTitleEnabled(false);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		setContentView(R.layout.activity_schedule_details);
		
		Intent i = getIntent();
		String slotId = i.getStringExtra("SCHED_SLOT_OBJID");
		
		ParseQuery<ScheduleSlot> query = ParseQuery.getQuery(ScheduleSlot.class);
		query.whereEqualTo("objectId", slotId);
		query.getInBackground(slotId, new GetCallback<ScheduleSlot>(){
			
			@Override
			public void done(final ScheduleSlot slot, ParseException e){
				if(e==null){
					ProgressBar prog = (ProgressBar) findViewById(R.id.progressBar1);
					prog.setVisibility(View.GONE);
					RelativeLayout relayout = (RelativeLayout) findViewById(R.id.rel_details);
					relayout.setVisibility(View.VISIBLE);
					
					ParseImageView img = (ParseImageView) findViewById(R.id.activityImg);
					RelativeLayout locLayout = (RelativeLayout) findViewById(R.id.rel_loc_info);
					TextView schedDetailTitle = (TextView) findViewById(R.id.schedDetailTitle);
					TextView schedDetailDate = (TextView) findViewById(R.id.schedDetailDate);
					TextView schedDetaillocation = (TextView) findViewById(R.id.schedDetaillocation);
					WebView schedDetailContent = (WebView) findViewById(R.id.schedDetailInfo);
					
					img.setVisibility(View.GONE);
					
					SimpleDateFormat startTime = new SimpleDateFormat("hh:mm a",Locale.ENGLISH);
					SimpleDateFormat endTime = new SimpleDateFormat("hh:mm a",Locale.ENGLISH);
					startTime.setTimeZone(TimeZone.getTimeZone("GMT"));
					endTime.setTimeZone(TimeZone.getTimeZone("GMT"));
					
					String schedDetailStartTime;
					String schedDetailEndTime;
					
					schedDetailTitle.setText(slot.getTitle());
					
					String htmlText = "<html><body style=\"text-align:justify;font-size:12px;color:#000;\"> %s </body></Html>";
					if(slot.getContent() == null){
						schedDetailContent.setVisibility(View.GONE);
					} else {
						String myData = slot.getContent();
						schedDetailContent.setBackgroundColor(0x00000000);
						schedDetailContent.getSettings().setJavaScriptEnabled(true);
						schedDetailContent.loadDataWithBaseURL("", String.format(htmlText, myData), "text/html", "UTF-8", "");
					}
					
					if(slot.getEndTime() == null){
						schedDetailStartTime = startTime.format(slot.getStartTime());
						schedDetailDate.setText(schedDetailStartTime);
					} else {
						schedDetailStartTime = startTime.format(slot.getStartTime());
						schedDetailEndTime = endTime.format(slot.getEndTime());
						String time = schedDetailStartTime + " - " + schedDetailEndTime;
						schedDetailDate.setText(time);
					}
					
					if(slot.getAddress() == null){
						locLayout.setVisibility(View.GONE);
					} else {
						Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
						schedDetaillocation.setTypeface(font);
						schedDetaillocation.setText(getString(R.string.fa_map_marker)+ "   " + slot.getAddress());
						if(slot.getLocation() != null){
							schedDetaillocation.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									Double lat = slot.getLocation().getLatitude();
									Double lon = slot.getLocation().getLongitude();
									String latitude = String.valueOf(lat);
									String longitude = String.valueOf(lon);
									Intent intent = new Intent(getApplicationContext(), Map.class);
									
									intent.putExtra("LATITUDE", latitude);
									intent.putExtra("LONGITUDE", longitude);
									startActivity(intent);
									Toast.makeText(getApplicationContext(), latitude, Toast.LENGTH_SHORT).show();
								}
							});
						}
					}
					
				} else {
					Toast.makeText(getApplicationContext(), "Unable to Fetch Data!", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.schedule_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
