package com.iconcept.tpbguide.tpbevents;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class MyProfileQRCode extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(true);
		setContentView(R.layout.activity_my_profile_qrcode);
		
		Bundle intent = getIntent().getExtras();
		
		String objectId = intent.getString("OBJECT_ID");
		String action = intent.getString("ACTION_NAME");
		String title = intent.getString("ACTION_TITLE");
		getActionBar().setTitle(title);
		QRCodeWriter writer = new QRCodeWriter();
		String generatedCode = "ACTION:"+action+";OBJECTID:"+objectId;
	    try {
	        BitMatrix bitMatrix = writer.encode(generatedCode, BarcodeFormat.QR_CODE, 512, 512);
	        int width = bitMatrix.getWidth();
	        int height = bitMatrix.getHeight();
	        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
	        for (int x = 0; x < width; x++) {
	            for (int y = 0; y < height; y++) {
	                bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
	            }
	        }
	        ((ImageView) findViewById(R.id.qrImage)).setImageBitmap(bmp);

	    } catch (WriterException e) {
	        e.printStackTrace();
	    }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my_profile_qrcode, menu);
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        this.finish();
	        return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
}
