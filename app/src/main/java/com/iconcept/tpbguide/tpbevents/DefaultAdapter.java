package com.iconcept.tpbguide.tpbevents;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iconcept.tpbguide.tpbevents.R;
import com.parse.ParseImageView;
import com.parse.ParseObject;

public class DefaultAdapter extends ArrayAdapter<ParseObject>{

	public DefaultAdapter(Context context, List<ParseObject> objects) {
		super(context, 0, objects);
		
	}
	public View getView(int position, View v, ViewGroup parent){
		if(v==null){
			v = LayoutInflater.from(getContext()).inflate(R.layout.list_items_sub, parent, false);
			final ViewHolder holder = new ViewHolder();
			holder.txt = (TextView) v.findViewById(R.id.itemsTitle);
			holder.subTxt = (TextView) v.findViewById(R.id.itemsSubTitle);
			holder.img = (ParseImageView) v.findViewById(R.id.itemsImg);
			holder.viewHolder = (RelativeLayout) v.findViewById(R.id.holderLayout);
			v.setTag(holder);
		}
		ParseObject ob = (ParseObject) getItem(position);
		ViewHolder holder = (ViewHolder) v.getTag();
		
		holder.txt.setText(ob.getString("title"));
		if(ob.getParseFile("thumbnail") != null){
			holder.img.setParseFile(ob.getParseFile("thumbnail"));
			holder.img.loadInBackground();
		} else {
			holder.viewHolder.setVisibility(View.GONE);
		}
		if(ob.getString("subtitle") != null ){
			holder.subTxt.setText(ob.getString("subtitle"));
		} else {
			holder.subTxt.setVisibility(View.GONE);
		}
		return v;
	}
	final class ViewHolder{
		TextView txt;
		TextView subTxt;
		ParseImageView img;
		RelativeLayout viewHolder;
	}

}
