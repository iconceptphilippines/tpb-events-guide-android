package com.iconcept.tpbguide.tpbevents.imagecaching;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.iconcept.tpbguide.tpbevents.Event_Single;
import com.iconcept.tpbguide.tpbevents.R;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

public class ViewPagerActivity extends Activity {
	private ViewPager viewPager;
	private List<ParseObject> ob;
	private ProgressBar progressBar;
	private ViewPagerAdapter adapter;
	private List<ImageLists> imagearraylist = null;
	private GestureDetector tapGestureDetector;
	private CallbackManager callbackManager;
	private String hashTag = null;
	private String event_id;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		FacebookSdk.sdkInitialize(getApplicationContext());
		callbackManager = CallbackManager.Factory.create();
		setContentView(R.layout.viewpager_main);
		progressBar = (ProgressBar)findViewById(R.id.loadingPhotos);
		event_id = Event_Single.getEvent_id();
		getActionBar().setDisplayHomeAsUpEnabled(true);		
		getActionBar().setDisplayShowTitleEnabled(false);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
	}

	private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressBar.setVisibility(View.SCREEN_STATE_ON);
		}
	
		@Override
		protected Void doInBackground(Void... params) {
			imagearraylist = new ArrayList<ImageLists>();
			try {
				ParseQuery<ParseObject> query = ParseQuery.getQuery("Gallery");
				query.whereEqualTo("objectId", id);
				ParseQuery<ParseObject> inner = ParseQuery.getQuery("Photo");
				inner.whereMatchesQuery("gallery", query);
				ob = inner.find();
				for (ParseObject photos : ob) {
					ParseFile image = (ParseFile) photos.get("image");
					ImageLists map = new ImageLists();
					map.setImagelist(image.getUrl());
					imagearraylist.add(map);
				}
			} catch (ParseException e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			viewPager = (ViewPager) findViewById(R.id.pager);
			adapter = new ViewPagerAdapter(ViewPagerActivity.this,
					imagearraylist);
			viewPager.setAdapter(adapter);
			viewPager.setCurrentItem(currentPosition);
			tapGestureDetector = new GestureDetector(ViewPagerActivity.this,
					new TapGestureListener());
			viewPager.setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					tapGestureDetector.onTouchEvent(event);
					return false;
				}
			});
			if(imagearraylist != null){
				progressBar.setVisibility(View.GONE);
			}		
		}
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.gallery_single, menu);
//		invalidateOptionsMenu();
		return true;
	}

	class TapGestureListener extends GestureDetector.SimpleOnGestureListener {

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			switch (e.getAction()) {
			case MotionEvent.ACTION_DOWN:

				return true;
			}
			return false;
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		case R.id.action_share:
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
			query.whereEqualTo("objectId", event_id);
			query.getInBackground(event_id, new GetCallback<ParseObject>() {
				
				@Override
				public void done(ParseObject object, ParseException ex) {
					if(ex==null){
						hashTag = object.getString("eventHashTag");
						alertDialog();
					} else {
						Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
					}
				}
			});
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	private void alertDialog(){
		final Dialog d = new Dialog(this);
		d.setTitle("Share via...");
		d.setContentView(R.layout.share_dialog);
		d.findViewById(R.id.imgFacebook).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String facebookPackageName = "com.facebook.katana";
                try {
                    ApplicationInfo facebookAppInfo = getPackageManager().getApplicationInfo(facebookPackageName, 0);
					d.dismiss();
					ImageLoader imageLoader = new ImageLoader(ViewPagerActivity.this);
					imageLoader.UploadImage(imagearraylist.get(viewPager.getCurrentItem()).getImagelist());
					Bitmap image = bmp;
					SharePhoto photo = new SharePhoto.Builder()
			        .setBitmap(image)
			        .build();			
					SharePhotoContent content = new SharePhotoContent.Builder()
			        .addPhoto(photo)
			        .build();
					ShareDialog.show(ViewPagerActivity.this, content);
				} catch(NameNotFoundException ex){
					// No Facebook app found
					Toast.makeText(getApplicationContext(), "No Facebook App found. Please install the app and try again.", Toast.LENGTH_SHORT).show();
				}
			}
		});
		d.show();
		d.findViewById(R.id.imgTwitter).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String twitterPackageName = "com.twitter.android";
                try {
                	ApplicationInfo twitterAppInfo = getPackageManager().getApplicationInfo(twitterPackageName, 0);
					d.dismiss();
					ImageLoader imageLoader = new ImageLoader(ViewPagerActivity.this);
					imageLoader.UploadImage(imagearraylist.get(viewPager.getCurrentItem()).getImagelist());
					Bitmap image = bmp;
					FileCache fileCache = new FileCache(ViewPagerActivity.this);
					fileCache.BitmapConversion(image);
					File myImageFile = new File(Environment.getExternalStorageDirectory()+"/tpb_cache/"+FileCache.getFname());
					Uri myImageUri = Uri.fromFile(myImageFile);
					TweetComposer.Builder builder = new TweetComposer.Builder(ViewPagerActivity.this)
							.text(hashTag)
							.image(myImageUri);
					builder.show();
				} catch(NameNotFoundException e) {
					//No Twitter App Found
					Toast.makeText(getApplicationContext(), "No Twitter app found. Please install twitter and try again.", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private static String id;
	private static int currentPosition;

	public static void setId(String id) {
		ViewPagerActivity.id = id;
	}

	public static void setPosition(int position) {
		ViewPagerActivity.currentPosition = position;

	}

	@Override
	protected void onResume() {
		super.onResume();	
		new RemoteDataTask().execute();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	private static Bitmap bmp;

	public static void setBmp(Bitmap bmp) {
		ViewPagerActivity.bmp = bmp;
	}
	
}