package com.iconcept.tpbguide.tpbevents.imagecaching;

import java.util.ArrayList;
import java.util.List;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.TouchImageView;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
public class ViewPagerAdapter extends PagerAdapter {
	// Declare Variables
	Context context;
	LayoutInflater inflater;
	ImageLoader imageLoader;
	private List<ImageLists> imagearraylist = null;
	private ArrayList<ImageLists> arraylist;


	public ViewPagerAdapter(Context context, List<ImageLists> imagearraylist) {
		this.context = context;
		this.imagearraylist = imagearraylist;
		this.arraylist = new ArrayList<ImageLists>();
		this.arraylist.addAll(imagearraylist);
		imageLoader = new ImageLoader(context);
	}

	private class ViewHolder {
		TouchImageView imgflag;

	}

	@Override
	public int getCount() {
		return imagearraylist.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, final int position) {
		ViewHolder viewHolder = null;
		View itemView = null;
		if (itemView == null) {
			viewHolder = new ViewHolder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			itemView = inflater.inflate(R.layout.viewpager_item, container,
					false);
			viewHolder.imgflag  = (TouchImageView) itemView.findViewById(R.id.flag); 
			itemView.setTag(viewHolder);
		}

		imageLoader.DisplayImage(imagearraylist.get(position).getImagelist(), viewHolder.imgflag);
		((ViewPager) container).addView(itemView);
		
		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((RelativeLayout) object);

	}
	
	public static int imagePosition;
	
	public static void setImagePosition(int position){
		ViewPagerAdapter.imagePosition = position;
	}

}