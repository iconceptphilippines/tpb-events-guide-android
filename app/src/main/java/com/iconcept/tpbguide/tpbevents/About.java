package com.iconcept.tpbguide.tpbevents;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import com.iconcept.tpbguide.tpbevents.R;

public class About extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		setContentView(R.layout.activity_about);
		
		WebView aboutApp = (WebView) findViewById(R.id.aboutApp);
		
		String data = readTextFile(this, R.raw.about);
//		String htmlText = "<html><body style=\"text-align:justify;font-size:12px;color:#000;\"> %s </body></Html>";
		StringBuilder sb = new StringBuilder();
		sb.append("<HTML><HEAD><LINK href=\"face.css\" type=\"text/css\" rel=\"stylesheet\"/></HEAD><body>");
		sb.append(data.toString());
		sb.append("</body></HTML>");
//		aboutApp.loadUrl("file:///assets/bootstrap.min.css");
//		aboutApp.setBackgroundColor(0x00000000);
		aboutApp.loadDataWithBaseURL("file:///android_asset/", sb.toString(), "text/html", "utf-8", null);
		
	}
	public static String readTextFile(Context ctx, int resId) {
        InputStream inputStream = ctx.getResources().openRawResource(resId);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader bufferedreader = new BufferedReader(inputreader);
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        try 
        {
            while (( line = bufferedreader.readLine()) != null) 
            {
                stringBuilder.append(line);
                stringBuilder.append('\n');
            }
        } catch (IOException e) {
            return null;
        }
        return stringBuilder.toString();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
