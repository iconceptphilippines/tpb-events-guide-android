package com.iconcept.tpbguide.tpbevents;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.Schedule;
import com.iconcept.tpbguide.tpbevents.Models.ScheduleSlot;
import com.iconcept.tpbguide.tpbevents.ScheduleListAdapter.ViewHolder;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ScheduleSlotAdapter extends ArrayAdapter<ScheduleSlot>{
	
	public ScheduleSlotAdapter(Context context,List<ScheduleSlot> scheduleSlot){
		super(context, 0, scheduleSlot);
		
	}

	@Override
	public View getView(int position, View v, ViewGroup parent){
		
		if(v==null){
			v = LayoutInflater.from(getContext()).inflate(R.layout.sched_slot_items, parent, false);
			final ViewHolder holder = new ViewHolder();
			
			holder.schedSlotTitle = (TextView) v.findViewById(R.id.schedSlotTitle);
			holder.schedSlotTime = (TextView) v.findViewById(R.id.schedSlotTime);
			holder.schedSlotVenue = (TextView) v.findViewById(R.id.schedSlotVenue);
			
			v.setTag(holder);
		}
		
		ScheduleSlot schedSlot = (ScheduleSlot) getItem(position);
		ViewHolder holder = (ViewHolder) v.getTag();
		
		DateFormat startTime = new SimpleDateFormat("hh:mm a",Locale.ENGLISH);
		DateFormat endTime = new SimpleDateFormat("hh:mm a",Locale.ENGLISH);
		
		startTime.setTimeZone(TimeZone.getTimeZone("GMT"));
		endTime.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		String schedStartTime;
		String schedEndTime;
		
		
		String schedTime;
		if(schedSlot.getEndTime() == null){
			schedStartTime = startTime.format(schedSlot.getStartTime());
			schedTime = schedStartTime;
			holder.schedSlotTime.setText(schedTime);
		} else {			
			schedStartTime = startTime.format(schedSlot.getStartTime());
			schedEndTime = endTime.format(schedSlot.getEndTime());
//			schedStartTime = startTime.getDateTimeInstance().format(schedSlot.getStartTime());
//			schedEndTime = endTime.getDateTimeInstance().format(schedSlot.getEndTime());
			
			schedTime = schedStartTime + " - " + schedEndTime;
			holder.schedSlotTime.setText(schedTime);
		}
		
		
		holder.schedSlotTitle.setText(schedSlot.getTitle());
		if(schedSlot.getAddress() == null || schedSlot.getAddress().equals("")){
			holder.schedSlotVenue.setVisibility(View.GONE);
		} else {
			holder.schedSlotVenue.setText(schedSlot.getAddress());
		}
		
		return v;
	}
	
	final class ViewHolder{
		TextView schedSlotTitle;
		TextView schedSlotTime;
		TextView schedSlotVenue;
	}
}
