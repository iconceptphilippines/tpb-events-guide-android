package com.iconcept.tpbguide.tpbevents;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.Schedule;
import com.iconcept.tpbguide.tpbevents.Models.ScheduleSlot;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ScheduleSlotItems extends Activity {
	
	String values;
	Intent intent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule_slot_items);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(false);
		intent = getIntent();
		
		values = intent.getStringExtra("ARRAY_VALUES");
		String date = intent.getStringExtra("SCHEDULE_SLOT_DATE");
		TextView slotDate = (TextView) findViewById(R.id.scheduleTitle);
		slotDate.setText(date);
		inflateList();
		
	}
	public void inflateList(){
		
		final ListView schedSlot = (ListView) findViewById(R.id.arrayValues);
		final ArrayList<ScheduleSlot> schedSlotData = new ArrayList<ScheduleSlot>();
		final ArrayList<ScheduleSlot> slotItems = new ArrayList<ScheduleSlot>();
		final ScheduleSlotAdapter schedSlotAdapter = new ScheduleSlotAdapter(this, slotItems);
		
		schedSlot.setAdapter(schedSlotAdapter);
		
		try {
			JSONArray slotId = new JSONArray(values);
//			Toast.makeText(getApplicationContext(), slotId.toString(), Toast.LENGTH_SHORT).show();
			for(int a=0; a<slotId.length(); a++){
				JSONObject objectId = slotId.getJSONObject(a);
				String valueId = objectId.getString("objectId");
				ParseQuery<ScheduleSlot> query = ParseQuery.getQuery(ScheduleSlot.class);
				query.whereEqualTo("objectId", valueId);
				query.orderByAscending("startTime");
				query.getInBackground(valueId, new GetCallback<ScheduleSlot>(){
					 @Override
					 public void done (final ScheduleSlot slots, ParseException e){
						if(e==null){
							slotItems.add(slots);
							Collections.sort(slotItems, new Comparator<ScheduleSlot>() {
							    public int compare(ScheduleSlot date1, ScheduleSlot date2) {
							    	return date1.getStartTime().compareTo(date2.getStartTime());
							    }
							});
							schedSlotData.addAll(slotItems);
							schedSlotAdapter.notifyDataSetChanged();
							schedSlot.invalidate();
							schedSlot.setOnItemClickListener(new OnItemClickListener() {
								@Override
								public void onItemClick(AdapterView<?> parent,
										View view, int position, long id) {
									// TODO Auto-generated method stub
									String schedSlotId = schedSlotAdapter.getItem(position).getObjectId().toString();
									Intent intent = new Intent(getApplicationContext(), ScheduleDetails.class);
									intent.putExtra("SCHED_SLOT_OBJID", schedSlotId);
									startActivity(intent);
								}
							});
						} else {
							Toast.makeText(getApplicationContext(), "Unable to Fetch Data!", Toast.LENGTH_SHORT).show();
						}
					 }
				});
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.schedule_slot_items, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
