package com.iconcept.tpbguide.tpbevents;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.Event;
import com.parse.ParseImageView;
import com.parse.ParseObject;

public class CustomListEvents extends ArrayAdapter<Event>{

	public CustomListEvents(Context context, List<Event> objects) {
		super(context, 0, objects);
		
	}
	public View getView(int position, View v, ViewGroup parent){
		if(v==null){
			v = LayoutInflater.from(getContext()).inflate(R.layout.custom_list_events, parent, false);
			final ViewHolder holder = new ViewHolder();
			holder.eventTitle = (TextView) v.findViewById(R.id.eventsTitle);
			holder.eventImage = (ParseImageView) v.findViewById(R.id.eventsImage);
			holder.isEditor = (TextView) v.findViewById(R.id.eventsObjectId);
			v.setTag(holder);
		}
		ParseObject ob = (ParseObject) getItem(position);
		ViewHolder holder = (ViewHolder) v.getTag();
		
		holder.eventTitle.setText(ob.getString("title"));
		if(ob.getParseFile("image") != null){
			holder.eventImage.setParseFile(ob.getParseFile("image"));
			holder.eventImage.loadInBackground();
		} else {
			holder.eventImage.setVisibility(View.GONE);
		}
		String status = ob.getString("status");
		if(status.equalsIgnoreCase("editor")){
			holder.isEditor.setText("- Editor");
		} else {
//			holder.isEditor.setVisibility(View.GONE);
		}
		
		return v;
	}
	final class ViewHolder{
		TextView eventTitle;
		TextView isEditor;
		ParseImageView eventImage;
	}

}
