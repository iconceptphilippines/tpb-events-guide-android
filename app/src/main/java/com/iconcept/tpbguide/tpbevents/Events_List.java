package com.iconcept.tpbguide.tpbevents;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.ParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.Event;
import com.iconcept.tpbguide.tpbevents.Models.Schedule;
import com.iconcept.tpbguide.tpbevents.tpbtodolists.TaskLayout;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.parse.ParseQueryAdapter.OnQueryLoadListener;
import com.parse.ParseUser;
import com.parse.ParseRelation;
import com.onesignal.OneSignal;
import android.content.pm.PackageManager;
import android.Manifest;
import android.content.pm.PackageManager;


public class Events_List extends Activity {
	
	ListView eventsLists;
    static boolean active = false;
	public List<String> eventStatus = new ArrayList<>();;
	public final static String EXTRA_MESSAGE = "com.tpbguide.tpbevents";
	public CustomListEvents listEventsAdapter;
	public JSONArray jsonArray;
	private ParseUser currentUser;
	private final int ADD_CONTACT = 0;
	private final int DELETE_CONTACT = 1;
	private int contactFlag;
	private int DELETE_SPECIFIC;
	private String dialogTitle;
	private ParseObject userObject;
	public String userRole;
	private final int CAMERA_REQUEST_PERMISSION = 1310162;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		active = true;
		
        getActionBar().setDisplayShowHomeEnabled(false);
        setContentView(R.layout.activity_events__list);
        invalidateOptionsMenu();
    	if(isNetworkAvailable()) {
        	connectionList();
        }else {
        	noConnectionList();
        }

		OneSignal.startInit(this)
        .setNotificationReceivedHandler(new NotificationReceivedHandler())
        .setNotificationOpenedHandler(new NotificationOpenedHandler())
  		.inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
		.init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
        if(isNetworkAvailable()) {
        	connectionList();
        }else {
        	 noConnectionList();
        }

    }

	public void noConnectionList() {
    	ProgressBar eventsLoading = (ProgressBar) findViewById(R.id.loadingEvents);
		eventsLoading.setVisibility(View.GONE);
		
    	Button btn_reload_list = (Button) findViewById(R.id.btn_retry_load);
    	btn_reload_list.setVisibility(View.VISIBLE);
    	
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Connection failed");
        builder.setMessage("Enable mobile network or Wi-Fi to continue using this application.");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            	startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        });
        builder.show();
    }
    
    public void btn_load_list(View view) {
    	if(isNetworkAvailable()) {
    		connectionList();
    		Button btn_reload_list = (Button) findViewById(R.id.btn_retry_load);
        	btn_reload_list.setVisibility(View.GONE);
    	}
    }
    
    private boolean isNetworkAvailable() {
    	ConnectivityManager connectivityManager 
        = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		  return activeNetworkInfo != null;
	}
    
    public void connectionList() {
    	final ArrayList<Event> eventListData = new ArrayList<Event>();
		final CustomListEvents eventListAdapter = new CustomListEvents(this, eventListData);
    	eventsLists = (ListView) findViewById(R.id.listEvents);
		eventsLists.setAdapter(eventListAdapter);
		
		
		if(ParseUser.getCurrentUser() != null){
			ParseUser use = ParseUser.getCurrentUser();
			ParseQuery<ParseObject> role = ParseQuery.getQuery("_Role");
	  		role.whereEqualTo("users", use);
	  		role.getFirstInBackground(new GetCallback<ParseObject>() {
				@Override
				public void done(ParseObject theRole,
						com.parse.ParseException q) {
					// TODO Auto-generated method stub
					if(q==null){
						if(theRole.getString("name").equals("Editor") || theRole.getString("name") == "Editor"){
							eventStatus.add("editor");
							eventStatus.add("publish");
							ParseQuery<Event> query = ParseQuery.getQuery("Event");
							query.whereContainedIn("status", eventStatus);
							query.findInBackground(new FindCallback<Event>() {

								@Override
								public void done(final List<Event> events, com.parse.ParseException x) {
									// TODO Auto-generated method stub
									if( x == null ){
										ProgressBar eventsLoading = (ProgressBar) findViewById(R.id.loadingEvents);
										eventsLoading.setVisibility(View.GONE);
										eventListData.addAll(events);
										eventsLists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
							                @Override
							                public void onItemClick(AdapterView<?> parent, View view,
							                                        int position, long id) {
							                	final Event eve = (Event) events.get(position);
							                    String eventsObjectId = eve.getObjectId().toString();
												Intent intent = new Intent(getApplicationContext(), Event_Single.class);
												String eventClick = eventsObjectId;
												intent.putExtra("EVENT_ID", eventClick);
												startActivity(intent);
							                }
										});
										eventListAdapter.notifyDataSetChanged();
										eventsLists.invalidate();
									} else {
										Log.e("error =============> ", x.getMessage());
									}
								}
							});
						} else {
							
						}
					} else {
						ParseQuery<Event> query = ParseQuery.getQuery("Event");
						query.whereEqualTo("status", "publish");
						query.findInBackground(new FindCallback<Event>() {

							@Override
							public void done(final List<Event> events, com.parse.ParseException x) {
								// TODO Auto-generated method stub
								if( x == null ){
									ProgressBar eventsLoading = (ProgressBar) findViewById(R.id.loadingEvents);
									eventsLoading.setVisibility(View.GONE);
									eventListData.addAll(events);
									eventsLists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
						                @Override
						                public void onItemClick(AdapterView<?> parent, View view,
						                                        int position, long id) {
						                	final Event eve = (Event) events.get(position);
						                    String eventsObjectId = eve.getObjectId().toString();
											Intent intent = new Intent(getApplicationContext(), Event_Single.class);
											String eventClick = eventsObjectId;
											intent.putExtra("EVENT_ID", eventClick);
											startActivity(intent);
						                }
									});
									eventListAdapter.notifyDataSetChanged();
									eventsLists.invalidate();
									
								} else {
									Log.e("error =============> ", x.getMessage());
								}
							}
						});
						Log.e("ERRORRRRRRRRR ===================>", q.getMessage());
					}
				}

	  		});
		} else {
			ParseQuery<Event> query = ParseQuery.getQuery("Event");
			query.whereEqualTo("status", "publish");
			query.findInBackground(new FindCallback<Event>() {

				@Override
				public void done(final List<Event> events, com.parse.ParseException x) {
					// TODO Auto-generated method stub
					if( x == null ){
						ProgressBar eventsLoading = (ProgressBar) findViewById(R.id.loadingEvents);
						eventsLoading.setVisibility(View.GONE);
						eventListData.addAll(events);
						eventsLists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			                @Override
			                public void onItemClick(AdapterView<?> parent, View view,
			                                        int position, long id) {
			                	final Event eve = (Event) events.get(position);
			                    String eventsObjectId = eve.getObjectId().toString();
								Intent intent = new Intent(getApplicationContext(), Event_Single.class);
								String eventClick = eventsObjectId;
								intent.putExtra("EVENT_ID", eventClick);
								startActivity(intent);
			                }
						});
						eventListAdapter.notifyDataSetChanged();
						eventsLists.invalidate();
						
					}
				}
			});
		}
		
    }


	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.events__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
        case R.id.action_login:
            action_login_user();
            invalidateOptionsMenu();
            if (Build.VERSION.SDK_INT >= 11)
            {
                VersionHelper.refreshActionBarMenu(this);
            }
            return true;
        case R.id.action_toDoList:
        	action_toDoList();
        	return true;
		case R.id.action_about:
            action_about();
            return true;
        case R.id.action_qr_scanner:
        	action_qr_scanner();
            return true;
        case R.id.action_contacts:
        	action_contacts();
        	return true;
        case R.id.action_logout:
          action_Logout_user();
          invalidateOptionsMenu();
          if (Build.VERSION.SDK_INT >= 11)
          {
              VersionHelper.refreshActionBarMenu(this);
          }
          return true;    
        case R.id.action_my_events:
          action_myEvents();
          return true;
        case R.id.action_profile:
            action_view_profile();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    private void action_about(){
    	Intent intent = new Intent(this, About.class);
    	invalidateOptionsMenu();
    	startActivity(intent);
    }
    private void action_contacts(){
    	Intent intent = new Intent(this, Contacts.class);
    	invalidateOptionsMenu();
    	startActivity(intent);
    }
    private void action_toDoList(){
    	Intent toDo = new Intent(this, TaskLayout.class);
    	invalidateOptionsMenu();
    	startActivity(toDo);
    }
    private void action_view_profile() {
    	Intent userProfile = new Intent(this, UserProfile.class);
    	invalidateOptionsMenu();
    	startActivity(userProfile);
    }
    private void action_login_user() {
    	Intent login = new Intent(this, LoginActivity.class);
    	invalidateOptionsMenu();
    	startActivity(login);
    }
    private void action_myEvents() {
    	Intent myEvents = new Intent(this, User_Events.class);
    	startActivity(myEvents);
	}

	private void action_Logout_user() {
		ParseUser current = ParseUser.getCurrentUser();
		if(current != null){
			current.put("oneSignalId", "");
			current.saveInBackground();
		}
		ParseUser.logOut();
		Intent login = new Intent(this, LoginActivity.class);
		invalidateOptionsMenu();
    	startActivity(login);
	}
	
	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case CAMERA_REQUEST_PERMISSION: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					action_qr_scanner();
				}
				return;
			}
		}
	}
	private  void action_qr_scanner() {
		 try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            CAMERA_REQUEST_PERMISSION);
                	return;
                }
            }
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
			startActivityForResult(intent, 0);
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), "ERROR:" + e, Toast.LENGTH_LONG).show();
		}
	 }
//	 	In the same activity you�ll need the following to retrieve the results:
		public void onActivityResult(int requestCode, int resultCode, Intent intent) {
			if (requestCode == 0) {
				if (resultCode == RESULT_OK) {

					/**
					 * Identify Scan Results
					 * **/

					String action = "";
					String object_id = "";

					String scanResult = intent.getStringExtra("SCAN_RESULT");
					String[] split = scanResult.split(";");
					for(String values : split){
						String[] inner_values = values.split(":");
						if( inner_values[0].equalsIgnoreCase("action") )
							action = inner_values[1];
						if( inner_values[0].equalsIgnoreCase("objectid") )
							object_id = inner_values[1];
					}

					/**
					 * Identify Scan Results
					 * **/
					final String ACTION = action;
					final String OBJECT_ID = object_id;
					
					//Toast.makeText(getApplicationContext(),"FullResults : " + scanResult,Toast.LENGTH_LONG).show();
					//Toast.makeText(getApplicationContext(),"Action : " + ACTION,Toast.LENGTH_LONG).show();
					//Toast.makeText(getApplicationContext(),"Object ID : " + OBJECT_ID,Toast.LENGTH_LONG).show();
					
					if(ACTION.equalsIgnoreCase("joinEvent")){
						ParseUser currentUser = ParseUser.getCurrentUser();
						if(currentUser != null){
							final ParseObject join = ParseObject.createWithoutData("_User", currentUser.getObjectId());
							final ParseObject event = ParseObject.createWithoutData("Event", OBJECT_ID);
							List<ParseObject> var = new ArrayList<>();
							var.add(event);
							var.add(join);
							ParseQuery<ParseObject> joinQuery = ParseQuery.getQuery("EventAttendee");
							joinQuery.whereContainedIn("event", var);
							joinQuery.whereContainedIn("user", var);
							joinQuery.findInBackground(new FindCallback<ParseObject>(){
								public void done(List<ParseObject> isAttendee, com.parse.ParseException e){
									List<ParseObject> attendee = new ArrayList<ParseObject>();
									attendee.addAll(isAttendee);
									if(attendee.isEmpty()){
										ParseQuery<ParseObject> eventId = ParseQuery.getQuery("Event");
										eventId.getInBackground(OBJECT_ID, new GetCallback<ParseObject>(){
											@Override
											public void done(ParseObject eventID, com.parse.ParseException e) {
												// TODO Auto-generated method stub
												if(e == null){
													String eventName = eventID.getString("title");
													Builder eventJoin = new Builder(Events_List.this);
													eventJoin.setTitle("Successfully Joined the Event");
													eventJoin.setMessage("Welcome. You have Successfully joined the " + eventName + " event.");
													eventJoin.setPositiveButton("Ok", new OnClickListener(){
														@Override
														public void onClick(
																DialogInterface dialog,
																int which) {
															// TODO Auto-generated method stub
															ParseObject joining = new ParseObject("EventAttendee");
															joining.put("event", event);
															joining.put("user", join);
															joining.saveInBackground();
															
															Intent intent = new Intent(getApplicationContext(), Event_Single.class);
															intent.putExtra("EVENT_ID", OBJECT_ID);
															startActivity(intent);
														}
													});
													eventJoin.show();
												} else {
													Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
												}
											}
										});
									} else {
										Builder errorDialog = new Builder(Events_List.this);
										errorDialog.setTitle("Existing");
										errorDialog.setMessage("You are already joined in this event.");
										errorDialog.setPositiveButton("OK", new OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												// TODO Auto-generated method stub
												Intent intent = new Intent(getApplicationContext(), Event_Single.class);
												intent.putExtra("EVENT_ID", OBJECT_ID);
												startActivity(intent);
											}
										});
										errorDialog.show();
									}
								}
							});
							
						} else {
							Builder loginDialog = new Builder(Events_List.this);
	        		        loginDialog.setTitle("Login Required");
	        		        
	        		        loginDialog.setMessage("Please Login to Continue.");
	        		        loginDialog.setPositiveButton("Ok", new OnClickListener() {
	        		            @Override
	        		            public void onClick(DialogInterface dialog, int which) {
	        		            	Intent intent = new Intent(Events_List.this, LoginActivity.class);
	        		            	Events_List.this.startActivity(intent);
	        		            }
	        		        });
	        		        loginDialog.setNegativeButton("Cancel", new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.cancel();
								}
							});
	        		        loginDialog.show();
						}
					} else if(ACTION.equalsIgnoreCase("addContact")){
						currentUser = ParseUser.getCurrentUser();
						ParseRelation relation = currentUser.getRelation("contacts");
						ParseQuery checkQuery = relation.getQuery();
						checkQuery.findInBackground(new FindCallback<ParseUser>() {
							@Override
							public void done(List<ParseUser> contactUsers, com.parse.ParseException e)
							{
								if( e == null )
								{
									Boolean match =false;
									for( ParseUser contactUser : contactUsers )
									{
										match = String.valueOf(contactUser.getObjectId()).equals(OBJECT_ID);
										if( match )
										{
											// Already Exists
											AlertDialog.Builder alertDialog = new AlertDialog.Builder(Events_List.this);
											alertDialog.setTitle("Existing");
											alertDialog.setMessage("Contact already existing");
											alertDialog.setPositiveButton("OK", new Dialog.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {
													// TODO Auto-generated method stub
													dialog.dismiss();
												}
											});
											alertDialog.show();
											break;
										}
									}

									if (!match) {
										ParseQuery<ParseObject> getUserQuery = ParseQuery.getQuery("_User");
										getUserQuery.getInBackground(OBJECT_ID, new GetCallback<ParseObject>() {
											@Override
											public void done(ParseObject userToBeAdd, com.parse.ParseException e)
											{
												if( e == null )
												{
													userObject = userToBeAdd;
													dialogTitle = "Are you sure you want to add this person?";
													alertDialog();
												}else{
													Toast.makeText(getApplicationContext(), "Error in getting contact info: " + e.getMessage(), Toast.LENGTH_LONG).show();
												}
											}
										});
									}
								}else{
									Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
								}
							}
						});

					} else if( ACTION.equalsIgnoreCase("checkIn") ) {
						ParseUser currentUser = ParseUser.getCurrentUser();
						if(currentUser != null){
							final ParseObject join = ParseObject.createWithoutData("_User", currentUser.getObjectId());
							final ParseObject event = ParseObject.createWithoutData("Event", OBJECT_ID);
							List<ParseObject> var = new ArrayList<>();
							var.add(event);
							var.add(join);
							ParseQuery<ParseObject> checkInquery = ParseQuery.getQuery("EventAttendee");
							checkInquery.whereContainedIn("event", var);
							checkInquery.whereContainedIn("user", var);
							checkInquery.findInBackground(new FindCallback<ParseObject>(){
								public void done(List<ParseObject> isAttendee, com.parse.ParseException e){
									List<ParseObject> attendee = new ArrayList<ParseObject>();
									attendee.addAll(isAttendee);
									if(attendee.isEmpty()){
										Builder errorDialog = new Builder(Events_List.this);
										errorDialog.setTitle("Check-In Error.");
										errorDialog.setMessage("You are not an attendee in this event. Please Join to CheckIn.");
										errorDialog.setPositiveButton("OK", new OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												// TODO Auto-generated method stub
												dialog.dismiss();
											}
										});
										errorDialog.show();
									} else {
										for(ParseObject checkIn : isAttendee){
											boolean check = checkIn.getBoolean("checkedIn");
											if(check != true){
												checkIn.put("checkedIn", true);
												checkIn.saveInBackground(new SaveCallback() {
													@Override
													public void done(com.parse.ParseException e) {
														// TODO Auto-generated method stub
														if(e==null){
															Builder checkedIn = new Builder(Events_List.this);
															checkedIn.setTitle("Success!");
															checkedIn.setMessage("You have successfully check-in to this event.");
															checkedIn.setPositiveButton("Ok", new OnClickListener() {
																@Override
																public void onClick(DialogInterface dialog, int which) {
																	// TODO Auto-generated method stub
																	dialog.dismiss();
																}
															});
															checkedIn.show();
														} else {
															Toast.makeText(getApplicationContext(), "Error. Please Try Again.", Toast.LENGTH_LONG).show();
														}
													}
												});
											} else {
												Builder joined = new Builder(Events_List.this);
												joined.setTitle("Error.");
												joined.setMessage("You are already checked in to this event.");
												joined.setPositiveButton("Ok.", new OnClickListener() {
													@Override
													public void onClick(DialogInterface dialog, int which) {
														// TODO Auto-generated method stub
														dialog.dismiss();
													}
												});
												joined.show();
											}
										}
									}
								}
							});
							
						} else {
							Builder loginDialog = new Builder(Events_List.this);
	        		        loginDialog.setTitle("Login Required");
	        		        
	        		        loginDialog.setMessage("Please login to continue.");
	        		        loginDialog.setPositiveButton("Ok", new OnClickListener() {
	        		            @Override
	        		            public void onClick(DialogInterface dialog, int which) {
	        		            	Intent intent = new Intent(Events_List.this, LoginActivity.class);
	        		            	Events_List.this.startActivity(intent);
	        		            }
	        		        });
	        		        loginDialog.setNegativeButton("Cancel", new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.cancel();
								}
							});
	        		        loginDialog.show();
						}
						
					}
				} else if (resultCode == RESULT_CANCELED) {
					Toast.makeText(getApplicationContext(), "Scan Cancelled.", Toast.LENGTH_LONG).show();
				}
			}
		}

	private void alertDialog() {
		Dialog dialog;
		AlertDialog.Builder alBuilder = new AlertDialog.Builder(this);
		alBuilder.setTitle(dialogTitle);
		alBuilder.setNegativeButton("YES", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {	
				if (contactFlag == ADD_CONTACT) {		
					ParseRelation relation = currentUser.getRelation("contacts");
					relation.add(userObject);
					currentUser.saveInBackground(new SaveCallback() {
						@Override
						public void done(com.parse.ParseException arg0) {
							String saveMessage = "UNSUCCESSFUL...";
							if (arg0 == null) {
								saveMessage = "SUCCESSFULLY ADDED...";
								Toast.makeText(getApplicationContext(),saveMessage,Toast.LENGTH_SHORT).show();
							}else{
								Toast.makeText(getApplicationContext(),saveMessage,Toast.LENGTH_SHORT).show();
							}
						}
					});
				}		
			}
		});
		alBuilder.setPositiveButton("NO",new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialog = alBuilder.create();
		dialog.show();
	}

	public boolean onPrepareOptionsMenu(Menu menu) {
    	MenuItem item_login = menu.findItem(R.id.action_login);
    	MenuItem item_logout = menu.findItem(R.id.action_logout);
    	MenuItem item_myEvents = menu.findItem(R.id.action_my_events);
    	MenuItem item_joinEvent = menu.findItem(R.id.action_qr_scanner);
    	MenuItem item_profile = menu.findItem(R.id.action_profile);
    	MenuItem item_todoList = menu.findItem(R.id.action_toDoList);
    	MenuItem item_about = menu.findItem(R.id.action_about);
    	MenuItem item_contact = menu.findItem(R.id.action_contacts);
    	MenuItem item_myProfile = menu.findItem(R.id.action_profile);
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
        	item_login.setVisible(false);
        } else {
        	item_logout.setVisible(false);
        	item_myEvents.setVisible(false);
        	item_profile.setVisible(false);
        	item_joinEvent.setVisible(false);
        	item_todoList.setVisible(false);
        	item_contact.setVisible(false);
        	item_myProfile.setVisible(false);
        	
        }

		return true;
    }
    
	private class NotificationOpenedHandler implements com.onesignal.OneSignal.NotificationOpenedHandler {
      @Override
      public void notificationOpened(com.onesignal.OSNotificationOpenResult openedResult) {
      	JSONObject additional = openedResult.notification.payload.additionalData;
      	if( additional != null )
      	{
      		try
      		{
				final Bundle extras = new Bundle();
	      		if( additional.getString("type").equals("message") )
	      		{
	      			// Message
	      			final String messageId = additional.getString("messageId");

					// Get Message Details
					ParseQuery<ParseObject> messageQuery = ParseQuery.getQuery("Message");
					messageQuery.getInBackground(messageId, new GetCallback<ParseObject>() {
						@Override
						public void done( ParseObject message, com.parse.ParseException e )
						{
							if( e == null )
							{
								if( message != null )
								{
									ParseObject sender = message.getParseObject("sender");
									if( sender != null )
									{
						      			if( Messaging.isActive() && Messaging.receiver != null )
						      			{
						      				if( Messaging.receiver.equalsIgnoreCase(String.valueOf(sender.getObjectId())) )
						      				{
						      					Messaging.appendMessage(getApplicationContext(), messageId);
						      					return;
						      				}
						      			}
										Intent openMessaging = new Intent(getApplicationContext(), Messaging.class);
										openMessaging.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
										openMessaging.putExtra("userid", String.valueOf(sender.getObjectId()));
										startActivity(openMessaging);
									}
								}
							}else{
								Toast.makeText(getApplicationContext(), "Error in retrieving message details: " + e.getMessage(), Toast.LENGTH_LONG).show();
							}
						}
					});

	      		}else{
	      			// Event
					Intent openEvent = new Intent(getApplicationContext(), Events_List.class);
					openEvent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(openEvent);
	      		}
      		}catch( JSONException e ){
      			Toast.makeText(getApplicationContext(), "Error in additional getString('message'): " + e.getMessage(), Toast.LENGTH_LONG).show();
      		}
      	}
      }
  	}

	private class NotificationReceivedHandler implements com.onesignal.OneSignal.NotificationReceivedHandler {
		@Override
		public void notificationReceived(com.onesignal.OSNotification notification) {
			JSONObject data = notification.payload.additionalData;
	      	if( data != null )
	      	{
	      		try
	      		{
					final Bundle extras = new Bundle();
		      		if( data.getString("type").equals("message") )
		      		{
		      			// Message
		      			final String messageId = data.getString("messageId");

						// Get Message Details
						ParseQuery<ParseObject> messageQuery = ParseQuery.getQuery("Message");
						messageQuery.getInBackground(messageId, new GetCallback<ParseObject>() {
							@Override
							public void done( ParseObject message, com.parse.ParseException e )
							{
								if( e == null )
								{
									if( message != null )
									{
										ParseObject sender = message.getParseObject("sender");
										if( sender != null && Messaging.receiver != null )
										{
							      			if( Messaging.isActive() )
							      			{
							      				if( Messaging.receiver.equalsIgnoreCase(String.valueOf(sender.getObjectId())) )
							      				{
							      					Messaging.appendMessage(getApplicationContext(), messageId);
							      					return;
							      				}
							      			}
										}
									}
								}else{
									Toast.makeText(getApplicationContext(), "Error in retrieving message details: " + e.getMessage(), Toast.LENGTH_LONG).show();
								}
							}
						});

		      		}
	      		}catch( JSONException e ){
	      			Toast.makeText(getApplicationContext(), "Error in additional getString('message'): " + e.getMessage(), Toast.LENGTH_LONG).show();
	      		}
	      	}
		}
	}

}
