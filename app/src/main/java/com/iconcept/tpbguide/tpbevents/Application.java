package com.iconcept.tpbguide.tpbevents;


import java.io.File;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import android.app.ActivityManager;
import android.provider.Settings.Secure;
import android.util.Log;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.iconcept.tpbguide.tpbevents.Models.Announcements;
import com.iconcept.tpbguide.tpbevents.Models.Download;
import com.iconcept.tpbguide.tpbevents.Models.Event;
import com.iconcept.tpbguide.tpbevents.Models.Message;
import com.iconcept.tpbguide.tpbevents.Models.Schedule;
import com.iconcept.tpbguide.tpbevents.Models.ScheduleSlot;
import com.iconcept.tpbguide.tpbevents.Models.User;
import com.parse.ConfigCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;
import com.onesignal.OneSignal;

public class Application extends android.app.Application{
	private static Application instance;
	public static String userRole;
	public static String parseClientKey;
	public static String parseApplicationId;
	public Application(){
		
	}
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
//	    Initialized Subclass
		ParseObject.registerSubclass(Message.class);
		ParseObject.registerSubclass(Download.class);
		ParseObject.registerSubclass(Schedule.class);
		ParseObject.registerSubclass(Announcements.class);
		ParseObject.registerSubclass(Event.class);
		ParseObject.registerSubclass(ScheduleSlot.class);
		Parse.initialize(new Parse.Configuration.Builder(this)
			.applicationId("GHfyuasdkdmaHJAasdlasdkasdasdAasd")
			.server("https://api.teg.zoom.ph/parse")
			.build()
			);
		ParseInstallation installation = ParseInstallation.getCurrentInstallation();

		if(ParseUser.getCurrentUser() != null){
			ParseUser currentUser = ParseUser.getCurrentUser();
			ParseQuery<ParseObject> role = ParseQuery.getQuery("_Role");
			role.whereEqualTo("users", currentUser);
			role.getFirstInBackground(new GetCallback<ParseObject>() {

				@Override
				public void done(ParseObject role, com.parse.ParseException x) {
	  				// TODO Auto-generated method stub
					if(x==null){
						Application.instance.setUserRole(role.getString("name"));
					}
				}

			});
			installation.put("currentUser", ParseObject.createWithoutData(ParseUser.class, currentUser.getObjectId().toString()));
			installation.saveInBackground();
		} else {
			installation.saveInBackground();
		}

		ParseConfig.getInBackground(new ConfigCallback() {
			@Override
			public void done(ParseConfig config, ParseException e) {
				if(e==null){
					JSONArray script = config.getJSONArray("scriptUris");
					if(script != null){
						for(int a=0; a<script.length(); a++){
							try {
								String ad = script.getString(a);
								Log.d("scr", ad);
							} catch (JSONException e1) {
								e1.printStackTrace();
							}
						}
					} else {
						clearApplicationData();
					}
				} else {
					Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
		
	}

	public void clearApplicationData() {
		File cache = getCacheDir();
		File appDir = new File(cache.getParent());
		if(appDir.exists()){
			String[] children = appDir.list();
			for(String s : children){
				if(!s.equals("lib")){
					deleteDir(new File(appDir, s));
					Log.i("TAG", "File /data/data/APP_PACKAGE/" + s +" DELETED");
				}
			}
		}
	}

	public static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}
	
	public static String getUserRole(){
		return userRole;
	}
	public void setUserRole ( String userRole ){
		Application.userRole = userRole;
	}
	
	// parse client key
	public String getParseClientKey(){
		return parseClientKey;
	}
	public void setParseClientKey( String parseClientKey ){
		Application.parseClientKey = parseClientKey;
	}
	
	// parse application id
	public String getParseApplicationId(){
		return parseApplicationId;
	}
	
	public void setParseApplicationId( String parseApplicationId ){
		Application.parseApplicationId = parseApplicationId;
	}

}
