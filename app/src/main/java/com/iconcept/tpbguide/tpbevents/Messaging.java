package com.iconcept.tpbguide.tpbevents;

import com.iconcept.tpbguide.tpbevents.R;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.iconcept.tpbguide.tpbevents.Models.Message;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class Messaging extends Activity {
	
//	private static final String TAG = Message.class.getName();
	Intent intent;
    static String sender;
    static String receiver;
    static boolean active = false;
    ParseUser user;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		setContentView(R.layout.activity_messaging);
		intent = getIntent();
		ParseUser currrent = ParseUser.getCurrentUser();
		if (currrent != null) { // start with existing user
			setupMessagePosting();
        } else { // If not logged in, login as a new anonymous user
            Toast.makeText(getApplicationContext(), "Not Logged Id", Toast.LENGTH_SHORT).show();
        }
	}

	@Override 
	public void onStart() {
		super.onStart();
		active = true;
	}

	@Override 
	public void onPause() {
		super.onPause();
		active = false;
	}

	@Override 
	public void onDestroy() {
		super.onDestroy();
		active = false;
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		this.setIntent(intent);
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
	    active = true;
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		intent = getIntent();
		ParseUser currrent = ParseUser.getCurrentUser();
		if (currrent != null) { // start with existing user
			setupMessagePosting();
        } else { // If not logged in, login as a new anonymous user
            Toast.makeText(getApplicationContext(), "Not Logged Id", Toast.LENGTH_SHORT).show();
        }
	    // receiveMessage();
	}

	private EditText etMessage;
	private Button btSend;
	
	// Setup button event handler which posts the entered message to Parse
	private void setupMessagePosting() {
		user = ParseUser.getCurrentUser();
		sender = user.getObjectId().toString(); 
		receiver = intent.getStringExtra("userid").toString();
		receiveMessage();
		if(lvChat != null){
			mAdapter.notifyDataSetChanged();
			lvChat.invalidateViews();
		} else {
			
		}
        etMessage = (EditText) findViewById(R.id.etMessage);
        btSend = (Button) findViewById(R.id.btSend);
        lvChat = (ListView) findViewById(R.id.lvChat);
        mMessages = new ArrayList<Message>();
        mAdapter = new ChatListAdapter(Messaging.this, sender, mMessages);
        lvChat.setAdapter(mAdapter);
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String body = etMessage.getText().toString();
                // Use Message model to create new messages now      
                if(body.equals("")){
                	Toast.makeText(getApplicationContext(), "Message cannot be Empty.", Toast.LENGTH_SHORT).show();
                } else {
                	Message message = new Message();
                    message.setSender(ParseObject.createWithoutData(user.getClassName(), sender));
                    message.setReceiver(ParseObject.createWithoutData(user.getClassName(), receiver));
                    message.setMessage(body);
                    message.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            receiveMessage();
                        }
                    });
                    etMessage.setText("");
                }
            }
        });
	}
	private static ListView lvChat;
	private static ArrayList<Message> mMessages;
	private static ChatListAdapter mAdapter;

	public static void appendMessage(android.content.Context context, String messageId)
	{
		ParseQuery<Message> query = ParseQuery.getQuery(Message.class);
		query.getInBackground(messageId, new GetCallback<Message>() {
			public void done( Message message, com.parse.ParseException e ){
                if (e == null) {
                    mMessages.add(message);
                    mAdapter.notifyDataSetChanged(); // update adapter
                    lvChat.invalidate(); // redraw listview
                } else {
                    Log.d("message", "Error: " + e.getMessage());
                }
            }
		});
	}

	public static boolean isActive()
	{
		return active;
	}

	// Query messages from Parse so we can load them into the chat adapter
	private void receiveMessage() {
			ParseObject senderId = ParseObject.createWithoutData(ParseUser.class, sender);
			ParseObject receiverId = ParseObject.createWithoutData(ParseUser.class, receiver);
			
			List<ParseObject> var = new ArrayList<>();
			var.add(senderId);
			var.add(receiverId);
	                // Construct query to execute
	        ParseQuery<Message> query = ParseQuery.getQuery(Message.class);
	                // Configure limit and sort order
	        query.whereContainedIn("sender", var);
	        query.whereContainedIn("receiver", var);
	        query.orderByAscending("createdAt");
	        // Execute query to fetch all messages from Parse asynchronously
	                // This is equivalent to a SELECT query with SQL
	        query.findInBackground(new FindCallback<Message>() {
	            public void done(List<Message> messages, ParseException e) {
	                if (e == null) {
	                    mMessages.clear();
	                    mMessages.addAll(messages);
	                    mAdapter.notifyDataSetChanged(); // update adapter
	                    lvChat.invalidate(); // redraw listview
	                } else {
	                    Log.d("message", "Error: " + e.getMessage());
	                }
	            }
	        });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
			Intent backProfile = new Intent(getApplicationContext(), AttendeesProfile.class);
			backProfile.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			backProfile.putExtra("UserId", String.valueOf(receiver));
			startActivity(backProfile);

			finish();
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
}
