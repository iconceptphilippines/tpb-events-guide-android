package com.iconcept.tpbguide.tpbevents.imagecaching;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.iconcept.tpbguide.tpbevents.R;

public class GridViewActivity extends Activity {
	GridView gridview;
	List<ParseObject> ob;
	private ProgressBar progressBar;
	GridViewAdapter adapter;
	private List<ImageLists> imagearraylist = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gridview_main);
		progressBar = (ProgressBar)findViewById(R.id.loadingPhotos);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		
		Intent i = getIntent();
		String id = i.getStringExtra("ALBUM_TITLE");

		ParseQuery<ParseObject> menu = ParseQuery.getQuery("Menu");
		Log.d("ALBUM_TITLE ID", id);
		Log.v("ALBUM_TITLE ID", id);
		menu.getInBackground(id, new GetCallback<ParseObject>() {
			public void done( ParseObject object, com.parse.ParseException e)
			{
				if( e == null )
				{
					ParseQuery<ParseObject> query = ParseQuery.getQuery("Gallery");
					Log.e("Object id Test", object.getString("objectId"));
					query.getInBackground(object.getString("objectId"), new GetCallback<ParseObject>(){
						public void done (ParseObject title, ParseException e){
							if(e==null){
								String albumTitle = title.getString("title");
								getActionBar().setTitle(albumTitle);
							} else {
								Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
							}
						}
					});
				}else{
					Log.e("Error in Menu Query", e.getMessage());
				}
			}
		});

	}

	private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressBar.setVisibility(View.SCREEN_STATE_ON);
		}

		@Override
		protected Void doInBackground(Void... params) {
			imagearraylist = new ArrayList<ImageLists>();
			try {
				ParseQuery<ParseObject> query = ParseQuery.getQuery("Gallery");
				query.whereEqualTo("objectId", id);
				ParseQuery<ParseObject> inner = ParseQuery.getQuery("Photo");
				inner.whereMatchesQuery("gallery", query);
				ob = inner.find();
				for (ParseObject photos : ob) {
					ParseFile image = (ParseFile) photos.get("image");
					ImageLists map = new ImageLists();
					map.setImagelist(image.getUrl());
					imagearraylist.add(map);
				}
			} catch (ParseException e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			gridview = (GridView) findViewById(R.id.gridview);
			adapter = new GridViewAdapter(GridViewActivity.this, imagearraylist);
			gridview.setAdapter(adapter);
			if(imagearraylist != null){
				progressBar.setVisibility(View.GONE);
			}	
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private static String id;

	public static void setId(String id) {
		GridViewActivity.id = id;
	}
	@Override
	protected void onResume() {
		super.onResume();
		File dir = new File(Environment.getExternalStorageDirectory()+"/tpb_cache"); 
		if (dir.isDirectory()) {
		        String[] children = dir.list();
		        for (int i = 0; i < children.length; i++) {
		            new File(dir, children[i]).delete();
		        }
		    }
		new RemoteDataTask().execute();
	}
}
