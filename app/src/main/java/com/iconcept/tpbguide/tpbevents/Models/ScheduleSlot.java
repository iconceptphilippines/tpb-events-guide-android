package com.iconcept.tpbguide.tpbevents.Models;

import java.util.Date;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

@ParseClassName("ScheduleSlot")
public class ScheduleSlot extends ParseObject{
	public String getTitle(){
		return getString("title");
	}
	public String getContent(){
		return getString("content");
	}
	public Date getStartTime(){
		return getDate("startTime");
	}
	public Date getEndTime(){
		return getDate("endTime");
	}
	public ParseGeoPoint getLocation(){
		return getParseGeoPoint("coordinates");
	}
	public String getAddress(){
		return getString("address");
	}
}
