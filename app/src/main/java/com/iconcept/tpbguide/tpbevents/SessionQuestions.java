package com.iconcept.tpbguide.tpbevents;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SessionQuestions extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_session_questions);
		
		Bundle intent = getIntent().getExtras();
		
		String eventId = intent.getString("EVENT_ID");
		String userId = intent.getString("USER_ID");
		String topicId = intent.getString("TOPIC_ID");
		
		final ParseObject questions = new ParseObject("Question");
		
		// initialized id for query pointer
		final ParseObject userQuestion = ParseObject.createWithoutData(ParseUser.class, userId);
		final ParseObject event = ParseObject.createWithoutData("Event", eventId);
		final ParseObject topic = ParseObject.createWithoutData("Topic", topicId);
		final EditText sessionQuestion = (EditText) findViewById(R.id.questions);
		
		Button send = (Button) findViewById(R.id.sendQuestion);
		send.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String ask = sessionQuestion.getText().toString();
				if(!ask.equals("")){
					questions.put("event", event);
					questions.put("question", ask);
					questions.put("user", userQuestion);
					questions.put("topic", topic);
					questions.saveInBackground(new SaveCallback() {
						
						@Override
						public void done(ParseException e) {
							// TODO Auto-generated method stub
							if(e == null){
								sessionQuestion.setText("");
								Toast.makeText(getApplicationContext(), "Question sent.", Toast.LENGTH_SHORT).show();
								finish();
							} else if(ParseUser.getCurrentUser() == null){
								sessionQuestion.setText("");
								Toast.makeText(getApplicationContext(), "You must login to submit your question.", Toast.LENGTH_SHORT).show();
							} else {
								sessionQuestion.setText("");
								Toast.makeText(getApplicationContext(), "An error occured. Please try again.", Toast.LENGTH_SHORT).show();
							}
						}
					});
				} else {
					Toast.makeText(getApplicationContext(), "TextField Cannot be Empty.", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.session_questions, menu);
		return false;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
