package com.iconcept.tpbguide.tpbevents.tpbtodolists;



import com.iconcept.tpbguide.tpbevents.R;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabWidget;

public class TaskLayout extends FragmentActivity implements OnTabChangeListener, OnPageChangeListener {

	private TabsPagerAdapter mAdapter;
    private ViewPager mViewPager;
    private TabHost mTabHost;
    Fragment fragment;

    TabWidget widget;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_layout);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);

        // Tab Initialization
        initialiseTabHost();
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        // Fragments and ViewPager Initialization
       
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOnPageChangeListener(this);
        
        widget = mTabHost.getTabWidget();
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        
        widget.setBackgroundColor(Color.parseColor("#7f7f7f"));
        
    }
    
    // Method to add a TabHost
    private static void AddTab(TaskLayout activity, TabHost tabHost, TabHost.TabSpec tabSpec) {
        tabSpec.setContent(new MyTabFactory(activity));
        tabHost.addTab(tabSpec);
    }

    // Manages the Tab changes, synchronizing it with Pages
    public void onTabChanged(String tag) {
        int pos = this.mTabHost.getCurrentTab();
        this.mViewPager.setCurrentItem(pos);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    // Manages the Page changes, synchronizing it with Tabs
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        int pos = this.mViewPager.getCurrentItem();
        this.mTabHost.setCurrentTab(pos);
   
    }

    @Override
    public void onPageSelected(int arg0) {
    	
    	fragment = ((TabsPagerAdapter)mViewPager.getAdapter()).getFragment(arg0);  
    	
        if (arg0 == 0 && fragment != null) {  
            fragment.onResume();  
            fragment.onPause(); 
            getActionBar().setTitle("To Do Lists");
//            widget.setBackgroundColor(Color.parseColor("#007799"));

        }
        if (arg0 == 1 && fragment != null) {  
            fragment.onResume();  
            fragment.onPause(); 
            getActionBar().setTitle("To Do Lists");
            mTabHost.getTabWidget().setStripEnabled(false);
            
//            widget.setBackgroundColor(Color.parseColor("#559911"));

        }
        if (arg0 == 2 && fragment != null) {  
            fragment.onResume(); 
            fragment.onPause(); 
            getActionBar().setTitle("To Do Lists");
//            widget.setBackgroundColor(Color.parseColor("#990077"));
            
        }
    }

    // Tabs Creation
    private void initialiseTabHost() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();

        // TODO Put here your Tabs
        TaskLayout.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("AllTab").setIndicator("", getResources().getDrawable(R.drawable.ic_alltask)));
        TaskLayout.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("OngoingTab").setIndicator("", getResources().getDrawable(R.drawable.ic_ongoingtask)));
        TaskLayout.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("DoneTab").setIndicator("", getResources().getDrawable(R.drawable.ic_donetask)));

        mTabHost.setOnTabChangedListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
}