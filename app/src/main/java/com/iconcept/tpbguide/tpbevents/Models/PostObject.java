package com.iconcept.tpbguide.tpbevents.Models;

import com.parse.ParseClassName;
import com.parse.ParseObject;


@ParseClassName("Announcement")
public class PostObject extends ParseObject{
	public String getTitle(){
		return getString("title");
	}
}
