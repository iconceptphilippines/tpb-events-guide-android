package com.iconcept.tpbguide.tpbevents.tpbtodolists;

import java.util.HashMap;
import java.util.Map;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

public class TabsPagerAdapter extends FragmentPagerAdapter {
	
	//HashMap mFragmentTags; 
	private Map<Integer, String> mFragmentTags;
	private FragmentManager mFragmentManager;

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
		
		mFragmentManager = fm;
		mFragmentTags = new HashMap<Integer,String>();
	}
	
	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Top Rated fragment activity
			return new TaskAll();
		case 1:
			// Games fragment activity
			return new TaskOngoing();
		case 2:
			// Movies fragment activity
			return new TaskDone();
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 3;
	}
	
    @Override  
    public Object instantiateItem(ViewGroup container, int position) {  
        Object obj = super.instantiateItem(container, position);  
        if (obj instanceof Fragment) {  
            // record the fragment tag here.  
            Fragment f = (Fragment) obj;  
            String tag = f.getTag();  
            mFragmentTags.put(position, tag);  
        }  
        return obj;  
    }  
	
	public Fragment getFragment(int position) {  
        String tag = mFragmentTags.get(position);  
        if (tag == null)  
            return null;  
        return mFragmentManager.findFragmentByTag(tag);  
	}  

}
