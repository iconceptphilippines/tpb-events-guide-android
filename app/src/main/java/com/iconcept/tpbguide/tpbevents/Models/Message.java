package com.iconcept.tpbguide.tpbevents.Models;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import java.text.SimpleDateFormat;
import java.util.Date;

@ParseClassName("Message")
public class Message extends ParseObject {
    public String getMessage(){
    	return getString("message");
    }
    public ParseObject getSender(){
        return getParseObject("sender");
    }
    public String getTime(){
        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat("hh:mm a");
        return formatter.format(getCreatedAt());
    }
    public String getDate(){
        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat("EEE, MMM d, yyyy");
        String today = formatter.format(new Date());
        String messageDate  = formatter.format(getCreatedAt());
        if( !messageDate.equals(today) ) { return formatter.format(getCreatedAt()); }
        return "";
    }
    public ParseObject getReceiver(){
    	return getParseObject("receiver");
    }
    public void setMessage(String message){
    	put("message", message);
    }
    public void setSender(ParseObject sender){
    	put("sender", sender);
    }
    public void setReceiver(ParseObject receiver){
    	put("receiver", receiver);
    }

}