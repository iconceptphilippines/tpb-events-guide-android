package com.iconcept.tpbguide.tpbevents;

import com.iconcept.tpbguide.tpbevents.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class AnnouncementDetails extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(false);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		setContentView(R.layout.activity_announcement_details);
		
		Intent intent = getIntent();
		String id = intent.getStringExtra("OBJECT_ID");
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Announcement");
		query.whereEqualTo("objectId", id);
		query.getInBackground(id, new GetCallback<ParseObject>() {
			@Override
			public void done(ParseObject object, ParseException e) {
				// TODO Auto-generated method stub
				if(e==null){
					TextView title = (TextView) findViewById(R.id.aTitle);
					TextView content = (TextView) findViewById(R.id.aContent);
					
					title.setText(object.getString("title"));
					content.setText(object.getString("content"));
				} else {
					Toast.makeText(getApplicationContext(), "Unable to Fetch Data", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.announcement_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
