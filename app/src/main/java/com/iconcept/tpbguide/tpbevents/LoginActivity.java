package com.iconcept.tpbguide.tpbevents;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.iconcept.tpbguide.tpbevents.R;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.onesignal.OneSignal;

public class LoginActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_acitivity);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	public void btn_login(View view) {

		final Button btn_login = (Button) findViewById(R.id.btn_login);
		final LinearLayout layout_register=(LinearLayout)this.findViewById(R.id.layout_register);
		final LinearLayout layout_username=(LinearLayout)this.findViewById(R.id.layout_username);
		final LinearLayout layout_password=(LinearLayout)this.findViewById(R.id.layout_password);
		ProgressBar progress = (ProgressBar) findViewById(R.id.progressBarLogin);
		final TextView forgotPw = (TextView) findViewById(R.id.forgotPassword);
		
		btn_login.setVisibility(View.GONE);
		forgotPw.setVisibility(View.GONE);
		progress.setVisibility(View.VISIBLE);
		layout_register.setVisibility(LinearLayout.GONE);
		final EditText txtUsername = (EditText) findViewById(R.id.login_username);
		final String userName = txtUsername.getText().toString().trim();
		final EditText txtPassword = (EditText) findViewById(R.id.login_password);
		final String passWord = txtPassword.getText().toString().trim();
		layout_username.setVisibility(View.GONE);
		layout_password.setVisibility(View.GONE);
		if (userName.equals("") || passWord.equals("")){
			Toast toast=Toast.makeText(getApplicationContext(),"All Fields are Required." , Toast.LENGTH_SHORT);
		    toast.show();
		    txtUsername.setText("");
		    txtPassword.setText("");
		    txtUsername.requestFocus();
			progress.setVisibility(View.GONE);
			btn_login.setVisibility(View.VISIBLE);
			layout_username.setVisibility(View.VISIBLE);
			layout_password.setVisibility(View.VISIBLE);
			layout_register.setVisibility(LinearLayout.VISIBLE);
			forgotPw.setVisibility(View.VISIBLE);
		}else {
			// Send data to Parse.com for verification
            ParseUser.logInInBackground(userName, passWord, 
                    new LogInCallback() {
						@Override
						public void done(ParseUser user, ParseException e) {
							if (user != null && e==null) {
								try {
									ParseUser.logIn(userName, passWord);
								} catch (ParseException e1) {
									e1.printStackTrace();
								}
								ParseInstallation install = ParseInstallation.getCurrentInstallation();
								install.put("user", ParseObject.createWithoutData(ParseUser.class, ParseUser.getCurrentUser().getObjectId().toString()));
								install.saveInBackground();
                                Toast.makeText(getApplicationContext(),"Successfully Logged in",Toast.LENGTH_LONG).show();
                                invalidateOptionsMenu();
                                finish();
                                registerOneSignalUserID();
                            } else {
                              //Unable to SignIn
        						Toast toast=Toast.makeText(getApplicationContext(),"Invalid username or password." , Toast.LENGTH_SHORT);
        					    toast.show();
        					    ProgressBar progress = (ProgressBar) findViewById(R.id.progressBarLogin);
        						progress.setVisibility(View.GONE);
        						forgotPw.setVisibility(View.VISIBLE);
        						btn_login.setVisibility(View.VISIBLE);
        						layout_username.setVisibility(View.VISIBLE);
        						layout_password.setVisibility(View.VISIBLE);
        						layout_register.setVisibility(LinearLayout.VISIBLE);
        					    txtUsername.setText("");
        					    txtPassword.setText("");
        					    txtUsername.requestFocus();
                            }
						}
                    });
		}
		
	}
	public void btn_register(View view) {

		Intent intent = new Intent(this, Registration.class);
		startActivity(intent);
		finish();
	}
	
    private void registerOneSignalUserID()
    {
	    OneSignal.idsAvailable( new OneSignal.IdsAvailableHandler() {
	    	@Override
	    	public void idsAvailable(String userId, String registrationId)
	    	{
				ParseUser current = ParseUser.getCurrentUser();
				if(current != null){
					current.put("oneSignalId", userId);
    				current.saveInBackground();
				}
	    	}
	    });
    }

	public void forgotPw(View view){
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		
		alert.setTitle("Reset Password.");
		alert.setMessage("Please enter the email address for your account.");
		
		final EditText resetPasswordEmail = new EditText(this);
		resetPasswordEmail.setPadding(10, 5, 10, 5);
		alert.setView(resetPasswordEmail);
		
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				final String userEmail = resetPasswordEmail.getText().toString().trim();
				ParseUser.requestPasswordResetInBackground(userEmail,new RequestPasswordResetCallback() {
					public void done(ParseException e) {
						if (e == null) {
							AlertDialog.Builder success = new AlertDialog.Builder(LoginActivity.this);
							success.setTitle("Password Reset");
							success.setMessage("An email with reset instructions has been sent to " + userEmail +".");
							success.setPositiveButton("Ok", new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
								}
							});
							success.show();
						} else {
							AlertDialog.Builder fail = new AlertDialog.Builder(LoginActivity.this);
							fail.setTitle("Password Reset Failed");
							fail.setMessage(e.getMessage());
							fail.setPositiveButton("Ok", new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
								}
							});
							fail.show();
						}
					}
				});
			}
		});
		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		
		alert.show();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.login_acitivity, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
	    case android.R.id.home:
	        this.finish();
	        return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
		
	}
}
