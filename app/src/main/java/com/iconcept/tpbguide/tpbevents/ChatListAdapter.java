package com.iconcept.tpbguide.tpbevents;

import java.util.List;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iconcept.tpbguide.tpbevents.R;
import com.parse.ParseUser;
import com.iconcept.tpbguide.tpbevents.Models.Message;
import com.iconcept.tpbguide.tpbevents.R.drawable;

public class ChatListAdapter extends ArrayAdapter<Message> {    
    ParseUser myId;

    public ChatListAdapter(Context context, String userId, List<Message> messages) {
            super(context, 0, messages);
            myId = ParseUser.getCurrentUser();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                inflate(R.layout.chat_item, parent, false);
            final ViewHolder holder = new ViewHolder();
            holder.body = (TextView)convertView.findViewById(R.id.tvBody);
            holder.time = (TextView)convertView.findViewById(R.id.time);
            holder.wrapper = (LinearLayout) convertView.findViewById(R.id.chat_rel);
            convertView.setTag(holder);
        }
        final Message message = (Message) getItem(position);
        final ViewHolder holder = (ViewHolder)convertView.getTag();
        final boolean isMe = message.getSender().hasSameId(myId);
        if (isMe) {
            holder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            holder.wrapper.setGravity(Gravity.RIGHT);
            holder.body.setBackgroundResource(drawable.bubble_green);
        } else {
        	holder.wrapper.setGravity(Gravity.LEFT);
            holder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            holder.body.setBackgroundResource(drawable.bubble_yellow);
        }
        
        holder.body.setText(message.getMessage());
        if( !message.getDate().equals("") )
        {
            holder.time.setText(message.getDate() + " @ ");
        }else{
            holder.time.setText("Today @ ");
        }
        holder.time.setText(holder.time.getText() + message.getTime());
        return convertView;
    }

    final class ViewHolder {
        public TextView body;
        public LinearLayout wrapper;
        public TextView time;
    }

}