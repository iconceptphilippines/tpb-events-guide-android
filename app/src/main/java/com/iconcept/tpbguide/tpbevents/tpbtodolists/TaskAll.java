package com.iconcept.tpbguide.tpbevents.tpbtodolists;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.iconcept.tpbguide.tpbevents.R;

import android.app.ActionBar;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TaskAll extends Fragment {
	
	View rootView2;
	
	ListView list13;
	ProgressBar waitTaskOngoing;
	TextView labelCursorOngoing, labelEmptyOngoing;
	
	String current_User;
	int itemPosition;
	
	ActionBar actionbar;
	
	ArrayList<ToDoLists> todoList = new ArrayList<ToDoLists>();
	
	MyCustomAdapter dataAdapter1 = null;

	int count2 = 0;
	ListView listView1;
	String [] taskName = new String[100];
	String [] taskStatus = new String[100];
	Date [] taskCreated = new Date[100];
	Date [] taskModified = new Date[100];
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView2 = inflater.inflate(R.layout.task_ongoing, container, false);
		
		setHasOptionsMenu(true);
		
		initComponents();
	    currentUser();	    
	   
	    getActivity().setTitle("To Do Lists");   

		return rootView2;
	}
	
	public void onPause(){
		super.onPause();
		//adapter.clear();
  		try{
	 	    dataAdapter1.clear();
 	    	dataAdapter1.notifyDataSetChanged();
    	}
		
    	catch(Exception e){
    	}
	
	}
	 
	public void onResume(){
		super.onResume();

		updateData();
		waitCursor(true);
		labelEmptyOngoing.setVisibility(4);
	 }
	
	 private void initComponents(){
		 
		 
		 waitTaskOngoing = (ProgressBar) rootView2.findViewById(R.id.prgWaitCursorOngoing);
		 labelCursorOngoing = (TextView) rootView2.findViewById(R.id.lblWaitCursorOngoing);
		 labelEmptyOngoing = (TextView) rootView2.findViewById(R.id.lblEmptyOngoing);
		 labelEmptyOngoing.setVisibility(4);
		 
		 labelEmptyOngoing.setText("Empty Task");
		 
	 }
	 
	 private void currentUser(){
	    	
		 ParseUser currentUser = ParseUser.getCurrentUser();
		 	if (currentUser != null) {
	    		
		 		current_User = currentUser.getUsername().toString();
	    	}
	  }
	 
	 private void showMessage(String title, String message) {
		 
		 Builder builder=new Builder(rootView2.getContext());
		 builder.setCancelable(true);
		 builder.setTitle(title);
		 builder.setMessage(message);
		 builder.setPositiveButton("Ok.", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		 builder.show();
	 }
	
	 private void waitCursor(boolean waiting){
		 
		 if(waiting == true){
			 waitTaskOngoing.setVisibility(0); //visible
			 labelCursorOngoing.setVisibility(0);
		 }
		 else{
			 waitTaskOngoing.setVisibility(4); //invisible
			 labelCursorOngoing.setVisibility(4);
		 }
	 }
	 
		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		    // Do something that differs the Activity's menu here
			inflater.inflate(R.menu.to_do_lists, menu);
		    super.onCreateOptionsMenu(menu, inflater);
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				addDialog();
				//Toast.makeText(rootView2.getContext(), "dsdsd", Toast.LENGTH_SHORT).show();
				//item.setVisible(false);
			return true;
			
			}
	        
			return super.onOptionsItemSelected(item);
		}
		
		 private void displayListView(String name, String status, Date created) {
			 
				count2 = 0;
				
	   			ToDoLists country = new ToDoLists(name, status, false);
	   			todoList.add(country);
				 
				//create an ArrayAdaptar from the String Array
				dataAdapter1 = new MyCustomAdapter(rootView2.getContext(),
						R.layout.task_ongoing, todoList);
				listView1 = (ListView) rootView2.findViewById(R.id.list3);
				// Assign adapter to ListView
				listView1.setAdapter(dataAdapter1);
				
				listView1.setVerticalScrollBarEnabled(false); 
		 
			}
		 
		 private class MyCustomAdapter extends ArrayAdapter<ToDoLists> {
			 
				private ArrayList<ToDoLists> todoList;
				
			    LayoutInflater mInflater;

				public MyCustomAdapter(Context context, int textViewResourceId, 
					ArrayList<ToDoLists> todoList) {
					
					super(context, textViewResourceId, todoList);
					
					mInflater = LayoutInflater.from(context);
					this.todoList = new ArrayList<ToDoLists>();
					this.todoList.addAll(todoList);
				}
		 
				private class ViewHolder {
					TextView title1, description1;
					ImageButton icon1;
					RelativeLayout rel1;
				}
		 
				@Override
				public View getView(int position, View convertView, ViewGroup parent) {
		 
					ViewHolder holder = null;
					Log.v("ConvertView", String.valueOf(position));
		 
					if (convertView == null) {
						//LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						convertView = mInflater.inflate(R.layout.todo_all, null);
		 
						holder = new ViewHolder();
						holder.rel1 = (RelativeLayout) convertView.findViewById(R.id.relative1);
						holder.description1 = (TextView) convertView.findViewById(R.id.lblDescAll);
						holder.title1 = (TextView) convertView.findViewById(R.id.lblTitleAll);
						holder.icon1 = (ImageButton) convertView.findViewById(R.id.imgButton1);
						holder.icon1.setVisibility(4);
						convertView.setTag(holder);
						
						holder.rel1.setOnClickListener( new View.OnClickListener() {  
							public void onClick(View v1) {
								
								final int item2 = listView1.getPositionForView((View) v1.getParent());
								
		
								showMessage("", taskName[item2]+ '\n' + '\n' + "Date Created: " + taskCreated[item2]
											+'\n'+'\n'+ "Date Modified: "+ taskModified[item2]);
								
							}
						});
					} 
					
					else {
						holder = (ViewHolder) convertView.getTag();
					}
		 
					ToDoLists todolist = todoList.get(position);
					
					holder.title1.setText(" " +  todolist.getCode());
					holder.description1.setText(" (" + todolist.getName() + ")");
		 
					return convertView;
				}
			}
		 
		 	private void updateData(){
		
			 ParseQuery<ParseObject> taskQuery = ParseQuery.getQuery("Todo");
			 taskQuery.whereEqualTo("user", ParseUser.getCurrentUser());
			 taskQuery.findInBackground(new FindCallback<ParseObject>() {         
			      @Override
			      public void done(List<ParseObject> tasks, ParseException error) {
			          if(error == null){
			        	  
				      		try{
				    	 	    dataAdapter1.clear();
				     	    	dataAdapter1.notifyDataSetChanged();
				        	}
				    		
				        	catch(Exception e){
				        	}

			         	  
			        	  for (int i = 0; i < tasks.size(); i++) {
			        		  
			        	       String names = tasks.get(i).getString("todo").trim();
			        	       String status = tasks.get(i).getString("status");
			        	       Date created = tasks.get(i).getCreatedAt();
			        	       //Date modified = tasks.get(i).getUpdatedAt();
			        	       taskName[i] = tasks.get(i).getString("todo");
			        	       //taskName[i] = tasks.get(i).getObjectId();
			        	       taskCreated[i] = tasks.get(i).getCreatedAt();
			        	       taskModified[i] = tasks.get(i).getUpdatedAt();
			        	       
			        	       try{
			        	    	   displayListView(names, status, created);
			        	       }
			        	       catch(Exception e){
			        	    	   
			        	       }
			        	       
			        	  
			        	  }
			        	  
			        	  int num;
			        	  
			        	  try{
			        		  num = listView1.getAdapter().getCount();
			        	  }
			        	  catch(Exception e){
			        		  num = 0;
			        	  }
			        	  //int num = listView1.getAdapter().getCount();
			        	  
			        	  if(num != 0){
			        		  waitCursor(false);
			        		  labelEmptyOngoing.setVisibility(4);
			        	  }
			        	  else{
			        		  waitCursor(false);
			        		  labelEmptyOngoing.setVisibility(0);
			        	  }
			          }
			          else{
			          }
			      }
			  });
		 }
		 	
		 	 private void addDialog(){
				 
				 final Dialog dialog = new Dialog(rootView2.getContext());
				 dialog.setContentView(R.layout.add_task_dialog);
				 dialog.setTitle("Add Task");
				 dialog.setCancelable(false);
				 
				 Button addTaskButton = (Button) dialog.findViewById(R.id.btnAdd);
				 Button cancelTaskButton = (Button) dialog.findViewById(R.id.btnCancel);
				 
				 final EditText addTaskAppend = (EditText) dialog.findViewById(R.id.txtTask);
					
				 // if button is clicked, close the custom dialog
				 addTaskButton.setOnClickListener(new OnClickListener() {
					 @Override
					 public void onClick(View v) {
						 
						 if(addTaskAppend.getText().toString().trim().equals("")){
							 showMessage("Warning", "Must not be empty");
						 }
						 else{
							 addTask(addTaskAppend.getText().toString().trim());
							
							 dialog.dismiss();
						 }
						 dialog.dismiss();
						 
					 }
				 });
				 cancelTaskButton.setOnClickListener(new OnClickListener() {
					 @Override
					 public void onClick(View v) {
						
						 dialog.dismiss();
					 }
				 });
				 dialog.show();
			 }	
		 	 
		 	private void addTask(String myOngoingTask){
				 
				// final String tSaved = "Saved";
				 
				 ParseObject tasks = new ParseObject("Todo");
		 		 tasks.put("user", ParseUser.getCurrentUser());
		 		 tasks.put("todo", myOngoingTask);
		 		 tasks.put("status", "ongoing");
		 		 
		 		 tasks.saveInBackground(new SaveCallback() {
		 	    	
		 			 public void done(ParseException e) {
		 				 // We have to use utility classes to add behavior to our ParseObjects.
		 				 //Toast.makeText(getApplicationContext(),
		 					// "Tasks Saved!",
		 					// Toast.LENGTH_LONG).show();
		 				//statDialog(tSaved);
		 				 if (e == null){
		 					 
		 					 waitCursor(true);
							 labelEmptyOngoing.setVisibility(4);
							 Toast.makeText(rootView2.getContext(), "Successfully Added", Toast.LENGTH_SHORT).show();
							 try{
								 dataAdapter1.clear();
								 dataAdapter1.notifyDataSetChanged();
								 
							 }
							 catch (Exception ex){
								 
							 }
		 				 
						 updateData();
						 
		 				 }
		 				 
		 			 else{

		 			 }
		 			 }
		 		 });
			 }
		 	
}