package com.iconcept.tpbguide.tpbevents;

import com.iconcept.tpbguide.tpbevents.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.R.drawable;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ListItemsDetails extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule_details);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		getActionBar().setDisplayShowTitleEnabled(false);
		Intent i = getIntent();
		String id = i.getStringExtra("ID");
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Page");
		query.whereEqualTo("objectId", id);
		query.getInBackground(id, new GetCallback<ParseObject>() {
			public void done(final ParseObject object, ParseException e) {
				if (e == null) {
					ProgressBar prog = (ProgressBar) findViewById(R.id.progressBar1);
					prog.setVisibility(View.GONE);
					RelativeLayout relayout = (RelativeLayout) findViewById(R.id.rel_details);
					relayout.setVisibility(View.VISIBLE);
					//display description
					ParseImageView imgView = (ParseImageView) findViewById(R.id.activityImg);
					TextView date = (TextView) findViewById(R.id.schedDetailDate);
					TextView title = (TextView) findViewById(R.id.schedDetailTitle);
					TextView location = (TextView) findViewById(R.id.schedDetaillocation);
					RelativeLayout loc = (RelativeLayout) findViewById(R.id.rel_loc_info);
					
					if(object.getParseFile("image") != null){
						imgView.setParseFile(object.getParseFile("image"));
						imgView.setScaleType(ScaleType.CENTER_CROP);
						imgView.loadInBackground();
					} else {
						imgView.setVisibility(View.GONE);
					}
					
					if(object.getString("address") != null || !object.getString("address").equals("")){
						Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
						RelativeLayout eventLocation = (RelativeLayout) findViewById(R.id.rel_loc_info);
						location.setTypeface(font);
						TextView chev = (TextView) findViewById(R.id.chev_right);
						chev.setTypeface(font);
						chev.setText(getString(R.string.fa_chevron_right));
						location.setText(getString(R.string.fa_map_marker)+ "   " + object.getString("address"));
						if(object.getParseGeoPoint("coordinates") != null){
							eventLocation.setBackgroundResource(drawable.list_selector_background);
							eventLocation.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									ParseGeoPoint geoPoint = object.getParseGeoPoint("coordinates");
									Double lat = geoPoint.getLatitude();
									Double lon = geoPoint.getLongitude();
									String latitude = String.valueOf(lat);
									String longitude = String.valueOf(lon);
									Intent intent = new Intent(getApplicationContext(), Map.class);
									intent.putExtra("LATITUDE", latitude);
									intent.putExtra("LONGITUDE", longitude);
									startActivity(intent);
								}
							});
						} else {
							eventLocation.setVisibility(View.GONE);
						}
					} else {
						loc.setVisibility(View.GONE);
					}
					title.setText(object.getString("title"));
					if(object.getString("subtitle") != null){
						date.setText(object.getString("subtitle"));
					}
					String css = "file:///android_asset/face.css";
					String myData = object.getString("content");
					WebView eventDescription = (WebView) findViewById(R.id.schedDetailInfo);
					eventDescription.setBackgroundColor(Color.WHITE);
					if(myData == null || myData == ""){
						eventDescription.setVisibility(View.GONE);
					} else {
						String htmlWithCss = "<html><head><style type=\"text/css\">" +css +"</style></head><body>" + myData +"</body></html>";
						eventDescription.setBackgroundColor(Color.WHITE);
						eventDescription.getSettings().setJavaScriptEnabled(true);
						eventDescription.loadDataWithBaseURL("",htmlWithCss, "text/html", "utf-8","");
					}

				} else {
					// something went wrong
					Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
				}
			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
				switch (item.getItemId()) {
				case android.R.id.home:
					this.finish();
					return true;
				default:
					return super.onOptionsItemSelected(item);
				}
	}
}
