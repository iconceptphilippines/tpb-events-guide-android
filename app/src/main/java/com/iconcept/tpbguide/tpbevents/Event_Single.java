package com.iconcept.tpbguide.tpbevents;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import android.R.drawable;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.iconcept.tpbguide.tpbevents.R;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class Event_Single extends Activity {

	TextView menuList;
	String eve;
	public final static String EVENT_ID = "com.tpbguide.tpbevents";
	public CustomMenuSingle menuListAdapter;
	public String eventName;
	private static String event_id;

	public static String getEvent_id() {
		return event_id;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event__single);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent))); 
		getActionBar().setTitle("");
		

		// Get the message from the intent
		Intent intent = getIntent();
		event_id = intent.getStringExtra("EVENT_ID");
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
		query.getInBackground(event_id, new GetCallback<ParseObject>() {
			public void done(ParseObject object, ParseException e) {
				if (e == null) {
					Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
					eventName = object.getString("title");
					getActionBar().setTitle(eventName);
					// display event title
					TextView eventTitle = (TextView) findViewById(R.id.event_single_title);
					eventTitle.setText(object.getString("title"));
					
					//display event date
					SimpleDateFormat end = new SimpleDateFormat("LLLL dd, yyyy"); // your required format
					end.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
					SimpleDateFormat start = new SimpleDateFormat("LLLL dd");
					start.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
					String startDate = start.format(object.getDate("startDate")); // convert it in your required format
					String startOnly = end.format(object.getDate("startDate"));
					String endDate = null;
					if(object.getDate("endDate") == object.getDate("startDate")){
						Toast.makeText(getApplicationContext(),"same", Toast.LENGTH_LONG).show();
					}
					if(object.getDate("endDate") != null){
						endDate = end.format(object.getDate("endDate")); // convert it in your required format
					}
					TextView eventDate = (TextView) findViewById(R.id.event_single_date);
					eventDate.setTypeface(font);
					if(object.getDate("endDate") ==  null){
						eventDate.setText(getString(R.string.fa_calendar) + " " + startOnly);					
					} else {
						eventDate.setText(getString(R.string.fa_calendar) + " " + startDate + " - " + endDate);
					}
					TextView eventLocation = (TextView) findViewById(R.id.event_single_location);
					eventLocation.setTypeface(font);
					if(object.getString("venueName") != null){
						eventLocation.setText(getString(R.string.fa_map_marker) + " " + object.getString("venueName"));
					}
					// Add and download the image
					ParseImageView eventsImage = (ParseImageView) findViewById(R.id.event_image_single);
					ParseFile imageFile = object.getParseFile("image");
					if (imageFile != null) {
						eventsImage.setParseFile(imageFile);
						eventsImage.loadInBackground();
					}
				} else {
					// something went wrong
					Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
				}
			}

		});
		
		if(ParseUser.getCurrentUser() != null){
			ParseUser use = ParseUser.getCurrentUser();
			ParseObject event = ParseObject.createWithoutData("Event", event_id);
			ParseObject user = ParseObject.createWithoutData("_User", use.getObjectId());
			List<ParseObject> var = new ArrayList<>();
			var.add(event);
			var.add(user);
			ParseQuery<ParseObject> joinQuery = ParseQuery.getQuery("EventAttendee");
			joinQuery.whereContainedIn("event", var);
			joinQuery.whereContainedIn("user", var);
			joinQuery.findInBackground(new FindCallback<ParseObject>(){
				public void done(List<ParseObject> isAttendee, ParseException e){
					ArrayList<ParseObject> attendee = new ArrayList<ParseObject>();
					attendee.addAll(isAttendee);
					if(attendee.isEmpty()){
						
					} else {
						TextView qw = new TextView(getApplicationContext());
						qw.setText("- Attending");
						qw.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
						qw.setTextColor(Color.WHITE);
						qw.setPadding(10,10,10,10);
						
						Event_Single.this.addContentView(qw, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
					}
				}
			});
		}
		ParseObject attendee = new ParseObject("EventAttendee");
		ParseObject attendeeId = null;
		if(ParseUser.getCurrentUser() != null){
			ParseUser currentUser = ParseUser.getCurrentUser();
			attendeeId = ParseObject.createWithoutData(ParseUser.class, currentUser.getObjectId());
		}
		if(ParseUser.getCurrentUser() != null) {
			ParseQuery<ParseObject> privateEvent = ParseQuery.getQuery("Event");
			privateEvent.getInBackground(event_id, new GetCallback<ParseObject>() {
				@Override
				public void done(ParseObject object, ParseException e) {
					// TODO Auto-generated method stub
					if(e==null){
						boolean isPrivate = object.getBoolean("isPrivate");
						if(isPrivate){
							final ParseUser currentUser = ParseUser.getCurrentUser();
							final ParseObject event = ParseObject.createWithoutData("Event", event_id);
							ParseObject user = ParseObject.createWithoutData("_User", currentUser.getObjectId());
							List<ParseObject> var = new ArrayList<>();
							var.add(event);
							var.add(user);
							ParseQuery<ParseObject> attendeeQuery = ParseQuery.getQuery("EventAttendee");
							attendeeQuery.whereNotEqualTo("isApproved", false);
							attendeeQuery.whereContainedIn("event", var);
							attendeeQuery.whereContainedIn("user", var);
							attendeeQuery.findInBackground(new FindCallback<ParseObject>(){
								public void done(List<ParseObject> attendee, ParseException e){
									if(e==null){
										ArrayList<ParseObject> list = new ArrayList<ParseObject>();
										list.addAll(attendee);
										ParseObject object = ParseObject.createWithoutData("_User", currentUser.getObjectId());
										if(list.isEmpty()){
											ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("Event");
											ParseRelation<ParseObject> relation = event.getRelation("menus");
											innerQuery.whereEqualTo("objectId", event_id);
											ParseQuery<ParseObject> querya = relation.getQuery();
											querya.orderByAscending("order");
											querya.include("post");
											querya.whereEqualTo("enabled", true);
											querya.whereNotEqualTo("visibility", "attendees");
											querya.whereEqualTo("status","publish");
											querya.findInBackground(new FindCallback<ParseObject>() {
												public void done(List<ParseObject> listmenu, ParseException e) {
								
													TableLayout lin = (TableLayout) findViewById(R.id.singledetails);
													Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
													Integer count=1;
													if (e == null) {
														ProgressBar prg = (ProgressBar) findViewById(R.id.eventSingleProgress);
														prg.setVisibility(View.GONE);
														RelativeLayout rel = (RelativeLayout) findViewById(R.id.relEventSingleItems);
														rel.setVisibility(View.VISIBLE);
														for (final ParseObject comment : listmenu) {
															final ParseObject post = comment.getParseObject("page");
																
															final ParseObject gallery = comment.getParseObject("gallery");
															final String menuId = comment.getObjectId().toString();
															final String mtitle = comment.getString("title");
															
															TableRow tr = new TableRow(Event_Single.this);
															tr.setPadding(0, 20, 0, 20);
															TextView icons = new TextView(Event_Single.this);
															icons.setId(count);
															icons.setTypeface(font);
															final String menuType = comment.getString("menuType");
															if(menuType != null){
																if(menuType.equals("list")){
																	icons.setText(getString(R.string.fa_list));  
																} else if (menuType.equals("map")){
																	icons.setText(getString(R.string.fa_map_marker));
																} else if (menuType.equals("page")){
																	icons.setText(getString(R.string.fa_info));
																} else if (menuType.equals("gallery")){
																	icons.setText(getString(R.string.fa_picture_o));
																} else if (menuType.equals("schedule")){
																	icons.setText(getString(R.string.fa_calendar_o));
																} else if (menuType.equals("attendees")){
																	icons.setText(getString(R.string.fa_user));
																} else if (menuType.equals("info")){
																	icons.setText(getString(R.string.fa_info_circle));
																} else if (menuType.equals("information")){
																	icons.setText(getString(R.string.fa_info));
																} else if (menuType.equals("questions")){
																	icons.setText(getString(R.string.fa_question));
																} else if (menuType.equals("files")){
																	icons.setText(getString(R.string.fa_file));
																} else if (menuType.equals("announcements") || menuType.equals("announcement")){
																	icons.setText(getString(R.string.fa_comments));
																} else if (menuType.equals("social_media")){
																	icons.setText(getString(R.string.fa_share));
																} else {
																	icons.setText("...");
																}
															}
															icons.setTextColor(Color.GRAY);
															icons.setPadding(10, 10, 10, 10);
															tr.addView(icons);
								
															final TextView id = new TextView(Event_Single.this);
															id.setText(comment.getString("objectId"));
															tr.addView(id);
								
															TextView myText= new TextView(Event_Single.this);
															myText.setId(count);
															myText.setText(comment.getString("title"));
															myText.setTextColor(Color.BLACK);
															myText.setPadding(10, 10, 10, 10);
															
															
															tr.setBackgroundResource(drawable.list_selector_background);
															tr.setOnClickListener(new OnClickListener() {								
																@Override
																public void onClick(View v) {
																	if(post != null){
																		eve = post.getObjectId().toString();
																	} else if(gallery != null) {
																		eve = gallery.getObjectId().toString();
																	}
																	String type = " ";
																	if(menuType != null){
																		type = comment.get("menuType").toString();
																	}
																	Intent intent = new Intent(Event_Single.this,MenuDesc.class);
																	Bundle extras = new Bundle();
																	extras.putString(EVENT_ID,event_id);
																	if(eve != null){
																		extras.putString("OBJECT_ID", eve);
																	}
																	extras.putString("MENU_ID", menuId);
																	extras.putString("MENU_TYPE", type);
																	extras.putString("MENU_TITLE", mtitle);
																	intent.putExtras(extras);
																	startActivity(intent);
																}
															});
															tr.addView(myText);
								
															View line = new View(Event_Single.this);
															line.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1, 1f));
															line.setBackgroundColor(Color.GRAY);
															line.setPadding(0, 5, 0, 5);
															lin.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
															lin.addView(line);
														}
													} else {
														// something went wrong
														Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
													}
												}
											});
										} else {
											ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("Event");
											innerQuery.whereEqualTo("objectId", event_id);
											ParseRelation<ParseObject> relation = event.getRelation("menus");
											ParseQuery<ParseObject> querya = relation.getQuery();
											querya.orderByAscending("order");
											querya.include("post");
											querya.whereEqualTo("enabled", true);
											querya.whereEqualTo("status", "publish");
											querya.findInBackground(new FindCallback<ParseObject>() {
												public void done(List<ParseObject> listmenu, ParseException e) {
								
													TableLayout lin = (TableLayout) findViewById(R.id.singledetails);
													Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
													Integer count=1;
													if (e == null) {
														ProgressBar prg = (ProgressBar) findViewById(R.id.eventSingleProgress);
														prg.setVisibility(View.GONE);
														RelativeLayout rel = (RelativeLayout) findViewById(R.id.relEventSingleItems);
														rel.setVisibility(View.VISIBLE);
														for (final ParseObject comment : listmenu) {
															final ParseObject post = comment.getParseObject("page");
																
															final ParseObject gallery = comment.getParseObject("gallery");
															final String menuId = comment.getObjectId().toString();
															final String mtitle = comment.getString("title");
															
															TableRow tr = new TableRow(Event_Single.this);
															tr.setPadding(0, 20, 0, 20);
															TextView icons = new TextView(Event_Single.this);
															icons.setId(count);
															icons.setTypeface(font);
															final String menuType = comment.getString("menuType");
															if(menuType != null ){
																if(menuType.equals("list")){
																	icons.setText(getString(R.string.fa_list));  
																} else if (menuType.equals("map")){
																	icons.setText(getString(R.string.fa_map_marker));
																} else if (menuType.equals("page")){
																	icons.setText(getString(R.string.fa_info));
																} else if (menuType.equals("gallery")){
																	icons.setText(getString(R.string.fa_picture_o));
																} else if (menuType.equals("schedule")){
																	icons.setText(getString(R.string.fa_calendar_o));
																} else if (menuType.equals("attendees")){
																	icons.setText(getString(R.string.fa_user));
																} else if (menuType.equals("info")){
																	icons.setText(getString(R.string.fa_info_circle));
																}else if (menuType.equals("information")){
																	icons.setText(getString(R.string.fa_info));
																}else if (menuType.equals("questions")){
																	icons.setText(getString(R.string.fa_question));
																}else if (menuType.equals("files")){
																	icons.setText(getString(R.string.fa_file));
																}else if (menuType.equals("announcements")){
																	icons.setText(getString(R.string.fa_comments));
																} else if (menuType.equals("social_media")){
																	icons.setText(getString(R.string.fa_share));
																} else {
																	icons.setText("...");
																}
															}
															icons.setTextColor(Color.GRAY);
															icons.setPadding(10, 10, 10, 10);
															tr.addView(icons);
								
															final TextView id = new TextView(Event_Single.this);
															id.setText(comment.getString("objectId"));
															tr.addView(id);
								
															TextView myText= new TextView(Event_Single.this);
															myText.setId(count);
															myText.setText(comment.getString("title"));
															myText.setTextColor(Color.BLACK);
															myText.setPadding(10, 10, 10, 10);
															
															tr.setBackgroundResource(drawable.list_selector_background);
															tr.setOnClickListener(new OnClickListener() {								
																@Override
																public void onClick(View v) {
																	if(post != null){
																		eve = post.getObjectId().toString();
																	} else if(gallery != null) {
																		eve = gallery.getObjectId().toString();
																	}
																	String type = " ";
																	if(menuType != null ){
																		type = comment.get("menuType").toString();
																	}
																	Intent intent = new Intent(Event_Single.this,MenuDesc.class);
																	Bundle extras = new Bundle();
																	extras.putString(EVENT_ID,event_id);
																	if(eve != null){
																		extras.putString("OBJECT_ID", eve);
																	}
																	extras.putString("MENU_ID", menuId);
																	extras.putString("MENU_TYPE", type);
																	extras.putString("MENU_TITLE", mtitle);
																	intent.putExtras(extras);
																	startActivity(intent);
																}
															});
															tr.addView(myText);
															View line = new View(Event_Single.this);
															line.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1, 1f));
															line.setBackgroundColor(Color.GRAY);
															line.setPadding(0, 5, 0, 5);
															lin.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
															lin.addView(line);
														}
													} else {
														Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
													}
												}
											});
										}
									} else {
										Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
									}
								}
							});
						} else if (Application.getUserRole() != null && Application.getUserRole().equals("Editor") || Application.getUserRole() == "Editor"){
							final ParseUser currentUser = ParseUser.getCurrentUser();
							final ParseObject event = ParseObject.createWithoutData("Event", event_id);
							ParseObject user = ParseObject.createWithoutData("_User", currentUser.getObjectId());
							List<ParseObject> var = new ArrayList<>();
							var.add(event);
							var.add(user);
							ParseQuery<ParseObject> attendeeQuery = ParseQuery.getQuery("EventAttendee");
							attendeeQuery.whereContainedIn("event", var);
							attendeeQuery.whereContainedIn("user", var);
							attendeeQuery.findInBackground(new FindCallback<ParseObject>(){
								public void done(List<ParseObject> attendee, ParseException e){
									if(e==null){
										ArrayList<ParseObject> list = new ArrayList<ParseObject>();
										list.addAll(attendee);
										ParseObject object = ParseObject.createWithoutData("_User", currentUser.getObjectId());
										if(list.isEmpty()){
											ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("Event");
											ParseRelation<ParseObject> relation = event.getRelation("menus");
											innerQuery.whereEqualTo("objectId", event_id);
											ParseQuery<ParseObject> querya = relation.getQuery();
											querya.orderByAscending("order");
											List<String> array = new ArrayList();
											array.add("editor");
											array.add("publish");
											querya.include("post");
											querya.whereContainedIn("status", array);
											querya.whereEqualTo("enabled", true);
											querya.findInBackground(new FindCallback<ParseObject>() {
												public void done(List<ParseObject> listmenu, ParseException e) {
								
													TableLayout lin = (TableLayout) findViewById(R.id.singledetails);
													Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
													Integer count=1;
													if (e == null) {
														ProgressBar prg = (ProgressBar) findViewById(R.id.eventSingleProgress);
														prg.setVisibility(View.GONE);
														RelativeLayout rel = (RelativeLayout) findViewById(R.id.relEventSingleItems);
														rel.setVisibility(View.VISIBLE);
														for (final ParseObject comment : listmenu) {
															final ParseObject post = comment.getParseObject("page");
																
															final ParseObject gallery = comment.getParseObject("gallery");
															final String menuId = comment.getObjectId().toString();
															final String mtitle = comment.getString("title");
															
															TableRow tr = new TableRow(Event_Single.this);
															tr.setPadding(0, 20, 0, 20);
															TextView icons = new TextView(Event_Single.this);
															icons.setId(count);
															icons.setTypeface(font);
															final String menuType = comment.getString("menuType");
															if(menuType != null){
																if(menuType.equals("list")){
																	icons.setText(getString(R.string.fa_list));  
																} else if (menuType.equals("map")){
																	icons.setText(getString(R.string.fa_map_marker));
																} else if (menuType.equals("page")){
																	icons.setText(getString(R.string.fa_info));
																} else if (menuType.equals("gallery")){
																	icons.setText(getString(R.string.fa_picture_o));
																} else if (menuType.equals("schedule")){
																	icons.setText(getString(R.string.fa_calendar_o));
																} else if (menuType.equals("attendees")){
																	icons.setText(getString(R.string.fa_user));
																} else if (menuType.equals("info")){
																	icons.setText(getString(R.string.fa_info_circle));
																} else if (menuType.equals("information")){
																	icons.setText(getString(R.string.fa_info));
																} else if (menuType.equals("questions")){
																	icons.setText(getString(R.string.fa_question));
																} else if (menuType.equals("files")){
																	icons.setText(getString(R.string.fa_file));
																} else if (menuType.equals("announcements")){
																	icons.setText(getString(R.string.fa_comments));
																} else if (menuType.equals("social_media")){
																	icons.setText(getString(R.string.fa_share));
																} else {
																	icons.setText("...");
																}
															}
															icons.setTextColor(Color.GRAY);
															icons.setPadding(10, 10, 10, 10);
															tr.addView(icons);
								
															final TextView id = new TextView(Event_Single.this);
															id.setText(comment.getString("objectId"));
															tr.addView(id);
								
															TextView myText= new TextView(Event_Single.this);
															myText.setId(count);
															myText.setText(comment.getString("title"));
															myText.setTextColor(Color.BLACK);
															myText.setPadding(10, 10, 10, 10);
															
															tr.setBackgroundResource(drawable.list_selector_background);
															tr.setOnClickListener(new OnClickListener() {								
																@Override
																public void onClick(View v) {
																	if(post != null){
																		eve = post.getObjectId().toString();
																	} else if(gallery != null) {
																		eve = gallery.getObjectId().toString();
																	}
																	String type = " ";
																	if(menuType != null){
																		type = comment.get("menuType").toString();
																	}
																	Intent intent = new Intent(Event_Single.this,MenuDesc.class);
																	Bundle extras = new Bundle();
																	extras.putString(EVENT_ID,event_id);
																	if(eve != null){
																		extras.putString("OBJECT_ID", eve);
																	}
																	extras.putString("MENU_ID", menuId);
																	extras.putString("MENU_TYPE", type);
																	extras.putString("MENU_TITLE", mtitle);
																	intent.putExtras(extras);
																	startActivity(intent);
																}
															});
															tr.addView(myText);

															View line = new View(Event_Single.this);
															line.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1, 1f));
															line.setBackgroundColor(Color.GRAY);
															line.setPadding(0, 5, 0, 5);
															lin.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
															lin.addView(line);
															count++;
														}
													} else {
														Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
													}
												}
											});
										} else {
											ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("Event");
											innerQuery.whereEqualTo("objectId", event_id);
											List<String> arr = new ArrayList();
											arr.add("editor");
											arr.add("publish");
											ParseRelation<ParseObject> relation = event.getRelation("menus");
											ParseQuery<ParseObject> querya = relation.getQuery();
											querya.orderByAscending("order");
											querya.include("post");
											querya.whereEqualTo("enabled", true);
											querya.whereContainedIn("status", arr);
											querya.findInBackground(new FindCallback<ParseObject>() {
												public void done(List<ParseObject> listmenu, ParseException e) {
								
													TableLayout lin = (TableLayout) findViewById(R.id.singledetails);
													Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
													Integer count=1;
													if (e == null) {
														ProgressBar prg = (ProgressBar) findViewById(R.id.eventSingleProgress);
														prg.setVisibility(View.GONE);
														RelativeLayout rel = (RelativeLayout) findViewById(R.id.relEventSingleItems);
														rel.setVisibility(View.VISIBLE);
														for (final ParseObject comment : listmenu) {
															final ParseObject post = comment.getParseObject("page");
																
															final ParseObject gallery = comment.getParseObject("gallery");
															final String menuId = comment.getObjectId().toString();
															final String mtitle = comment.getString("title");
															
															TableRow tr = new TableRow(Event_Single.this);
															tr.setPadding(0, 20, 0, 20);
															TextView icons = new TextView(Event_Single.this);
															icons.setId(count);
															icons.setTypeface(font);
															final String menuType = comment.getString("menuType");
															if(menuType != null){
																if(menuType.equals("list")){
																	icons.setText(getString(R.string.fa_list));  
																} else if (menuType.equals("map")){
																	icons.setText(getString(R.string.fa_map_marker));
																} else if (menuType.equals("page")){
																	icons.setText(getString(R.string.fa_info));
																} else if (menuType.equals("gallery")){
																	icons.setText(getString(R.string.fa_picture_o));
																} else if (menuType.equals("schedule")){
																	icons.setText(getString(R.string.fa_calendar_o));
																} else if (menuType.equals("attendees")){
																	icons.setText(getString(R.string.fa_user));
																} else if (menuType.equals("info")){
																	icons.setText(getString(R.string.fa_info_circle));
																}else if (menuType.equals("information")){
																	icons.setText(getString(R.string.fa_info));
																}else if (menuType.equals("questions")){
																	icons.setText(getString(R.string.fa_question));
																}else if (menuType.equals("files")){
																	icons.setText(getString(R.string.fa_file));
																}else if (menuType.equals("announcements")){
																	icons.setText(getString(R.string.fa_comments));
																} else if (menuType.equals("social_media")){
																	icons.setText(getString(R.string.fa_share));
																} else {
																	icons.setText("...");
																}
															}
															icons.setTextColor(Color.GRAY);
															icons.setPadding(10, 10, 10, 10);
															tr.addView(icons);
								
															final TextView id = new TextView(Event_Single.this);
															id.setText(comment.getString("objectId"));
															tr.addView(id);
								
															TextView myText= new TextView(Event_Single.this);
															myText.setId(count);
															myText.setText(comment.getString("title"));
															myText.setTextColor(Color.BLACK);
															myText.setPadding(10, 10, 10, 10);
															
															tr.setBackgroundResource(drawable.list_selector_background);
															tr.setOnClickListener(new OnClickListener() {								
																@Override
																public void onClick(View v) {
																	if(post != null){
																		eve = post.getObjectId().toString();
																	} else if(gallery != null) {
																		eve = gallery.getObjectId().toString();
																	}
																	String type = " ";
																	if(menuType != null){
																		type = comment.get("menuType").toString();
																	}
																	Intent intent = new Intent(Event_Single.this,MenuDesc.class);
																	Bundle extras = new Bundle();
																	extras.putString(EVENT_ID,event_id);
																	if(eve != null){
																		extras.putString("OBJECT_ID", eve);
																	}
																	extras.putString("MENU_ID", menuId);
																	extras.putString("MENU_TYPE", type);
																	extras.putString("MENU_TITLE", mtitle);
																	intent.putExtras(extras);
																	startActivity(intent);
																}
															});
															tr.addView(myText);

															View line = new View(Event_Single.this);
															line.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1, 1f));
															line.setBackgroundColor(Color.GRAY);
															line.setPadding(0, 5, 0, 5);
															lin.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
															lin.addView(line);
															count++;
														}
													} else {
														// something went wrong
														Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
													}
												}
											});
										}
									} else {
										Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
									}
								}
							});
						} else {
							final ParseUser currentUser = ParseUser.getCurrentUser();
							final ParseObject event = ParseObject.createWithoutData("Event", event_id);
							ParseObject user = ParseObject.createWithoutData("_User", currentUser.getObjectId());
							List<ParseObject> var = new ArrayList<>();
							var.add(event);
							var.add(user);
							ParseQuery<ParseObject> attendeeQuery = ParseQuery.getQuery("EventAttendee");
							attendeeQuery.whereContainedIn("event", var);
							attendeeQuery.whereContainedIn("user", var);
							attendeeQuery.findInBackground(new FindCallback<ParseObject>(){
								public void done(List<ParseObject> attendee, ParseException e){
									if(e==null){
										ArrayList<ParseObject> list = new ArrayList<ParseObject>();
										list.addAll(attendee);
										ParseObject object = ParseObject.createWithoutData("_User", currentUser.getObjectId());
										if(list.isEmpty()){
											ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("Event");
											ParseRelation<ParseObject> relation = event.getRelation("menus");
											innerQuery.whereEqualTo("objectId", event_id);
											ParseQuery<ParseObject> querya = relation.getQuery();
											querya.orderByAscending("order");
											querya.include("post");
											querya.whereEqualTo("enabled", true);
											querya.whereEqualTo("status", "publish");
											querya.whereNotEqualTo("visibility", "attendees");
											querya.findInBackground(new FindCallback<ParseObject>() {
												public void done(List<ParseObject> listmenu, ParseException e) {
								
													TableLayout lin = (TableLayout) findViewById(R.id.singledetails);
													Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
													Integer count=1;
													if (e == null) {
														ProgressBar prg = (ProgressBar) findViewById(R.id.eventSingleProgress);
														prg.setVisibility(View.GONE);
														RelativeLayout rel = (RelativeLayout) findViewById(R.id.relEventSingleItems);
														rel.setVisibility(View.VISIBLE);
														for (final ParseObject comment : listmenu) {
															final ParseObject post = comment.getParseObject("page");
																
															final ParseObject gallery = comment.getParseObject("gallery");
															final String menuId = comment.getObjectId().toString();
															final String mtitle = comment.getString("title");
															
															TableRow tr = new TableRow(Event_Single.this);
															tr.setPadding(0, 20, 0, 20);
															TextView icons = new TextView(Event_Single.this);
															icons.setId(count);
															icons.setTypeface(font);
															final String menuType = comment.getString("menuType");
															if(menuType != null){
																if(menuType.equals("list")){
																	icons.setText(getString(R.string.fa_list));  
																} else if (menuType.equals("map")){
																	icons.setText(getString(R.string.fa_map_marker));
																} else if (menuType.equals("page")){
																	icons.setText(getString(R.string.fa_info));
																} else if (menuType.equals("gallery")){
																	icons.setText(getString(R.string.fa_picture_o));
																} else if (menuType.equals("schedule")){
																	icons.setText(getString(R.string.fa_calendar_o));
																} else if (menuType.equals("attendees")){
																	icons.setText(getString(R.string.fa_user));
																} else if (menuType.equals("info")){
																	icons.setText(getString(R.string.fa_info_circle));
																} else if (menuType.equals("information")){
																	icons.setText(getString(R.string.fa_info));
																} else if (menuType.equals("questions")){
																	icons.setText(getString(R.string.fa_question));
																} else if (menuType.equals("files")){
																	icons.setText(getString(R.string.fa_file));
																} else if (menuType.equals("announcements")){
																	icons.setText(getString(R.string.fa_comments));
																} else if (menuType.equals("social_media")){
																	icons.setText(getString(R.string.fa_share));
																} else {
																	icons.setText("...");
																}
															}
															icons.setTextColor(Color.GRAY);
															icons.setPadding(10, 10, 10, 10);
															tr.addView(icons);
								
															final TextView id = new TextView(Event_Single.this);
															id.setText(comment.getString("objectId"));
															tr.addView(id);
								
															TextView myText= new TextView(Event_Single.this);
															myText.setId(count);
															myText.setText(comment.getString("title"));
															myText.setTextColor(Color.BLACK);
															myText.setPadding(10, 10, 10, 10);
															
															tr.setBackgroundResource(drawable.list_selector_background);
															tr.setOnClickListener(new OnClickListener() {								
																@Override
																public void onClick(View v) {
																	if(post != null){
																		eve = post.getObjectId().toString();
																	} else if(gallery != null) {
																		eve = gallery.getObjectId().toString();
																	}
																	String type = " ";
																	if(menuType != null){
																		type = comment.get("menuType").toString();
																	}
																	Intent intent = new Intent(Event_Single.this,MenuDesc.class);
																	Bundle extras = new Bundle();
																	extras.putString(EVENT_ID,event_id);
																	if(eve != null){
																		extras.putString("OBJECT_ID", eve);
																	}
																	extras.putString("MENU_ID", menuId);
																	extras.putString("MENU_TYPE", type);
																	extras.putString("MENU_TITLE", mtitle);
																	intent.putExtras(extras);
																	startActivity(intent);
																}
															});
															tr.addView(myText);

															View line = new View(Event_Single.this);
															line.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1, 1f));
															line.setBackgroundColor(Color.GRAY);
															line.setPadding(0, 5, 0, 5);
															lin.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
															lin.addView(line);
															count++;
														}
													} else {
														Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
													}
												}
											});
										} else {
											ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("Event");
											ParseRelation<ParseObject> relation = event.getRelation("menus");
											innerQuery.whereEqualTo("objectId", event_id);
											ParseQuery<ParseObject> querya = relation.getQuery();
											querya.orderByAscending("order");
											querya.include("post");
											querya.whereEqualTo("enabled", true);
											querya.findInBackground(new FindCallback<ParseObject>() {
												public void done(List<ParseObject> listmenu, ParseException e) {
								
													TableLayout lin = (TableLayout) findViewById(R.id.singledetails);
													Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
													Integer count=1;
													if (e == null) {
														ProgressBar prg = (ProgressBar) findViewById(R.id.eventSingleProgress);
														prg.setVisibility(View.GONE);
														RelativeLayout rel = (RelativeLayout) findViewById(R.id.relEventSingleItems);
														rel.setVisibility(View.VISIBLE);
														for (final ParseObject comment : listmenu) {
															final ParseObject post = comment.getParseObject("page");
																
															final ParseObject gallery = comment.getParseObject("gallery");
															final String menuId = comment.getObjectId().toString();
															final String mtitle = comment.getString("title");
															
															TableRow tr = new TableRow(Event_Single.this);
															tr.setPadding(0, 20, 0, 20);
															TextView icons = new TextView(Event_Single.this);
															icons.setId(count);
															icons.setTypeface(font);
															final String menuType = comment.getString("menuType");
															if(menuType != null){
																if(menuType.equals("list")){
																	icons.setText(getString(R.string.fa_list));  
																} else if (menuType.equals("map")){
																	icons.setText(getString(R.string.fa_map_marker));
																} else if (menuType.equals("page")){
																	icons.setText(getString(R.string.fa_info));
																} else if (menuType.equals("gallery")){
																	icons.setText(getString(R.string.fa_picture_o));
																} else if (menuType.equals("schedule")){
																	icons.setText(getString(R.string.fa_calendar_o));
																} else if (menuType.equals("attendees")){
																	icons.setText(getString(R.string.fa_user));
																} else if (menuType.equals("info")){
																	icons.setText(getString(R.string.fa_info_circle));
																}else if (menuType.equals("information")){
																	icons.setText(getString(R.string.fa_info));
																}else if (menuType.equals("questions")){
																	icons.setText(getString(R.string.fa_question));
																}else if (menuType.equals("files")){
																	icons.setText(getString(R.string.fa_file));
																}else if (menuType.equals("announcements")){
																	icons.setText(getString(R.string.fa_comments));
																} else if (menuType.equals("social_media")){
																	icons.setText(getString(R.string.fa_share));
																} else {
																	icons.setText("...");
																}
															}
															icons.setTextColor(Color.GRAY);
															icons.setPadding(10, 10, 10, 10);
															tr.addView(icons);
								
															final TextView id = new TextView(Event_Single.this);
															id.setText(comment.getString("objectId"));
															tr.addView(id);
								
															TextView myText= new TextView(Event_Single.this);
															myText.setId(count);
															myText.setText(comment.getString("title"));
															myText.setTextColor(Color.BLACK);
															myText.setPadding(10, 10, 10, 10);
															
															tr.setBackgroundResource(drawable.list_selector_background);
															tr.setOnClickListener(new OnClickListener() {								
																@Override
																public void onClick(View v) {
																	if(post != null){
																		eve = post.getObjectId().toString();
																	} else if(gallery != null) {
																		eve = gallery.getObjectId().toString();
																	}
																	String type = " ";
																	if(menuType != null){
																		type = comment.get("menuType").toString();
																	}
																	Intent intent = new Intent(Event_Single.this,MenuDesc.class);
																	Bundle extras = new Bundle();
																	extras.putString(EVENT_ID,event_id);
																	if(eve != null){
																		extras.putString("OBJECT_ID", eve);
																	}
																	extras.putString("MENU_ID", menuId);
																	extras.putString("MENU_TYPE", type);
																	extras.putString("MENU_TITLE", mtitle);
																	intent.putExtras(extras);
																	startActivity(intent);
																}
															});
															tr.addView(myText);

															View line = new View(Event_Single.this);
															line.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1, 1f));
															line.setBackgroundColor(Color.GRAY);
															line.setPadding(0, 5, 0, 5);
															lin.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
															lin.addView(line);
															count++;
														}
													} else {
														// something went wrong
														Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
													}
												}
											});
										}
									} else {
										Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
									}
								}
							});
						}
					} else {
						Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
					}
				}
			});
			
		} else {
			ParseObject event = ParseObject.createWithoutData("Event", event_id);
			ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("Event");
			innerQuery.whereEqualTo("objectId", event_id);
			ParseRelation<ParseObject> relation = event.getRelation("menus");
			ParseQuery<ParseObject> querya = relation.getQuery();
			querya.orderByAscending("order");
			querya.include("page");
			querya.whereEqualTo("enabled", true);
			querya.whereEqualTo("visibility", "public");
			querya.whereEqualTo("status", "publish");
			querya.findInBackground(new FindCallback<ParseObject>() {
				public void done(List<ParseObject> listmenu, ParseException e) {

					TableLayout lin = (TableLayout) findViewById(R.id.singledetails);
					lin.setPadding(5, 5, 5, 5);
					Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
					Integer count=1;
					if (e == null) {
						ProgressBar prg = (ProgressBar) findViewById(R.id.eventSingleProgress);
						prg.setVisibility(View.GONE);
						RelativeLayout rel = (RelativeLayout) findViewById(R.id.relEventSingleItems);
						rel.setVisibility(View.VISIBLE);
						for (final ParseObject comment : listmenu) {
							
							final ParseObject post = comment.getParseObject("page");
							final ParseObject gallery = comment.getParseObject("gallery");
							final String menuId = comment.getObjectId().toString();
							final String mtitle = comment.getString("title");
							
							final TableRow tr = new TableRow(Event_Single.this);
							tr.setPadding(0, 20, 0, 20);
							TextView icons = new TextView(Event_Single.this);
							icons.setId(count);
							icons.setTypeface(font);
							final String menuType = comment.getString("menuType");
							if (menuType != null){
								if(menuType.equals("list")){
									icons.setText(getString(R.string.fa_list));  
								} else if (menuType.equals("map")){
									icons.setText(getString(R.string.fa_map_marker));
								} else if (menuType.equals("page")){
									icons.setText(getString(R.string.fa_info));
								} else if (menuType.equals("gallery")){
									icons.setText(getString(R.string.fa_picture_o));
								} else if (menuType.equals("schedule")){
									icons.setText(getString(R.string.fa_calendar_o));
								} else if (menuType.equals("attendees")){
									icons.setText(getString(R.string.fa_user));
								} else if (menuType.equals("info")){
									icons.setText(getString(R.string.fa_info_circle));
								}else if (menuType.equals("information")){
									icons.setText(getString(R.string.fa_info));
								}else if (menuType.equals("questions")){
									icons.setText(getString(R.string.fa_question));
								}else if (menuType.equals("files")){
									icons.setText(getString(R.string.fa_file));
								}else if (menuType.equals("announcements")){
									icons.setText(getString(R.string.fa_comments));
								} else if (menuType.equals("social_media")){
									icons.setText(getString(R.string.fa_share));
								} else {
									icons.setText("...");
								}
							} else {
								icons.setText("!!!");
							}
							icons.setTextColor(Color.GRAY);
							icons.setPadding(10, 10, 10, 10);
							tr.addView(icons);

							final TextView id = new TextView(Event_Single.this);
							id.setText(comment.getString("objectId"));
							tr.addView(id);

							TextView myText= new TextView(Event_Single.this);
							myText.setId(count);
							myText.setText(comment.getString("title"));
							myText.setTextColor(Color.BLACK);
							myText.setPadding(10, 10, 10, 10);
							
							tr.setBackgroundResource(drawable.list_selector_background);
							// No Login
							tr.setOnClickListener(new OnClickListener() {								
								@Override
								public void onClick(View v) {
									if(post != null){
										eve = post.getObjectId().toString();
									} else if(gallery != null) {
										eve = gallery.getObjectId().toString();
									}
									String type = " ";
									if(menuType != null){
										type = comment.get("menuType").toString();
									}
									Intent intent = new Intent(Event_Single.this,MenuDesc.class);
									Bundle extras = new Bundle();
									extras.putString(EVENT_ID,event_id);
									if(eve != null){
										extras.putString("OBJECT_ID", eve);
									}
									extras.putString("MENU_ID", menuId);
									extras.putString("MENU_TYPE", type);
									extras.putString("MENU_TITLE", mtitle);
									intent.putExtras(extras);
									startActivity(intent);
								}
							});
							// No Login
							tr.addView(myText);

							View line = new View(Event_Single.this);
							line.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1, 1f));
							line.setBackgroundColor(Color.GRAY);
							line.setPadding(0, 5, 0, 5);
							lin.addView(tr, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
							lin.addView(line);
							count++;
						}
					} else {
						// something went wrong
						Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
					}
				}
			});
		}
	}

	public void btn_join_event(View view) {
		Toast.makeText(getApplicationContext(),"Join Event",Toast.LENGTH_LONG).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event__single, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			finish();
			return true;
		case R.id.action_login:
			Intent login = new Intent(getApplicationContext(), LoginActivity.class);
			startActivity(login);
			finish();
			return true;
		case R.id.action_join:
			join();
			return true;
		case R.id.action_unjoin:
			unjoin();
			return true;
		case R.id.action_eventQR:
			eventQR();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void eventQR(){
		Bundle extra = new Bundle();
		Intent intent = new Intent(this, MyProfileQRCode.class);
		
		extra.putString("OBJECT_ID", event_id);
		extra.putString("ACTION_NAME", "joinEvent");
		extra.putString("ACTION_TITLE", eventName+" QR Code");
		
		intent.putExtras(extra);
		startActivity(intent);
		
	}
	
	public void unjoin(){
		final ParseUser currentUser = ParseUser.getCurrentUser();
		final ParseObject event = ParseObject.createWithoutData("Event", event_id);
		final ParseObject user = ParseObject.createWithoutData("_User", currentUser.getObjectId());
		final List<ParseObject> var = new ArrayList<>();
		var.add(event);
		var.add(user);
		ParseQuery<ParseObject> joinQuery = ParseQuery.getQuery("EventAttendee");
		joinQuery.whereContainedIn("event", var);
		joinQuery.whereContainedIn("user", var);
		joinQuery.findInBackground(new FindCallback<ParseObject>(){
			public void done(List<ParseObject> isAttendee, ParseException e){
				List<ParseObject> attendee = new ArrayList<ParseObject>();
				attendee.addAll(isAttendee);
				if(!attendee.isEmpty()){
					for(ParseObject ob : isAttendee){
						ob.remove("event");
						ob.remove("user");
						ob.saveInBackground(new SaveCallback() {
							@Override
							public void done(ParseException e) {
								if(e==null){
									Intent intent = new Intent(Event_Single.this,Events_List.class);
									startActivity(intent);
								} else {
									Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
								}
							}
						});
					}
				} else {
					Toast.makeText(getApplicationContext(), "You are not yet included in this event.", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	public void join(){
		ParseQuery<ParseObject> privateEvent = ParseQuery.getQuery("Event");
		privateEvent.getInBackground(event_id, new GetCallback<ParseObject>() {
			@Override
			public void done(ParseObject object, ParseException e) {
				// TODO Auto-generated method stub
				if(e==null){
					final boolean isPrivate = object.getBoolean("isPrivate");
					AlertDialog.Builder joinEvent = new AlertDialog.Builder(Event_Single.this);
					joinEvent.setTitle("Join Event");
					joinEvent.setMessage("Are you sure you want to join this event?");
					joinEvent.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							if(isPrivate){
								AlertDialog.Builder isPrivateEvent = new AlertDialog.Builder(Event_Single.this);
								isPrivateEvent.setTitle("Private Event");
								isPrivateEvent.setMessage("This is a private event. Please wait for the admin confirmation.");
								isPrivateEvent.setPositiveButton("Ok.", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										ParseUser currentUser = ParseUser.getCurrentUser();
										ParseObject join = ParseObject.createWithoutData("_User", currentUser.getObjectId());
										ParseObject event = ParseObject.createWithoutData("Event", event_id);
										ParseObject joining = new ParseObject("EventAttendee");
										joining.put("event", event);
										joining.put("user", join);
										joining.saveInBackground();
										Intent intent = new Intent(Event_Single.this,Events_List.class);
										startActivity(intent);
									}
								});
								isPrivateEvent.show();
							} else {
								ParseUser currentUser = ParseUser.getCurrentUser();
								ParseObject join = ParseObject.createWithoutData("_User", currentUser.getObjectId());
								ParseObject event = ParseObject.createWithoutData("Event", event_id);
								ParseObject joining = new ParseObject("EventAttendee");
								joining.put("event", event);
								joining.put("user", join);
								joining.saveInBackground();
								Intent intent = new Intent(Event_Single.this,Events_List.class);
								startActivity(intent);
							}
						}
						
					});
					joinEvent.setNegativeButton("No.", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
					joinEvent.show();
				} else {
					Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem item_login = menu.findItem(R.id.action_login);
		final MenuItem item_join = menu.findItem(R.id.action_join);
		final MenuItem item_unjoin = menu.findItem(R.id.action_unjoin);
		final MenuItem item_eventQR = menu.findItem(R.id.action_eventQR);
		
		final ParseUser currentUser = ParseUser.getCurrentUser();
		
		if (currentUser != null) {
			ParseObject event = ParseObject.createWithoutData("Event", event_id);
			ParseObject user = ParseObject.createWithoutData("_User", currentUser.getObjectId());
			List<ParseObject> var = new ArrayList<>();
			var.add(event);
			var.add(user);
			ParseQuery<ParseObject> joinQuery = ParseQuery.getQuery("EventAttendee");
			joinQuery.whereContainedIn("event", var);
			joinQuery.whereContainedIn("user", var);
//			joinQuery.include("checkedIn");
			joinQuery.findInBackground(new FindCallback<ParseObject>(){
				public void done(List<ParseObject> isAttendee, ParseException e){
					List<ParseObject> attendee = new ArrayList<ParseObject>();
					attendee.addAll(isAttendee);
					if(attendee.isEmpty()){
						item_unjoin.setVisible(false);
					} else {
						for(ParseObject ob : isAttendee){
							boolean isCheckedIn = ob.getBoolean("checkedIn");
							if(isCheckedIn){
//								item_checkOut.setVisible(true);
							} else {
//								item_checkOut.setVisible(false);
							}
						}
						item_join.setVisible(false);
					}
				}
			});
			item_login.setVisible(false);
		} else {
			item_eventQR.setVisible(false);
			item_join.setVisible(false);
			item_unjoin.setVisible(false);
		}
//		invalidateOptionsMenu();
		return true;
	}

	protected boolean isRouteDisplayed() {
		return false;
	}
}