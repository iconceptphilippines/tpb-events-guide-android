package com.iconcept.tpbguide.tpbevents;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import android.*;
import android.Manifest;
import android.R.drawable;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.Announcements;
import com.iconcept.tpbguide.tpbevents.Models.Download;
import com.iconcept.tpbguide.tpbevents.Models.Schedule;
import com.iconcept.tpbguide.tpbevents.imagecaching.GridViewActivity;
import com.iconcept.tpbguide.tpbevents.imagecaching.GridViewAdapter;
import com.iconcept.tpbguide.tpbevents.imagecaching.ViewPagerActivity;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ParseRelation;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

public  class MenuDesc extends Activity{
	public String id;
	public String type;
	public String menu_id;
	public String userId;
	Bundle intent;
	CustomInfoList items;
	private GoogleMap map;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu_desc);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
//		invalidateOptionsMenu();
		intent = getIntent().getExtras();
		String title = intent.getString("MENU_TITLE");
		setTitle(title);
		type = intent.getString("MENU_TYPE").toString();
		TextView view = (TextView) findViewById(R.id.type);

		switch (type){
		case "list":
			list();
			break;
		case "map":
			map();
			break;
		case "page":
			info();
			break;
		case "gallery":
			id = intent.getString("OBJECT_ID");
			Intent intent = new Intent(getApplicationContext(), GridViewActivity.class);
			GridViewActivity.setId(id);
			GridViewAdapter.setId(id);
			intent.putExtra("ALBUM_TITLE", id);
			startActivity(intent);
			finish();
			break;
		case "schedule":
			sched();
			break;
		case "attendees":
			attend();
			break;
		case "info":
			event_info();
			break;
		case "announcements":
			announce();
			break;
		case "questions":
			questions();
			break;
		case "files":
			download();
			break;
		case "social_media":
			socialMedia();
			break;
		default:
			break;
		}

	}
	
	public void socialMedia(){
		id=intent.getString("MENU_ID");
		setContentView(R.layout.social_media_list);
		
		final ListView socialMediaList = (ListView) findViewById(R.id.socialListItems);
		final ArrayList<String> socialListData = new ArrayList<String>();
		final ArrayAdapter<String> socialListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,socialListData);
		
		socialMediaList.setAdapter(socialListAdapter);
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Menu");
		query.whereEqualTo("objectId", id);
		query.getInBackground(id, new GetCallback<ParseObject>() {
			@Override
			public void done (final ParseObject object, ParseException e){
				if(e==null){
					if(!object.getString("facebookLabel").isEmpty()){
						socialListAdapter.add(object.getString("facebookLabel"));
						socialListAdapter.notifyDataSetChanged();
					}
					if(!object.getString("twitterTweet").isEmpty()){
						socialListAdapter.add(object.getString("twitterLabel"));
						socialListAdapter.notifyDataSetChanged();
					}
					socialMediaList.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,View view, int position, long id) {
							// TODO Auto-generated method stub
							String label = socialListAdapter.getItem(position);
							if(label.toLowerCase().contains("facebook")){
								String facebookPackageName = "com.facebook.katana";
                                String facebookClassName = "com.facebook.katana.LoginActivity";
                                try {
                                    ApplicationInfo facebookAppInfo = getPackageManager().getApplicationInfo(facebookPackageName, 0);
                                    Intent intent = new Intent("android.intent.category.LAUNCHER");
                                    intent.setClassName(facebookPackageName, facebookClassName);
                                    startActivity(intent);
                                } catch (NameNotFoundException e) {
                                // Didn't installed
                                    Toast.makeText(getApplicationContext(), "Facebook app not found! Please install application.", Toast.LENGTH_LONG).show();
                                //Start Market to downoad and install Facebook App
                                
                               }
							}
							if(label.toLowerCase().contains("twitter")){
								String hashTag = object.getString("twitterTweet");
								String twitterPackageName = "com.twitter.android";
//                                String facebookClassName = "com.facebook.katana.LoginActivity";
                                try {
                                    ApplicationInfo twitterAppInfo = getPackageManager().getApplicationInfo(twitterPackageName, 0);
                                    TweetComposer.Builder builder = new TweetComposer.Builder(MenuDesc.this).text(hashTag);
                                    builder.show();
//                                    startActivity(intent);
                                } catch (NameNotFoundException e) {
                                // Didn't installed
                                    Toast.makeText(getApplicationContext(), "Twitter app not found! Please install application.", Toast.LENGTH_LONG).show();
                                
                               }
							}
						}
					});
					socialListAdapter.notifyDataSetChanged();
				} else {
					Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	public void download() {
		id=intent.getString("MENU_ID");
		setContentView(R.layout.download_list);
		
		final ListView documentList = (ListView) findViewById(R.id.downloadList);
		final ArrayList<Download> documentListData = new ArrayList<Download>();
		final DownloadAdapter downloadListAdapter = new DownloadAdapter(this, id, documentListData); 
		
		documentList.setAdapter(downloadListAdapter);
		
		ParseObject menu = ParseObject.createWithoutData("Menu", id);
		ParseRelation<Download> rL = menu.getRelation("files");
		ParseQuery<Download> query = rL.getQuery();
		
		query.findInBackground(new FindCallback<Download>() {
			@Override
			public void done(final List<Download> downloads, ParseException e) {
				if (e == null) {
					documentListData.addAll(downloads);
					documentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							final Download download = (Download) downloads.get(position);
							String downloadUrl = null;
							if (download.getFileSource().equals("file") && download.getFile() != null) {
								downloadUrl = download.getFile().getUrl();
							} else {
								downloadUrl = download.getFileUrl();
							}
							
							Intent internetIntent = new Intent(Intent.ACTION_VIEW);
							internetIntent.setData(Uri.parse(downloadUrl));
//							internetIntent.setComponent(new ComponentName("com.android.browser","com.android.browser.BrowserActivity"));
//							internetIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(internetIntent);
						}
					});
					downloadListAdapter.notifyDataSetChanged();
				} else {
					Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}
		});
	}
	public void questions() {
		id = intent.getString(Event_Single.EVENT_ID);
		setContentView(R.layout.session_questions);
		if(ParseUser.getCurrentUser() != null){
			userId = ParseUser.getCurrentUser().getObjectId().toString();
		}
		final ParseObject eventId = ParseObject.createWithoutData("Event", id);
		
		setContentView(R.layout.list_items_sub_list);
		final ListView listViewItems = (ListView) findViewById(R.id.listSubItems);
		ArrayList<ParseObject> listItems = new ArrayList<ParseObject>();
		final DefaultAdapter items = new DefaultAdapter(getApplicationContext(), listItems);
		
		listViewItems.setAdapter(items);
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Topic");
		query.whereEqualTo("event", eventId);
		query.findInBackground(new FindCallback<ParseObject>(){

			@Override
			public void done(final List<ParseObject> topics, ParseException e) {
				// TODO Auto-generated method stub
				if(e == null){
					items.addAll(topics);
					listViewItems.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long theID) {
							// TODO Auto-generated method stub
							String pid = topics.get(position).getObjectId().toString();
							Intent i = new Intent(getApplicationContext(), SessionQuestions.class);
							Bundle bundle = new Bundle();
							// GET the IDs for submission to session questions
							bundle.putString("TOPIC_ID", pid); // topic id
							bundle.putString("USER_ID", userId); // user id of current user
							bundle.putString("EVENT_ID", id); // event id 
							
							i.putExtras(bundle);
							startActivity(i);
						}
					});
					items.notifyDataSetChanged();
				} else {
					Log.e("ERROR  ==========>", e.getMessage());
				}
			}
			
		});
		
	}
	public void announce(){
		id=intent.getString(Event_Single.EVENT_ID);
		setContentView(R.layout.announce_list);
		final ListView announcementList = (ListView) findViewById(R.id.listItems);
		final ArrayList<Announcements> announceListData = new ArrayList<Announcements>();
		final AnnouncementAdapter announceListAdapter = new AnnouncementAdapter(this, id, announceListData); 
		
		announcementList.setAdapter(announceListAdapter);
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
		query.whereEqualTo("objectId", id);
		ParseQuery<Announcements> inner = ParseQuery.getQuery(Announcements.class);
		inner.whereMatchesQuery("event", query);
		inner.findInBackground(new FindCallback<Announcements>() {
			@Override
			public void done(final List<Announcements> announcement, ParseException e) {
				if(e==null){
					announceListData.addAll(announcement);
					announcementList.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							Announcements announce = (Announcements) announcement.get(position);
							Intent i = new Intent(getApplicationContext(), AnnouncementDetails.class);
							i.putExtra("OBJECT_ID", announce.getObjectId().toString());
							startActivity(i);
						}
					});
					announceListAdapter.notifyDataSetChanged();
				} else {
					Toast.makeText(getApplicationContext(), "Unable to Fetch Data", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	public void event_info(){
		id = intent.getString(Event_Single.EVENT_ID);
		setContentView(R.layout.event_info);
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
		query.getInBackground(id, new GetCallback<ParseObject>() {
			public void done(final ParseObject object, ParseException e) {
				if (e == null) {
					ProgressBar prog = (ProgressBar) findViewById(R.id.progressEvent);
					prog.setVisibility(View.GONE);
					
					RelativeLayout relEvent = (RelativeLayout) findViewById(R.id.relEvent);
					relEvent.setVisibility(View.VISIBLE);
					
					Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
					ParseImageView eventsImage = (ParseImageView) findViewById(R.id.event_info_image);
					ParseFile imageFile = object.getParseFile("image");
					if (imageFile != null) {
						eventsImage.setParseFile(imageFile);
						eventsImage.loadInBackground();
					} else {
						eventsImage.setVisibility(View.GONE);
					}
					
					// display event title
					TextView eventTitle = (TextView) findViewById(R.id.event_info_title);
					eventTitle.setText(object.getString("title"));

					//display event date
					SimpleDateFormat end = new SimpleDateFormat("LLLL dd, yyyy"); // your required format
					SimpleDateFormat start = new SimpleDateFormat("LLLL dd");
					start.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
					end.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
					String startDate = start.format(object.getDate("startDate")); // convert it in your required format
					String startOnly = end.format(object.getDate("startDate"));
					String endDate = null;
					if(object.getDate("endDate") != null){
						endDate = end.format(object.getDate("endDate")); // convert it in your required format
					}
					
					TextView eventDate = (TextView) findViewById(R.id.event_info_date); // event date
					if(object.getDate("endDate") ==  null){
						eventDate.setText(startOnly);					
					} else {
						eventDate.setText(startDate + " - " + endDate);
					}
					
					final RelativeLayout evnt = (RelativeLayout) findViewById(R.id.rel_loc_info);
					//display event location map
					if(object.getParseGeoPoint("coordinates") != null){
						final ParseGeoPoint geoPoint = object.getParseGeoPoint("coordinates");
						if( geoPoint.getLongitude() != 0 || geoPoint.getLatitude() != 0 )
						{
							TextView eventLocation = (TextView) findViewById(R.id.event_info_location);
							TextView chev = (TextView) findViewById(R.id.chev_right);
							chev.setTypeface(font);
							chev.setText(getString(R.string.fa_chevron_right));
							eventLocation.setTypeface(font);
							eventLocation.setText(getString(R.string.fa_map_marker) + " " + object.getString("address"));
							evnt.setBackgroundResource(drawable.list_selector_background);

							OnClickListener locationClicked = new OnClickListener() {
								@Override
								public void onClick(View v) {	
									evnt.performClick();
								}
							};

							evnt.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {	
									if(geoPoint != null){
										Double lat = geoPoint.getLatitude();
										Double lon = geoPoint.getLongitude();
										String latitude = String.valueOf(lat);
										String longitude = String.valueOf(lon);
										Intent intent = new Intent(getApplicationContext(), Map.class);
										intent.putExtra("LATITUDE", latitude);
										intent.putExtra("LONGITUDE", longitude);
										startActivity(intent);
									}
								}
							});
							eventLocation.setOnClickListener(locationClicked);
							chev.setOnClickListener(locationClicked);
						}else{
							evnt.setVisibility(View.GONE);
						}
					} else {
						evnt.setVisibility(View.GONE);
					}
					//display description
					String css = "file:///android_asset/face.css";
					String myData = object.getString("content");
					String htmlWithCss = "<html><head><style type=\"text/css\">" +css +"</style></head><body>" + myData +"</body></html>";
					WebView eventDescription = (WebView) findViewById(R.id.event_info_description);
					eventDescription.setBackgroundColor(0x00000000);
					eventDescription.getSettings().setJavaScriptEnabled(true);
					eventDescription.loadDataWithBaseURL("", htmlWithCss, "text/html", "UTF-8", "");
				} else {
					// something went wrong
					Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
				}
			}

		});
	}
	
	ListView attendeesList;
	public void attend(){

		if( ParseUser.getCurrentUser() != null )
		{
			userId = String.valueOf(ParseUser.getCurrentUser().getObjectId());
		}
		id = intent.getString(Event_Single.EVENT_ID);
		setContentView(R.layout.attendees_list);
		attendeesList = (ListView) findViewById(R.id.attendeesList);
		final ArrayList<ParseObject> attendeesListData = new ArrayList<ParseObject>();
		final AttendeesAdapter attendeesListAdapter = new AttendeesAdapter(this, attendeesListData); 
		attendeesList.setAdapter(attendeesListAdapter);
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("eventId", id);
		ParseCloud.callFunctionInBackground("getEventAttendees", params, new FunctionCallback<ArrayList<?>>() {
			public void done(ArrayList<?> attendees, ParseException e) {
				for (int i = 0; i < attendees.size(); i++) {
					if(!attendees.get(i).equals(null)){
						ParseUser pUser = (ParseUser) attendees.get(i);
						pUser.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
							@Override
							public void done(ParseObject user, ParseException e) {
								if(e== null){
									if( !String.valueOf(user.getObjectId()).equals(userId) )
									{
										attendeesListData.add(user);         
										attendeesList.setOnItemClickListener(new OnItemClickListener() {
											@Override
											public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
												String userObjId = attendeesListData.get(position).getObjectId().toString();
												Intent intent = new Intent(getApplicationContext(),AttendeesProfile.class);
												intent.putExtra("UserId", userObjId);
												startActivity(intent);          
											}
										});	
									}
								} else {
									Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
								}
							}
						});
					}
				}
				attendeesListAdapter.notifyDataSetChanged();
			}
		});
	}
		
    ListView schedList;
    String schedTitle = null;
    String subId = null;
	public void sched() {
		id = intent.getString("OBJECT_ID");
		setContentView(R.layout.schedule_list);
		
		schedList = (ListView) findViewById(R.id.schedList);
		final ArrayList<Schedule> schedListData = new ArrayList<Schedule>();
		final ScheduleListAdapter schedListAdapter = new ScheduleListAdapter(this, id, schedListData); 		
		
		schedList.setAdapter(schedListAdapter);
		
		ParseQuery<ParseObject> inner = ParseQuery.getQuery("Post");
		inner.whereEqualTo("objectId", id);
		ParseQuery<Schedule> query = ParseQuery.getQuery(Schedule.class);
		query.whereMatchesQuery("post", inner);
		query.include("scheduleSlots");
		query.findInBackground(new FindCallback<Schedule>() {
			@Override
			public void done(final List<Schedule> sched, ParseException e) {
				if(e==null){
					schedListData.addAll(sched);
					schedList.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							Schedule schedule = (Schedule) sched.get(position);
							SimpleDateFormat date = new SimpleDateFormat("LLLL dd, yyyy - cccc");
							date.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
							String eventSchedDate = date.format(schedule.getDate());
							Intent intent = new Intent(MenuDesc.this, ScheduleSlotItems.class);
							if(schedule.getScheduleSlots() != null){
								intent.putExtra("ARRAY_VALUES", schedule.getScheduleSlots().toString());
								intent.putExtra("SCHEDULE_SLOT_DATE", eventSchedDate);
								startActivity(intent);
							}
						}
					});
					schedListAdapter.notifyDataSetChanged();
				} else {
					Toast.makeText(getApplicationContext(), "Unable to Fetch Data!", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	public void info(){
		setContentView(R.layout.activity_schedule_details);
		id = intent.getString("OBJECT_ID");
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Page");
		
		query.whereEqualTo("objectId", id);
		query.getInBackground(id, new GetCallback<ParseObject>() {
			public void done(final ParseObject object, ParseException e) {
				if (e == null) {
					ProgressBar prog = (ProgressBar) findViewById(R.id.progressBar1);
					prog.setVisibility(View.GONE);
					RelativeLayout relayout = (RelativeLayout) findViewById(R.id.rel_details);
					relayout.setVisibility(View.VISIBLE);
					
					//display description
					ParseImageView imgView = (ParseImageView) findViewById(R.id.activityImg);
					TextView date = (TextView) findViewById(R.id.schedDetailDate);
					TextView title = (TextView) findViewById(R.id.schedDetailTitle);
					TextView location = (TextView) findViewById(R.id.schedDetaillocation);
					RelativeLayout loc = (RelativeLayout) findViewById(R.id.rel_loc_info);
					
					if(object.getParseFile("image") != null){
						imgView.setParseFile(object.getParseFile("image"));
						imgView.loadInBackground();
					} else {
						imgView.setVisibility(View.GONE);
					}
					
					if(object.getString("address") != null){
						if( !object.getString("address").equals("") )
						{
							Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
							final RelativeLayout eventLocation = (RelativeLayout) findViewById(R.id.rel_loc_info);
							location.setTypeface(font);
							TextView chev = (TextView) findViewById(R.id.chev_right);
							chev.setTypeface(font);
							chev.setText(getString(R.string.fa_chevron_right));
							location.setText(getString(R.string.fa_map_marker) + "   " + object.getString("address"));
							final ParseGeoPoint geoPoint = object.getParseGeoPoint("coordinates");
							if(geoPoint != null){
								if( geoPoint.getLatitude() != 0 || geoPoint.getLongitude() != 0 )
								{
									eventLocation.setBackgroundResource(drawable.list_selector_background);
									View.OnClickListener locationClicked = new View.OnClickListener() {
										@Override
										public void onClick(View v) {	
											eventLocation.performClick();
										}
									};

									eventLocation.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {	
											if(geoPoint != null){
												Double lat = geoPoint.getLatitude();
												Double lon = geoPoint.getLongitude();
												String latitude = String.valueOf(lat);
												String longitude = String.valueOf(lon);
												Intent intent = new Intent(getApplicationContext(), Map.class);
												intent.putExtra("LATITUDE", latitude);
												intent.putExtra("LONGITUDE", longitude);
												startActivity(intent);
											}
										}
									});
									location.setOnClickListener(locationClicked);
									chev.setOnClickListener(locationClicked);
								}else{
									loc.setVisibility(View.GONE);
								}
							}
						}else{
							loc.setVisibility(View.GONE);
						}
					} else {
						loc.setVisibility(View.GONE);
					}
					title.setText(object.getString("title"));
					date.setText(object.getString("excerpt"));
					String myData = object.getString("content");
					WebView eventDescription = (WebView) findViewById(R.id.schedDetailInfo);
					if(myData != ""){
						String css =  "file:///android_asset/face.css";
						String htmlWithCss = "<html><head><style type=\"text/css\">" +css +"</style></head><body>" + myData +"</body></html>";
						eventDescription.setBackgroundColor(Color.WHITE);
						eventDescription.getSettings().setJavaScriptEnabled(true);
						eventDescription.loadDataWithBaseURL("", htmlWithCss, "text/html", "UTF-8", "");
					} else {
						eventDescription.setVisibility(View.GONE);
					}

				} else {
					// something went wrong
					Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
				}
			}
		});
	}
	public void map(){
		id = intent.getString(Event_Single.EVENT_ID);
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
		query.whereEqualTo("objectId", id);
		final MenuDesc menuDesc_ = this;
		query.getInBackground(id, new GetCallback<ParseObject>() {
			@Override
			public void done(final ParseObject object, ParseException e) {
				if (e == null) {
					setContentView(R.layout.map_location);
					ParseGeoPoint point = object.getParseGeoPoint("coordinates");
					Double lat = point.getLatitude();
					Double lon = point.getLongitude();
					final LatLng latLng = new LatLng(lat,lon);

					MapFragment fm = (MapFragment) getFragmentManager().findFragmentById(R.id.locationMap);
					fm.getMapAsync(new OnMapReadyCallback() {
						@Override
					    public void onMapReady(GoogleMap map) {
					        map.setMyLocationEnabled(true);
							map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
					        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

					    	String add = object.getString("addressLine1") + " " + object.getString("city") + ", " + object.getString("stateOrProvince") + " " + object.getString("zipCode");
							map.addMarker(new MarkerOptions().position(latLng).title(object.getString("title")).snippet(add));
						}
					});
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
						if (checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
							return;
						}
					}
				} else {
					// something went wrong
					Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
				}
			}
		});
	}
	public void list(){
		setContentView(R.layout.list_items_sub_list);
		id = intent.getString("MENU_ID");
		menu_id = intent.getString("OBJECT_ID");
		final ListView listViewItems = (ListView) findViewById(R.id.listSubItems);
		ArrayList<ParseObject> listItems = new ArrayList<ParseObject>();
		final DefaultAdapter items = new DefaultAdapter(getApplicationContext(), listItems);
		
		listViewItems.setAdapter(items);
		ParseObject ob = ParseObject.createWithoutData("Menu", id);
		ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("Menu");
		
		innerQuery.whereEqualTo("objectId", menu_id);
		ParseRelation<ParseObject> rel = ob.getRelation("pages");
		ParseQuery<ParseObject> querya = rel.getQuery();
		querya.orderByAscending("order");
		querya.findInBackground(new FindCallback<ParseObject>() {
			public void done(final List<ParseObject> listmenu, ParseException e) {
				if (e == null) {
					items.addAll(listmenu);
					listViewItems.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent,
							View view, int position, long id) {
							String pid = listmenu.get(position).getObjectId().toString();
							Intent i = new Intent(getApplicationContext(), ListItemsDetails.class);
							i.putExtra("ID", pid);
							startActivity(i);
						}
					});
					items.notifyDataSetChanged();
				} else {
					// something went wrong
					Toast.makeText(getApplicationContext(),"Sorry, Unable to Fetch Data.",Toast.LENGTH_LONG).show();
				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	protected boolean isRouteDisplayed() {
		return false;
	}
}
