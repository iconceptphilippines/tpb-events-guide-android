package com.iconcept.tpbguide.tpbevents.tpbtodolists;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.iconcept.tpbguide.tpbevents.R;

import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TaskDone extends Fragment{

	ProgressBar waitTaskCancelled;
	TextView labelCursorCancelled, labelEmptyCancelled;
	
	ArrayList<ToDoLists> todoList = new ArrayList<ToDoLists>();
	
	MyCustomAdapter dataAdapter3 = null;

	String current_User;
	
	int itemPosition;
	
	View rootView1;
	
	int count2 = 0;
	ListView listView3;
	
	String [] taskID = new String[100];
	String [] taskName = new String[100];
	String [] taskStatus = new String[100];
	Date [] taskCreated = new Date[100];
	Date [] taskModified = new Date[100];

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		rootView1 = inflater.inflate(R.layout.task_cancelled, container, false);
		
		currentUser();
		
	    initComponents();
		currentUser();
		  	
		return rootView1;
	}
	
	public void onPause(){
		super.onPause();

  		try{
	 	    dataAdapter3.clear();
 	    	dataAdapter3.notifyDataSetChanged();
    	}
		
    	catch(Exception e){
    	}
		
	}
	
	public void onResume(){
		super.onResume();
		 
		updateData();
		waitCursor(true);
		labelEmptyCancelled.setVisibility(4);
	}
	
	private void initComponents(){
		 
		 waitTaskCancelled = (ProgressBar) rootView1.findViewById(R.id.prgWaitCursorCancelled);
		 labelCursorCancelled = (TextView) rootView1.findViewById(R.id.lblWaitCursorCancelled);
		 labelEmptyCancelled = (TextView) rootView1.findViewById(R.id.lblEmptyCancelled);
		 labelEmptyCancelled.setVisibility(4);
	}
	
	 
	 private void currentUser(){
	    	
		 ParseUser currentUser = ParseUser.getCurrentUser();
		 if (currentUser != null) {
			 current_User = currentUser.getUsername().toString();
	     }
	 }
	 
	 private void showMessage(String title, String message) {
		 
		 Builder builder=new Builder(rootView1.getContext());
		 builder.setCancelable(true);
		 builder.setTitle(title);
		 builder.setMessage(message);
		 builder.setPositiveButton("Ok.", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		 builder.show();
	 }
	 
	 private void waitCursor(boolean waiting){
		 
		 if(waiting == true){
			 waitTaskCancelled.setVisibility(0); //visible
			 labelCursorCancelled.setVisibility(0);
		 }
		 else{
			 waitTaskCancelled.setVisibility(4); //invisible
			 labelCursorCancelled.setVisibility(4);
		 }
		 
	 }
	 
	 private void displayListView(String name, String status, Date created) {
		 
			count2 = 0;
			
			ToDoLists country = new ToDoLists(name, status, false);
			todoList.add(country);
			 
			//create an ArrayAdaptar from the String Array
			dataAdapter3 = new MyCustomAdapter(rootView1.getContext(),
					R.layout.task_cancelled, todoList);
			listView3 = (ListView) rootView1.findViewById(R.id.list1);
			// Assign adapter to ListView
			listView3.setAdapter(dataAdapter3);
			
			listView3.setVerticalScrollBarEnabled(false); 
	 
	 }
	 
	 private class MyCustomAdapter extends ArrayAdapter<ToDoLists> {
		 
			private ArrayList<ToDoLists> todoList;
			
		    LayoutInflater mInflater;

			public MyCustomAdapter(Context context, int textViewResourceId, 
				ArrayList<ToDoLists> todoList) {
				
				super(context, textViewResourceId, todoList);
				
				mInflater = LayoutInflater.from(context);
				this.todoList = new ArrayList<ToDoLists>();
				this.todoList.addAll(todoList);
			}
	 
			private class ViewHolder {
				TextView title3, description3;
				ImageButton icon3;
				RelativeLayout rel3;
			}
	 
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
	 
				ViewHolder holder = null;
				Log.v("ConvertView", String.valueOf(position));
	 
				if (convertView == null) {
					//LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					convertView = mInflater.inflate(R.layout.todo_ongoing, null);
	 
					holder = new ViewHolder();
					holder.rel3 = (RelativeLayout) convertView.findViewById(R.id.relative3);
					holder.description3 = (TextView) convertView.findViewById(R.id.lblDescOngoing);
					holder.title3 = (TextView) convertView.findViewById(R.id.lblTitleOngoing);
					holder.icon3 = (ImageButton) convertView.findViewById(R.id.imgButton3);
					convertView.setTag(holder);
	 
					holder.icon3.setOnClickListener( new View.OnClickListener() {  
						public void onClick(View v) {
							
							final int item2 = listView3.getPositionForView((View) v.getParent());
						
							//Toast.makeText(rootView1.getContext(),"s"+ item2, Toast.LENGTH_SHORT).show();
							customDialog(taskName[item2], item2);
						}  
					});  
					
					holder.rel3.setOnClickListener( new View.OnClickListener() {  
						public void onClick(View v1) {
							
							final int item2 = listView3.getPositionForView((View) v1.getParent());
							
	
							showMessage("", taskName[item2]+ '\n' + '\n' +"Date Created: "+'\n' + taskCreated[item2]
									     +'\n'+'\n'+ "Date Modified: "+ taskModified[item2]);
							
						}
					});
				} 
				
				else {
					holder = (ViewHolder) convertView.getTag();
				}
	 
				ToDoLists todolist = todoList.get(position);
				
				holder.title3.setText(" " +  todolist.getCode());
				holder.description3.setText(" (" + todolist.getName() + ")");
	 
				return convertView;
			}
		}
	 
	 	private void updateData(){
	
			 ParseQuery<ParseObject> taskQuery = ParseQuery.getQuery("Todo");
			 taskQuery.whereEqualTo("user", ParseUser.getCurrentUser());
			 taskQuery.whereEqualTo("status", "done");
			 taskQuery.findInBackground(new FindCallback<ParseObject>() {         
			      @Override
			      public void done(List<ParseObject> tasks, ParseException error) {
			          if(error == null){
			        	  
			        		try{
			        	 	    dataAdapter3.clear();
			         	    	dataAdapter3.notifyDataSetChanged();
			            	}
			        		
			            	catch(Exception e){
			            	}
			         	  
			        	  for (int i = 0; i < tasks.size(); i++) {
			        		  
			        	       String names = tasks.get(i).getString("todo").trim();
			        	       String status = tasks.get(i).getString("status");
			        	       Date created = tasks.get(i).getCreatedAt();
			        	       
			        	       taskID[i] = tasks.get(i).getObjectId();
			        	       taskName[i] = tasks.get(i).getString("todo");
			        	       taskCreated[i] = tasks.get(i).getCreatedAt();
			        	       taskModified[i] = tasks.get(i).getUpdatedAt();
			        	       
			        	       try{
			        	    	   displayListView(names, status, created);
			        	       }
			        	       catch(Exception e){
			        	    	   
			        	       }
			        	  
			        	  }
			        	  
			        	  int num;
			        	  
			        	  try{
			        		  num = listView3.getAdapter().getCount();
			        	  }
			        	  catch(Exception e){
			        		  num = 0;
			        	  }
			        	  
			        	  if(num != 0){
			        		  waitCursor(false);
			        		  labelEmptyCancelled.setVisibility(4);
			        	  }
			        	  else{
			        		  waitCursor(false);
			        		  labelEmptyCancelled.setVisibility(0);
			        	  }
			          }
			          
			          else{
			        	  
			          }
			      }
			  });
	 }
	 	
	 	 private void customDialog(final String title, final int id){
				
				final Dialog dialog = new Dialog(rootView1.getContext());
				dialog.setContentView(R.layout.custom_dialog);
				dialog.setTitle("");
				//dialog.setCancelable(false);
				
				Button dialogEditTask= (Button) dialog.findViewById(R.id.btnEditTask);
				Button dialogMoveTask = (Button) dialog.findViewById(R.id.btnMoveTask);
				Button dialogButtonCancel = (Button) dialog.findViewById(R.id.btnOnClose);
				Button dialogButtonDelete = (Button) dialog.findViewById(R.id.btnDelete);
				TextView dialogDescription = (TextView) dialog.findViewById(R.id.lblOnDescription);
				
				dialogMoveTask.setText("Set As Ongoing");
				
				dialogDescription.setText(title);
				// if button is clicked, close the custom dialog
				dialogMoveTask.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						
			      		try{
			    	 	    dataAdapter3.clear();
			     	    	dataAdapter3.notifyDataSetChanged();
							waitCursor(true);
							labelEmptyCancelled.setVisibility(4);
			        	}
			      		catch(Exception ee){
			      			
			      		}
						moveTask(id);
						dialog.dismiss();
					}
				});
				dialogEditTask.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						
			      		try{
			    	 	    dataAdapter3.clear();
			     	    	dataAdapter3.notifyDataSetChanged();
							waitCursor(true);
							labelEmptyCancelled.setVisibility(4);
			        	}
			      		catch(Exception ee){
			      			
			      		}
						editDialog(title, id);
						//Toast.makeText(rootView1.getContext(), "Edit me", Toast.LENGTH_SHORT).show();
						dialog.dismiss();
					}
				});
				dialogButtonDelete.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						deleteTaskDialog(id);
						dialog.dismiss();
					}
				});
				dialogButtonCancel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
				dialog.show();
			 }
		 
		 private void moveTask(final int id) {
			// TODO Auto-generated method stub
			 
			 ParseObject tasks = new ParseObject("Todo");
	    	 tasks.put("user", ParseUser.getCurrentUser());
	    	 tasks.put("todo", taskName[id]);
	    	 tasks.put("status", "ongoing");
	    	 
	    	 tasks.saveInBackground(new SaveCallback() {
	    	    	
	    		 public void done(ParseException e) {
	    			 
	    			 if(e==null){
	    				 // We have to use utility classes to add behavior to our ParseObjects.
	    				 //Toast.makeText(getApplicationContext(),
	    					// "Objects Saved!",
	    					 //Toast.LENGTH_LONG).show();
	    				 deleteTask(id, "Moved");
	    			 }
	    			 else{
	    				 Toast.makeText(rootView1.getContext(),
	    					 "Error Saving object",
	    					 Toast.LENGTH_LONG).show();
	    			 }
	    		 }	 
	    	 });

		}
		 private void deleteTask(int id, final String stats){
			 
			 ParseQuery<ParseObject> query = ParseQuery.getQuery("Todo");
			 query.whereEqualTo("user", ParseUser.getCurrentUser());	
			 query.whereEqualTo("objectId", taskID[id]);	
			 query.getFirstInBackground(new GetCallback<ParseObject>() {
				 public void done(ParseObject tasks, ParseException e) {
					 if (e == null) {
						 
						 tasks.deleteInBackground(new DeleteCallback() {
							
							@Override
							public void done(ParseException arg0) {
								// TODO Auto-generated method stub
								updateData();
								Toast.makeText(rootView1.getContext(), "Successfully "+stats, Toast.LENGTH_SHORT).show();

							}
						});
					
		    	     }
					 else {
						 showMessage("Message", "Not Found");	
		    	     }
		    	 }
			 });
		 }
		 
		 private void editDialog(String task, final int id) {
				// TODO Auto-generated method stub
				 
				 final Dialog dialog = new Dialog(rootView1.getContext());
				 dialog.setContentView(R.layout.todo_edit);
				 dialog.setTitle("Edit Task");
				 dialog.setCancelable(false);
				 
				 Button editTaskButton = (Button) dialog.findViewById(R.id.btnOKEdit);
				 Button cancelTaskButton = (Button) dialog.findViewById(R.id.btnCancelEdit);
				 
				 final EditText addTaskAppend = (EditText) dialog.findViewById(R.id.txtEdit);
				 
				 addTaskAppend.setText(task);
					
				 // if button is clicked, close the custom dialog
				 editTaskButton.setOnClickListener(new OnClickListener() {
					 @Override
					 public void onClick(View v) {
						 
						 if(addTaskAppend.getText().toString().equals("")){
							 showMessage("Warning", "Must not be empty");
						 }
						 else{
							 //addTask(addTaskAppend.getText().toString());
							 editData(addTaskAppend.getText().toString(), id);
							
							 dialog.dismiss();
						 }
						 dialog.dismiss();
						 
					 }
				 });
				 cancelTaskButton.setOnClickListener(new OnClickListener() {
					 @Override
					 public void onClick(View v) {
						 
						 waitCursor(true);
						 labelEmptyCancelled.setVisibility(4);
						
						 //adapter.notifyDataSetChanged();
						 updateData();
						
						 dialog.dismiss();
					 }
				 });
				 dialog.show();
			}
			 
			 private void editData(final String edited, int id) {
					// TODO Auto-generated method stub
					 ParseQuery<ParseObject> query = ParseQuery.getQuery("Todo");
					 
					 query.whereEqualTo("user", ParseUser.getCurrentUser()); 
					 query.whereEqualTo("status", "done");
					 query.getInBackground(taskID[id], new GetCallback<ParseObject>() {
						 
						 public void done(ParseObject gameScore, ParseException e) {
							 if (e == null) {

								 gameScore.put("todo", edited);
								 gameScore.saveInBackground(new SaveCallback() {
									
									@Override
									public void done(ParseException e) {
										// TODO Auto-generated method stub
										 waitCursor(true);
										 labelEmptyCancelled.setVisibility(4);
										
										 //adapter.notifyDataSetChanged();
										 updateData();
									}
								});
								 
							 }
						 }
					 });
				}
			 
			 private void deleteTaskDialog(final int id){
					
					final Dialog dialog = new Dialog(rootView1.getContext());
					dialog.setContentView(R.layout.delete_task_dialog);
					dialog.setTitle("");
					//dialog.setCancelable(false);
					
					Button dialogDelOK = (Button) dialog.findViewById(R.id.btnDelOK);
					Button dialogDelCancel = (Button) dialog.findViewById(R.id.btnDelCancel);
					
					// if button is clicked, close the custom dialog

					dialogDelOK.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							 deleteTask(id, "Removed");
							 dialog.dismiss();
						}
					});
					dialogDelCancel.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
						}
					});
					dialog.show();
				 }
	 
}
