package com.iconcept.tpbguide.tpbevents.Models;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;


@ParseClassName("Event")
public class Event extends ParseObject{
	public String getTitle(){
		return getString("title");
	}
	public ParseFile getImageFile(){
		return getParseFile("image");
	}
		
}
