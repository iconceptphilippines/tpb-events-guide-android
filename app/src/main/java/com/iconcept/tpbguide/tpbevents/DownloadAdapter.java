package com.iconcept.tpbguide.tpbevents;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.iconcept.tpbguide.tpbevents.R;
import com.iconcept.tpbguide.tpbevents.Models.Download;
import com.parse.ParseException;

public class DownloadAdapter extends ArrayAdapter<Download>{

	Download download;
	String size;
	
	public DownloadAdapter(Context context, String menuId, List<Download> downloadList) {
		super(context, 0, downloadList);

		download = new Download();
	}
	@Override
	public View getView(final int position, View v, final ViewGroup parent){
		 if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.download_item, parent, false);
            final ViewHolder holder = new ViewHolder();
            holder.fileName = (TextView) v.findViewById(R.id.fileName);
            holder.dateCreated = (TextView) v.findViewById(R.id.dateCreated);
            holder.fileType = (TextView) v.findViewById(R.id.fileType);
            holder.fileSize = (TextView) v.findViewById(R.id.fileSize);
            v.setTag(holder);
		 }
		 final Download download = (Download) getItem(position);
		 final ViewHolder holder = (ViewHolder) v.getTag();
//		 final int pos = position;
		 
		 SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		 String dateCreate = date.format(download.getDateCreated());
		 holder.fileName.setText(download.getFileName());
		 holder.fileType.setText("type: " + download.getFileType() +", ");
		 holder.dateCreated.setText(dateCreate);
		 
		 if (download.getFileSource().equals("file") && download.getFileSource() != null) {
			 try {
				byte[] data = download.getFile().getData();
				String ext = download.getFile().getName();
				String filenameArray[] = ext.split("\\.");
		        String extension = filenameArray[filenameArray.length-1];
				float file = data.length;
				float fileToKb = file / 1024;
				float fileToMb = fileToKb / 1024;
				float d = (float) fileToMb;
				
				String filesize = String.valueOf(String.format("%.2f", d));
				holder.fileSize.setText("size: " + filesize + "MB ");
				holder.fileType.setText("type: " +extension+", ");
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		 } else if(download.getFileSource().equals("url") && download.getFileSource() != null){
			 holder.fileSize.setText("size: " + download.getFileSize().toString() + "MB ");
		 } else {
			 //
		 }
		 
		
		 return v;
	}
	
	final class ViewHolder {
		public TextView fileName;
        public TextView dateCreated;
        public TextView fileType;
        public TextView fileSize;
	}
}
